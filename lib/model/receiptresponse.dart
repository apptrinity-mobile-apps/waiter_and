
import 'getitemmodifierssubmitresponse.dart';

class receiptresponse {
  OrderInfo orderInfo;
  int responseStatus;
  String result;

  receiptresponse({this.orderInfo, this.responseStatus, this.result});

  receiptresponse.fromJson(Map<String, dynamic> json) {
    orderInfo = json['orderInfo'] != null
        ? new OrderInfo.fromJson(json['orderInfo'])
        : null;
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.orderInfo != null) {
      data['orderInfo'] = this.orderInfo.toJson();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class OrderInfo {
  int checkNumber;
  String checkUniqueId;
  String createdBy;
  String createdOn;
  double creditsUsed;
  CustomerDetails customerDetails;
  String dineInBehaviour;
  String dineInOptionId;
  String dineInOptionName;
  double discountAmount;
  bool haveCustomer;
  List<ItemsList> itemsList;
  int noOfGuest;
  String orderId;
  int orderNumber;
  String orderUniqueId;
  int paymentStatus;
  String revenueCenterId;
  String serviceAreaId;
  String subTotal;
  int tableNumber;
  String taxAmount;
  String tipAmount;
  String totalAmount;

  OrderInfo(
      {this.checkNumber,
        this.checkUniqueId,
        this.createdBy,
        this.createdOn,
        this.creditsUsed,
        this.customerDetails,
        this.dineInBehaviour,
        this.dineInOptionId,
        this.dineInOptionName,
        this.discountAmount,
        this.haveCustomer,
        this.itemsList,
        this.noOfGuest,
        this.orderId,
        this.orderNumber,
        this.orderUniqueId,
        this.paymentStatus,
        this.revenueCenterId,
        this.serviceAreaId,
        this.subTotal,
        this.tableNumber,
        this.taxAmount,
        this.tipAmount,
        this.totalAmount});

  OrderInfo.fromJson(Map<String, dynamic> json) {
    checkNumber = json['checkNumber'];
    checkUniqueId = json['checkUniqueId'];
    createdBy = json['createdBy'];
    createdOn = json['createdOn'];
    creditsUsed = json['creditsUsed'] as double;
    customerDetails = json['customerDetails'] != null
        ? new CustomerDetails.fromJson(json['customerDetails'])
        : null;
    dineInBehaviour = json['dineInBehaviour'];
    dineInOptionId = json['dineInOptionId'];
    dineInOptionName = json['dineInOptionName'];
    discountAmount = json['discountAmount'] as double;
    haveCustomer = json['haveCustomer'];
    if (json['itemsList'] != null) {
      itemsList = new List<ItemsList>();
      json['itemsList'].forEach((v) {
        itemsList.add(new ItemsList.fromJson(v));
      });
    }
    noOfGuest = json['noOfGuest'];
    orderId = json['orderId'];
    orderNumber = json['orderNumber'];
    orderUniqueId = json['orderUniqueId'];
    paymentStatus = json['paymentStatus'];
    revenueCenterId = json['revenueCenterId'];
    serviceAreaId = json['serviceAreaId'];
    subTotal = json['subTotal'];
    tableNumber = json['tableNumber'];
    taxAmount = json['taxAmount'];
    tipAmount = json['tipAmount'];
    totalAmount = json['totalAmount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['checkNumber'] = this.checkNumber;
    data['checkUniqueId'] = this.checkUniqueId;
    data['createdBy'] = this.createdBy;
    data['createdOn'] = this.createdOn;
    data['creditsUsed'] = this.creditsUsed;
    if (this.customerDetails != null) {
      data['customerDetails'] = this.customerDetails.toJson();
    }
    data['dineInBehaviour'] = this.dineInBehaviour;
    data['dineInOptionId'] = this.dineInOptionId;
    data['dineInOptionName'] = this.dineInOptionName;
    data['discountAmount'] = this.discountAmount;
    data['haveCustomer'] = this.haveCustomer;
    if (this.itemsList != null) {
      data['itemsList'] = this.itemsList.map((v) => v.toJson()).toList();
    }
    data['noOfGuest'] = this.noOfGuest;
    data['orderId'] = this.orderId;
    data['orderNumber'] = this.orderNumber;
    data['orderUniqueId'] = this.orderUniqueId;
    data['paymentStatus'] = this.paymentStatus;
    data['revenueCenterId'] = this.revenueCenterId;
    data['serviceAreaId'] = this.serviceAreaId;
    data['subTotal'] = this.subTotal;
    data['tableNumber'] = this.tableNumber;
    data['taxAmount'] = this.taxAmount;
    data['tipAmount'] = this.tipAmount;
    data['totalAmount'] = this.totalAmount;
    return data;
  }
}

class CustomerDetails {
  String customerId;
  String email;
  String firstName;
  String lastName;
  String phoneNumber;

  CustomerDetails(
      {this.customerId,
        this.email,
        this.firstName,
        this.lastName,
        this.phoneNumber});

  CustomerDetails.fromJson(Map<String, dynamic> json) {
    customerId = json['customerId'];
    email = json['email'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    phoneNumber = json['phoneNumber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['customerId'] = this.customerId;
    data['email'] = this.email;
    data['firstName'] = this.firstName;
    data['lastName'] = this.lastName;
    data['phoneNumber'] = this.phoneNumber;
    return data;
  }
}

class ItemsList {
  String discountAmount;
  String discountName;
  String discountType;
  String discountValue;
  bool isBogo;
  bool isCombo;
  String itemId;
  String itemName;
  int itemStatus;
  String itemType;
  List<ModifierListApi> modifiersList;
  int quantity;
  List<SpecialRequestList> specialRequestList;
  String totalPrice;
  String unitPrice;

  ItemsList(
      {this.discountAmount,
        this.discountName,
        this.discountType,
        this.discountValue,
        this.isBogo,
        this.isCombo,
        this.itemId,
        this.itemName,
        this.itemStatus,
        this.itemType,
        this.modifiersList,
        this.quantity,
        this.specialRequestList,
        this.totalPrice,
        this.unitPrice});

  ItemsList.fromJson(Map<String, dynamic> json) {
    discountAmount = json['discountAmount'];
    discountName = json['discountName'];
    discountType = json['discountType'];
    discountValue = json['discountValue'] ;
    isBogo = json['isBogo'];
    isCombo = json['isCombo'];
    itemId = json['itemId'];
    itemName = json['itemName'];
    itemStatus = json['itemStatus'];
    itemType = json['itemType'];
    if (json['modifiersList'] != null) {
      modifiersList = new List<ModifierListApi>();
      json['modifiersList'].forEach((v) {
        modifiersList.add(new ModifierListApi.fromJson(v));
      });
    }
    quantity = json['quantity'];
    if (json['specialRequestList'] != null) {
      specialRequestList = new List<SpecialRequestList>();
      json['specialRequestList'].forEach((v) {
        specialRequestList.add(new SpecialRequestList.fromJson(v));
      });
    }
    totalPrice = json['totalPrice'];
    unitPrice = json['unitPrice'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['discountAmount'] = this.discountAmount;
    data['discountName'] = this.discountName;
    data['discountType'] = this.discountType;
    data['discountValue'] = this.discountValue;
    data['isBogo'] = this.isBogo;
    data['isCombo'] = this.isCombo;
    data['itemId'] = this.itemId;
    data['itemName'] = this.itemName;
    data['itemStatus'] = this.itemStatus;
    data['itemType'] = this.itemType;
    if (this.modifiersList != null) {
      data['modifiersList'] =
          this.modifiersList.map((v) => v.toJson()).toList();
    }
    data['quantity'] = this.quantity;
    if (this.specialRequestList != null) {
      data['specialRequestList'] =
          this.specialRequestList.map((v) => v.toJson()).toList();
    }
    data['totalPrice'] = this.totalPrice;
    data['unitPrice'] = this.unitPrice;
    return data;
  }
}

class SpecialRequestList {
  String name;
  int requestPrice;

  SpecialRequestList({this.name, this.requestPrice});

  SpecialRequestList.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    requestPrice = json['requestPrice'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['requestPrice'] = this.requestPrice;
    return data;
  }
}