import 'dart:convert';
import 'dart:core';
import 'dart:math';

import 'package:async/async.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'package:requests/requests.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:waiter/apis/CartsRepository.dart';
import 'package:waiter/apis/getAllDiscountItemsApi.dart';
import 'package:waiter/apis/getitemmodifiers.dart';
import 'package:waiter/model/SendTaxRatesModel.dart';
import 'package:waiter/model/cartmodelitems.dart';
import 'package:waiter/model/getitemmodifiersresponse.dart';
import 'package:waiter/session/userRepository.dart';
import 'package:waiter/utils/Globals.dart';
import 'package:waiter/utils/all_constans.dart';

import 'appbar_back_arrow.dart';
import 'model/cartmodelitemsapi.dart';
import 'model/getalldiscountitemresponse.dart';
import 'model/getitemmodifierssubmitresponse.dart';
import 'model/getmenuitems.dart';

class GetMenuItems {
  Future<List<MenuItem>> getmenu(
    String menu_id,
    String menu_groupid,
    String res_id,
  ) async {
    var body = json.encode({
      'menuId': menu_id,
      'menuGroupId': menu_groupid,
      'restaurantId': res_id
    });
    print(body);
    dynamic response = await Requests.post(base_url + "menu_items",
        body: body, headers: {'Content-type': 'application/json'});
    print("resultadditem---" + response);
    final res = json.decode(response);
    //print("result---"+res);
    if (res['responseStatus'] == 1) {
      Iterable list = res['menuItem'];
      return list.map((model) => MenuItem.fromJson(model)).toList();
    } else {
      return [];
    }
  }
}

class AddFoodItem extends StatefulWidget {
  final String menu_name;
  final String menu_id;
  final String menu_groupid;
  final String table_number;
  final String no_of_guests;
  final bool header_value;

  const AddFoodItem(this.menu_name, this.menu_id, this.menu_groupid,
      this.table_number, this.no_of_guests, this.header_value,
      {Key key})
      : super(key: key);

  @override
  _AddFoodItemState createState() => _AddFoodItemState();
}

class _AddFoodItemState extends State<AddFoodItem> {
  /* List<OrderDetailsModel> _get_order_details;
  bool _loading = true;
  int cart_count = 0;
  double total_qty = 0.0;
  List<AddProducts> __cart_items_list;
  String userid = "";
  CancelableOperation cancelableOperation;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();*/
  List<MenuItem> _get_all_menuitems;
  List<ModifiersGroupsList> _get_all_menuitemsmodifiersgrouplist;
  List<ModifierList> _get_all_menuitemsmodifierslist;
  List<MenuCartItemapi> __cart_items_list = new List();
  List<String> selectedCheckbox = [];
  bool _loading = true;

  List<bool> _isChecked;

  String item_id = "";
  String item_menugroup_id = "";
  String item_menu_id = "";
  String item_name = "";
  String table_number_session = "";
  String no_of_guests_session = "";

  String userid = "";
  String employee_id = "";
  String res_id = "";
  bool checkvalue = false;

  int cart_count = 0;
  List<String> selected_item_quantity = new List();
  int _items_cart_quantity_main = 0;
  int _items_cart_quantity = 0;
  String returnVal = "";

  final _formKey = GlobalKey<FormState>();
  CancelableOperation cancelableOperation;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  @override
  void initState() {
    super.initState();
    // print(widget.main_order_id.toString());

    UserRepository().getuserdetails().then((userdetails) {
      setState(() {
        employee_id = userdetails[0];
        UserRepository().getGenerateOtpDetails().then((restaurantdetails) {
          setState(() {
            res_id = restaurantdetails[0];
            print("restaurant_id" + res_id);

            print("FOODITEMS " +
                widget.menu_id +
                "--" +
                widget.menu_groupid +
                "--" +
                res_id);
            GetMenuItems()
                .getmenu(widget.menu_id, widget.menu_groupid, res_id)
                .then((result_allmenuitems) {
              setState(() {
                //isLoading = true;
                _get_all_menuitems = result_allmenuitems;

                Future.delayed(Duration(seconds: 2), () async {
                  setState(() {
                    CartsRepository().getcartslisting().then((cartList) {
                      setState(() {
                        __cart_items_list = cartList;
                        cart_count = __cart_items_list.length;
                        print(cart_count);

                        final mapped = __cart_items_list
                            .fold<Map<String, Map<String, dynamic>>>({},
                                (p, v) {
                          final name = v.itemId;
                          if (p.containsKey(name)) {
                            p[name]["NumberOfItems"] += int.parse(v.quantity);
                          } else {
                            p[name] = {"NumberOfItems": int.parse(v.quantity)};
                          }
                          return p;
                        });
                        //print(mapped.length);
                        // print(mapped);

                        for (var i = 0; i < _get_all_menuitems.length; i++) {
                          print(mapped.containsKey(_get_all_menuitems[i].id));
                          if (mapped.containsKey(_get_all_menuitems[i].id) ==
                              true) {
                            //print(mapped[_get_all_menuitems[i].id]['NumberOfItems']);
                            selected_item_quantity.add(
                                mapped[_get_all_menuitems[i].id]
                                        ['NumberOfItems']
                                    .toString());
                          } else {
                            //print("0");
                            selected_item_quantity.add("0");
                          }
                        }

                        UserRepository()
                            .getGenerateTablenumbernGuests()
                            .then((tableservicedetails) {
                          setState(() {
                            table_number_session = tableservicedetails[0];
                            no_of_guests_session = tableservicedetails[1];
                            print("TABLESERVICEDETAILS" +
                                table_number_session +
                                "---------" +
                                no_of_guests_session);
                          });
                        });
                        _loading = false;
                        //_loading = false;
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: YourAppbarBackArrow(
          count: cart_count,
          title_text: widget.menu_name,
        ),
        body: _loading
            ? Center(
                child: SpinKitFadingCircle(color: Colors.lightBlueAccent),
              )
            : Container(
                color: Colors.white,
                child: Column(
                  children: [
                    widget.header_value == true
                        ? Container(
                            color: dashboard_bg,
                            margin: EdgeInsets.only(left: 15, right: 15),
                            padding: EdgeInsets.symmetric(
                                horizontal: 12.0, vertical: 12.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    new Image.asset(
                                      "images/round_table.png",
                                      height: 48,
                                      width: 48,
                                    ),
                                    Padding(
                                      padding:
                                          EdgeInsets.only(left: 10, bottom: 0),
                                      child: RichText(
                                          text: TextSpan(children: [
                                        TextSpan(
                                            text: "Table no\n",
                                            style: TextStyle(
                                                color: add_food_item_bg,
                                                fontSize: 14,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w400)),
                                        TextSpan(
                                            text: widget.table_number,
                                            style: new TextStyle(
                                                fontSize: 18,
                                                color: add_food_item_bg,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w700))
                                      ])),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    new Image.asset(
                                      "images/round_customers_icon.png",
                                      height: 48,
                                      width: 48,
                                    ),
                                    Padding(
                                      padding:
                                          EdgeInsets.only(left: 10, bottom: 0),
                                      child: RichText(
                                          text: TextSpan(children: [
                                        TextSpan(
                                            text: "Customers\n",
                                            style: TextStyle(
                                                color: add_food_item_bg,
                                                fontSize: 14,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w400)),
                                        TextSpan(
                                            text: widget.no_of_guests,
                                            style: new TextStyle(
                                                fontSize: 18,
                                                color: add_food_item_bg,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w700))
                                      ])),
                                    ),
                                  ],
                                )
                              ],
                            ))
                        : SizedBox(),
                    SizedBox(height: 15),
                    Expanded(
                      child: Container(
                          margin: EdgeInsets.only(left: 15, right: 15),
                          padding: EdgeInsets.all(10),
                          height: MediaQuery.of(context).size.height * 0.2,
                          color: dashboard_bg,
                          child: ListView.builder(
                              scrollDirection: Axis.vertical,
                              itemCount: _get_all_menuitems.length,
                              itemBuilder: (context, index) {
                                return Container(
                                  margin: EdgeInsets.all(0),
                                  width:
                                      MediaQuery.of(context).size.width * 0.3,
                                  child: Container(
                                    color: Colors.white,
                                    //padding: EdgeInsets.all(15),
                                    //height: 60,
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Expanded(
                                                flex: 3,
                                                child: Row(
                                                  children: [
                                                    Expanded(
                                                        flex: 2,
                                                        child: Padding(
                                                            padding:
                                                                EdgeInsets.all(
                                                                    10),
                                                            child: Text(
                                                              _get_all_menuitems[
                                                                      index]
                                                                  .name,
                                                              style: TextStyle(
                                                                  color:
                                                                      add_food_item_bg,
                                                                  fontSize: 16,
                                                                  fontFamily:
                                                                      'Poppins',
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w600),
                                                              textAlign:
                                                                  TextAlign
                                                                      .start,
                                                            ))),
                                                    Expanded(
                                                        flex: 0,
                                                        child:
                                                            selected_item_quantity[
                                                                        index] !=
                                                                    "0"
                                                                ? new Container(
                                                                    width: 18.0,
                                                                    height:
                                                                        18.0,
                                                                    //padding: const EdgeInsets.only(top: 2.0),
                                                                    //I used some padding without fixed width and height
                                                                    decoration:
                                                                        new BoxDecoration(
                                                                      shape: BoxShape
                                                                          .circle,
                                                                      // You can use like this way or like the below line
                                                                      //borderRadius: new BorderRadius.circular(30.0),
                                                                      color:
                                                                          kToolbarTitleColor,
                                                                    ),
                                                                    child: new Text(
                                                                        selected_item_quantity[index]
                                                                            .toString(),
                                                                        textAlign:
                                                                            TextAlign
                                                                                .center,
                                                                        style: new TextStyle(
                                                                            color: Colors
                                                                                .white,
                                                                            fontSize:
                                                                                12.0,
                                                                            fontWeight:
                                                                                FontWeight.bold)), // You can add a Icon instead of text also, like below.
                                                                    //child: new Icon(Icons.arrow_forward, size: 50.0, color: Colors.black38)),
                                                                  )
                                                                : SizedBox())
                                                  ],
                                                )),
                                            Expanded(
                                                flex: 3,
                                                child: Row(
                                                  children: [
                                                    Expanded(
                                                        flex: 1,
                                                        child: Padding(
                                                            padding:
                                                                EdgeInsets.only(
                                                              left: 0,
                                                              right: 5,
                                                            ),
                                                            child: _get_all_menuitems[
                                                                            index]
                                                                        .basePrice !=
                                                                    null
                                                                ? Text(
                                                                    new String.fromCharCodes(new Runes(
                                                                            '\u0024')) +
                                                                        _get_all_menuitems[index]
                                                                            .basePrice
                                                                            .toString(),
                                                                    style: TextStyle(
                                                                        color:
                                                                            login_passcode_text,
                                                                        fontSize:
                                                                            14,
                                                                        fontFamily:
                                                                            'Poppins',
                                                                        fontWeight:
                                                                            FontWeight.w500),
                                                                    textAlign:
                                                                        TextAlign
                                                                            .right,
                                                                  )
                                                                : SizedBox())),
                                                    Expanded(
                                                        flex: 1,
                                                        child: Container(
                                                            padding:
                                                                EdgeInsets.only(
                                                              left: 0,
                                                              right: 10,
                                                            ),
                                                            child: FlatButton(
                                                                //minWidth: 30,
                                                                color: Colors
                                                                    .lightBlueAccent,
                                                                child: Text(
                                                                    "Add",
                                                                    style: TextStyle(
                                                                        fontSize:
                                                                            14,
                                                                        fontFamily:
                                                                            'Poppins',
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .w800,
                                                                        color:
                                                                            add_food_item_bg)),
                                                                onPressed: () {
                                                                  item_id = _get_all_menuitems[
                                                                          index]
                                                                      .id
                                                                      .toString();
                                                                  item_menugroup_id = _get_all_menuitems[
                                                                          index]
                                                                      .menuGroupId
                                                                      .toString();
                                                                  item_menu_id =
                                                                      _get_all_menuitems[
                                                                              index]
                                                                          .menuId
                                                                          .toString();
                                                                  item_name =
                                                                      _get_all_menuitems[
                                                                              index]
                                                                          .name;

                                                                  UserRepository()
                                                                      .getGenerateOtpDetails()
                                                                      .then(
                                                                          (restaurantdetails) {
                                                                    setState(
                                                                        () {
                                                                      res_id =
                                                                          restaurantdetails[
                                                                              0];
                                                                      print("restaurant_id" +
                                                                          res_id);
                                                                      print("FOODITEMSMODIFIERS " +
                                                                          _get_all_menuitems[index]
                                                                              .id +
                                                                          "----" +
                                                                          res_id);
                                                                      GetItemModifiers()
                                                                          .getmenu(
                                                                              res_id,
                                                                              _get_all_menuitems[index].id)
                                                                          .then((result_allmenuitemsmodifiers) {
                                                                        setState(
                                                                            () {
                                                                          //isLoading = true;

                                                                          _get_all_menuitemsmodifiersgrouplist =
                                                                              result_allmenuitemsmodifiers;
                                                                          print("chai" +
                                                                              _get_all_menuitemsmodifiersgrouplist.length.toString());

                                                                          if (_get_all_menuitems[index].priceProvider == 3 &&
                                                                              _get_all_menuitems[index].pricingStrategy == 1) {
                                                                            gewinner(index);
                                                                          } else if (_get_all_menuitems[index].priceProvider == 3 &&
                                                                              _get_all_menuitems[index].pricingStrategy == 4) {
                                                                            gewinner(index);
                                                                          } else if (_get_all_menuitems[index].priceProvider == 3 &&
                                                                              _get_all_menuitems[index].pricingStrategy == 5) {
                                                                            print("MEDUVADA");
                                                                            Toast.show("MEDUVADA",
                                                                                context,
                                                                                duration: Toast.LENGTH_SHORT,
                                                                                gravity: Toast.BOTTOM);
                                                                            /*showDialog(
                                                                      context: context,
                                                                      builder: (context) {
                                                                        return _MyDialog(
                                                                            cart_item_id: _get_all_menuitems[index].id.toString(),
                                                                            cart_menu_id: _get_all_menuitems[index].menuId.toString(),
                                                                            cart_menugroup_id: _get_all_menuitems[index].menuGroupId.toString(),
                                                                            cart_item_unit_price: _get_all_menuitems[index].basePrice.toString(),
                                                                            header_name: _get_all_menuitems[index].name.toString(),
                                                                            header_price: _get_all_menuitems[index].basePrice.toString(),
                                                                            priceProvider: _get_all_menuitems[index].priceProvider.toString(),
                                                                            priceStrategy: _get_all_menuitems[index].pricingStrategy.toString(),
                                                                            sizeList: _get_all_menuitems[index].sizeList,
                                                                            timepriceList: _get_all_menuitems[index].timePriceList,
                                                                            modifierGroupList: _get_all_menuitemsmodifiersgrouplist,
                                                                            selectedCities: selectedCheckbox,
                                                                            onSelectedCitiesListChanged: (modifierGroupList) {
                                                                              selectedCheckbox = modifierGroupList;
                                                                              print(selectedCheckbox);
                                                                            });
                                                                      });*/
                                                                            gewinner(index);
                                                                          } else {
                                                                            print("IDLY");
                                                                            /* Toast.show("IDLY", context,
                                                                                duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);*/
                                                                            /*showDialog(
                                                                      context: context,
                                                                      builder: (context) {
                                                                        return _MyDialog(
                                                                            cart_item_id: _get_all_menuitems[index].id.toString(),
                                                                            cart_menu_id: _get_all_menuitems[index].menuId.toString(),
                                                                            cart_menugroup_id: _get_all_menuitems[index].menuGroupId.toString(),
                                                                            cart_item_unit_price: _get_all_menuitems[index].basePrice.toString(),
                                                                            header_name: _get_all_menuitems[index].name.toString(),
                                                                            header_price: _get_all_menuitems[index].basePrice.toString(),
                                                                            priceProvider: _get_all_menuitems[index].priceProvider.toString(),
                                                                            priceStrategy: _get_all_menuitems[index].pricingStrategy.toString(),
                                                                            sizeList: _get_all_menuitems[index].sizeList,
                                                                            timepriceList: _get_all_menuitems[index].timePriceList,
                                                                            modifierGroupList: _get_all_menuitemsmodifiersgrouplist,
                                                                            selectedCities: selectedCheckbox,
                                                                            onSelectedCitiesListChanged: (modifierGroupList) {
                                                                              selectedCheckbox = modifierGroupList;
                                                                              print(selectedCheckbox);
                                                                            });
                                                                      });*/
                                                                            gewinner(index);
                                                                          }
                                                                        });
                                                                      });
                                                                    });
                                                                  });
                                                                })))
                                                  ],
                                                )),
                                          ],
                                        ),
                                        Container(
                                          margin: EdgeInsets.fromLTRB(
                                              10.0, 0.0, 10.0, 0.0),
                                          child: Divider(
                                            color: cart_viewline,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              })),
                    ),
                  ],
                )));
  }

  Future gewinner(int index) async {
    returnVal = await showDialog(
        context: context,
        builder: (context) {
          return _MyDialog(
              user_id: employee_id,
              restaurant_id: res_id,
              taxIncludeOption: _get_all_menuitems[index].taxIncludeOption,
              cart_item_id: _get_all_menuitems[index].id.toString(),
              cart_menu_id: _get_all_menuitems[index].menuId.toString(),
              cart_menugroup_id:
                  _get_all_menuitems[index].menuGroupId.toString(),
              cart_item_unit_price:
                  _get_all_menuitems[index].basePrice.toString(),
              header_name: _get_all_menuitems[index].name.toString(),
              header_price: _get_all_menuitems[index].basePrice.toString(),
              priceProvider: _get_all_menuitems[index].priceProvider.toString(),
              priceStrategy:
                  _get_all_menuitems[index].pricingStrategy.toString(),
              sizeList: _get_all_menuitems[index].sizeList,
              timepriceList: _get_all_menuitems[index].timePriceList,
              modifierGroupList: _get_all_menuitemsmodifiersgrouplist,
              applicableTaxrates: _get_all_menuitems[index].aplicableTaxRates,
              selectedCities: selectedCheckbox,
              onSelectedCitiesListChanged: (modifierGroupList) {
                selectedCheckbox = modifierGroupList;
                print(selectedCheckbox);
              });
        });
    print("RETURNVALUEINSIDE" + returnVal);

    if (returnVal == "Success") {
      Future.delayed(Duration(seconds: 2), () async {
        setState(() {
          CartsRepository().getcartslisting().then((cartList) {
            setState(() {
              __cart_items_list = cartList;
              cart_count = __cart_items_list.length;
              print(cart_count);
              final mapped = __cart_items_list
                  .fold<Map<String, Map<String, dynamic>>>({}, (p, v) {
                final name = v.itemId;
                if (p.containsKey(name)) {
                  p[name]["NumberOfItems"] += int.parse(v.quantity);
                } else {
                  p[name] = {"NumberOfItems": int.parse(v.quantity)};
                }
                return p;
              });
              print(mapped.length);
              print(mapped);
              print(_get_all_menuitems.length);
              selected_item_quantity.clear();
              for (var i = 0; i < _get_all_menuitems.length; i++) {
                print(mapped.containsKey(_get_all_menuitems[i].id));
                if (mapped.containsKey(_get_all_menuitems[i].id) == true) {
                  print(mapped[_get_all_menuitems[i].id]['NumberOfItems']);
                  selected_item_quantity.add(mapped[_get_all_menuitems[i].id]
                          ['NumberOfItems']
                      .toString());
                } else {
                  print("0");
                  selected_item_quantity.add("0");
                }
              } //_loading = false;
            });
          });
        });
      });
    }
  }
}

class _MyDialog extends StatefulWidget {
  _MyDialog({
    this.user_id,
    this.restaurant_id,
    this.taxIncludeOption,
    this.cart_item_id,
    this.cart_menu_id,
    this.cart_menugroup_id,
    this.cart_item_unit_price,
    this.header_name,
    this.header_price,
    this.priceProvider,
    this.priceStrategy,
    this.sizeList,
    this.timepriceList,
    this.modifierGroupList,
    this.applicableTaxrates,
    this.selectedCities,
    this.onSelectedCitiesListChanged,
  });

  final String user_id;
  final String restaurant_id;
  final bool taxIncludeOption;
  final String cart_item_id;
  final String cart_menu_id;
  final String cart_menugroup_id;
  final String cart_item_unit_price;
  final String header_name;
  final String header_price;
  final String priceProvider;
  final String priceStrategy;
  final List<SizeList> sizeList;
  final List<TimePriceList> timepriceList;

  final List<ModifiersGroupsList> modifierGroupList;
  final List<AplicableTaxRates> applicableTaxrates;
  final List<String> selectedCities;

  final ValueChanged<List<String>> onSelectedCitiesListChanged;

  @override
  _MyDialogState createState() => _MyDialogState();
}

class _MyDialogState extends State<_MyDialog> {
  var _isChecked = false;
  var main_item_name = "";
  var main_item_price = "";
  final _formKey = GlobalKey<FormState>();
  final _formKey2 = GlobalKey<FormState>();
  List<List<bool>> _group_checkslist = new List();
  bool _loading = true;

  List<String> cartdataadding = new List();
  List<ModifierListApi> modifier_arraylist = new List();
  List<TaxTable> tax_table_arraylist = new List();
  List<SendTaxRatesModel> taxes_arraylist = new List();
  List<SpecialRequestListApi> special_request_arraylist = new List();

  TextEditingController cart_count_text_controller;
  TextEditingController custom_item_price_controller = TextEditingController();
  TextEditingController text_description_controller = TextEditingController();
  TextEditingController text_specialitem_controller = TextEditingController();
  TextEditingController text_discount_amount_controller =
      TextEditingController();
  TextEditingController text_specialitemprice_controller =
      TextEditingController();
  var counter_value = 1;
  var open_discount_type = "";
  double open_discount_maxamount = 0.0;
  var open_discount_id = "";
  var discount_type = "";
  var discount_type_item = "";
  var discount_id = "";
  var discount_name = "";
  double discount_maxamount = 0.0;
  var open_discount_name = "";
  double total_discountprice = 0.0;
  double total_discountitemprice = 0.0;
  double total_discountprice_double = 0.0;
  var body = "";
  int selected_pos = 0;

  List<Discounts> _get_All_Discounts = new List();
  var discount_id_api = "";
  var discount_type_api = "";

  var taxType = "";
  var taxTypeId = 0;
  var tax = 0.0;
  String roundingOptions = "";
  int roundingOptionId = 0;
  var day = "";
  var time = "";
  var timefrom = "";
  var timeto = "";
  var time_base_price = "";
  List<String> _timedays = new List();

  Widget _incrementButton() {
    return Container(
      margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
      child: Material(
        color: login_passcode_text,
        // button color
        child: InkWell(
          splashColor: login_passcode_bg2,
          // inkwell color
          child: SizedBox(
              width: 35,
              height: 35,
              child: Icon(
                Icons.add,
                color: Colors.white,
                size: 17,
              )),
          onTap: () {
            setState(() {
              counter_value = counter_value + 1;
            });
          },
        ),
      ),
    );
  }

  Widget _decrementButton() {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
      child: Material(
        color: login_passcode_bg1,
        // button color
        child: InkWell(
          splashColor: login_passcode_bg2,
          // inkwell color
          child: SizedBox(
              width: 35,
              height: 35,
              child: Icon(
                Icons.remove,
                color: Colors.white,
                size: 17,
              )),
          onTap: () {
            setState(() {
              if (counter_value > 1) {
                counter_value = counter_value - 1;
              }
            });
          },
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    // print(widget.main_order_id.toString());
    print("PRICESTRATEGY " +
        widget.priceProvider +
        "-----" +
        widget.priceStrategy);
    print(widget.modifierGroupList.length);
    print(widget.sizeList.length);
    print("TIMEPRICELIST" + widget.timepriceList.length.toString());

    day = DateFormat('EEEE').format(DateTime.now());
    var now = DateTime.now();
    time = DateFormat('HH:mm').format(now);
    print(day + "=====" + time.toString());

    main_item_name = widget.header_name;
    main_item_price = widget.cart_item_unit_price;

    for (int t = 0; t < widget.timepriceList.length; t++) {
      timefrom = widget.timepriceList[t].timeFrom.toString();
      timeto = widget.timepriceList[t].timeTo.toString();
      for (int d = 0; d < widget.timepriceList[t].days.length; d++) {
        //_timedays.add(widget.timepriceList[t].days[d].toString());
        DateTime  tempDatefrom = new DateFormat("HH:mm").parse(timefrom);
        DateTime  tempDateto = new DateFormat("HH:mm").parse(timeto);

        print("TIMEFROMTO"+timefrom+"+======="+timeto);
        var isDateInRange = isCurrentDateInRange(tempDatefrom, tempDateto);
        if(day == widget.timepriceList[t].days[d].toString()){
          print("DAYLOOP"+day+"========"+widget.timepriceList[t].days[d].toString());
          if(isDateInRange){
             time_base_price = widget.timepriceList[t].price.toStringAsFixed(2);
            print("BASEPRICE"+time_base_price);
             main_item_price = time_base_price.toString();
            break;
          }
        }

      }
      //print("DAYS" + _timedays.toString());

    }





    for (int g = 0; g < widget.modifierGroupList.length; g++) {
      List<bool> _items_checked = new List();
      for (int h = 0;
          h < widget.modifierGroupList[g].modifierList.length;
          h++) {
        //print(g.toString()+"--"+h.toString()+"--"+widget.modifierGroupList[g].modifierList.length.toString());
        _items_checked.add(false);
      }
      _group_checkslist.add(_items_checked);
    }


    print("ADDITEMSTOCARTDIALOG " +
        widget.cart_item_id +
        "----" +
        widget.cart_menu_id +
        "-----" +
        widget.cart_menugroup_id +
        "-------" +
        widget.header_name +
        "-----" +
        widget.cart_item_unit_price +
        "TAXINCLUDE" +
        widget.taxIncludeOption.toString());

    GetAllDiscountsApi(widget.cart_item_id);

    // getCartItemModel.add();

    /*for(int g=0;g < _group_checkslist.length;g++){

      for(int h=0;h < _group_checkslist[g].length;h++){
        print(g.toString()+"--"+h.toString()+"--"+_group_checkslist[g][h].toString());

      }

    }*/
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.white,
      insetPadding: EdgeInsets.all(20),
      child: Column(
        children: [
          Expanded(
              child: SingleChildScrollView(
                  child: Container(
                      //height: MediaQuery.of(context).size.height,
                      child: Expanded(
                          child: Column(
            children: <Widget>[
              Container(
                  width: double.infinity,
                  child: Column(mainAxisSize: MainAxisSize.max, children: [
                    Container(
                        height: 56,
                        color: Colors.lightBlueAccent,
                        width: double.infinity,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Expanded(
                                flex: 5,
                                child: Padding(
                                    padding:
                                        EdgeInsets.only(left: 10, bottom: 0),
                                    child: Text(
                                      widget.header_name,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 16,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w600),
                                      textAlign: TextAlign.start,
                                    ))),
                            widget.header_price != 'null'
                                ? Expanded(
                                    flex: 3,
                                    child: Padding(
                                        padding: EdgeInsets.only(
                                            right: 5, bottom: 0),
                                        child: Text(
                                          new String.fromCharCodes(
                                                  new Runes('\u0024')) +
                                              main_item_price,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w700),
                                          textAlign: TextAlign.start,
                                        )))
                                : SizedBox(),
                            Expanded(
                                flex: 1,
                                child: InkWell(
                                    child: Padding(
                                        padding: EdgeInsets.only(
                                            right: 15, bottom: 0),
                                        child: Image.asset(
                                          "images/cancel.png",
                                          height: 25,
                                          width: 25,
                                          color: Colors.white,
                                        )),
                                    onTap: () {
                                      Navigator.pop(context);
                                    })),
                            /*Expanded(
                                    flex: 1,
                                    child: InkWell(
                                        child: Padding(
                                            padding: EdgeInsets.only(
                                                right: 15, bottom: 0),
                                            child: Image.asset(
                                              "images/cancel.png",
                                              height: 30,
                                              width: 30,
                                              color: Colors.white,
                                            )),
                                        onTap: () {})),*/
                          ],
                        )),
                  ])),

              widget.priceStrategy == '5'
                  ? Container(
                      height: 30,
                      child: Row(
                        children: [
                          TextFormField(
                            validator: (val) {
                              if (val.isEmpty) return 'Enter Username';
                              return null;
                            },
                            controller: custom_item_price_controller,
                            obscureText: false,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                                filled: true,
                                fillColor: Colors.white,
                                hintText: "Enter Price",
                                contentPadding: EdgeInsets.only(
                                  bottom: 30 / 2,
                                  left: 50 / 2, // HERE THE IMPORTANT PART
                                  // HERE THE IMPORTANT PART
                                ),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(0.0))),
                          ),
                          Container(
                              margin: EdgeInsets.fromLTRB(25, 30, 25, 0),
                              height: 50,
                              width: double.infinity,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  color: login_passcode_bg1,
                                  borderRadius: BorderRadius.circular(0)),
                              child: InkWell(
                                  child: FlatButton(
                                      minWidth: double.infinity,
                                      height: double.infinity,
                                      child: Text("SUBMIT",
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w800,
                                              color: Colors.white)),
                                      onPressed: () {})))
                        ],
                      ),
                    )
                  : SizedBox(),
              //SIZELISTDATA
              widget.sizeList.length > 0
                  ? Container(
                      margin: EdgeInsets.only(left: 10, top: 10, right: 10),
                      //padding: EdgeInsets.all(5),
                      height: 70,
                      color: dashboard_bg,
                      child: ListView.separated(
                          separatorBuilder: (context, i) => Divider(
                                color: Colors.grey,
                                height: 0.1,
                              ),
                          scrollDirection: Axis.horizontal,
                          itemCount: widget.sizeList.length,
                          itemBuilder: (context, i) {
                            return InkWell(
                              onTap: () {
                                setState(() {
                                  selected_pos = i;
                                  var sizelistname =
                                      widget.sizeList[i].sizeName.toString();
                                  var sizelistprice =
                                      widget.sizeList[i].price.toString();
                                  main_item_name =
                                      widget.header_name + " " + sizelistname;
                                  main_item_price = sizelistprice;
                                  print("SIZELISTDATA" +
                                      main_item_name +
                                      "-----" +
                                      main_item_price);
                                });
                              },
                              child: Card(
                                elevation: 2,
                                color: selected_pos == i
                                    ? Colors.lightBlueAccent
                                    : Colors.white,
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.horizontal(
                                        left: Radius.circular(3.00),
                                        right: Radius.zero),
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Padding(
                                          padding: EdgeInsets.only(
                                              left: 10,
                                              top: 5,
                                              right: 10,
                                              bottom: 0),
                                          child: Text(
                                            widget.sizeList[i].sizeName,
                                            style: TextStyle(
                                                fontSize: 15,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w500,
                                                color: login_passcode_text),
                                          )),
                                      SizedBox(height: 3),
                                      Padding(
                                          padding: EdgeInsets.only(
                                              left: 10,
                                              top: 0,
                                              right: 10,
                                              bottom: 5),
                                          child: Text(
                                            new String.fromCharCodes(
                                                    new Runes('\u0024')) +
                                                widget.sizeList[i].price
                                                    .toString(),
                                            style: TextStyle(
                                                fontSize: 14,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w500,
                                                color: login_passcode_text),
                                          ))
                                    ],
                                  ),
                                ),
                              ),
                            );
                          }))
                  : SizedBox(),
              //MODIFIERLISTDATA
              widget.modifierGroupList.length > 0
                  ? Container(
                      margin: EdgeInsets.only(left: 10, top: 10, right: 10),
                      //padding: EdgeInsets.all(5),
                      //height: MediaQuery.of(context).size.height * 0.2,
                      color: dashboard_bg,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListView.builder(
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: widget.modifierGroupList.length,
                            itemBuilder: (BuildContext context1, index) {
                              return Container(
                                margin: EdgeInsets.fromLTRB(10, 0, 0, 15),
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Padding(
                                            padding: EdgeInsets.only(
                                                left: 5, bottom: 0, top: 10),
                                            child: Text(
                                              widget.modifierGroupList[index]
                                                  .modifierGroupName,
                                              style: TextStyle(
                                                  color: login_passcode_text,
                                                  fontSize: 15,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w700),
                                              textAlign: TextAlign.start,
                                            )),
                                        widget.modifierGroupList[index]
                                                    .minSelections !=
                                                null
                                            ? Padding(
                                                padding: EdgeInsets.only(
                                                    right:   0,
                                                    bottom: 0,
                                                    top: 10),
                                                child: Text(
                                                  "  (Please choose up to " +
                                                      widget
                                                          .modifierGroupList[
                                                              index]
                                                          .minSelections
                                                          .toString() +
                                                      ")",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 14,
                                                      fontFamily: 'Poppins',
                                                      fontWeight:
                                                          FontWeight.w400),
                                                  textAlign: TextAlign.start,
                                                ),
                                              )
                                            : SizedBox()
                                      ],
                                    ),
                                    ListView.builder(
                                        physics: NeverScrollableScrollPhysics(),
                                        shrinkWrap: true,
                                        itemCount: widget
                                            .modifierGroupList[index]
                                            .modifierList
                                            .length,
                                        itemBuilder:
                                            (BuildContext context1, j) {
                                          //print("KKK"+k.toString()+"--"+m.toString());
                                          //print(default_select_all[k][m]+"default_select--"+k.toString()+"--"+m.toString()+"--"+multi_selected_list[k][m].toString());
                                          return Container(
                                              child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Row(children: <Widget>[
                                                Theme(
                                                    data: ThemeData(
                                                        unselectedWidgetColor:
                                                            Colors.grey),
                                                    child: new Checkbox(
                                                        value:
                                                            _group_checkslist[
                                                                index][j],
                                                        onChanged: (value) {
                                                          setState(() {
                                                            _group_checkslist[
                                                                    index][j] =
                                                                value;
                                                            print(value);
                                                          });
                                                        },
                                                        activeColor: Colors
                                                            .lightBlueAccent)),
                                                new Text(
                                                  widget
                                                      .modifierGroupList[index]
                                                      .modifierList[j]
                                                      .modifierName
                                                      .toString(),
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 14,
                                                      fontFamily: 'Poppins',
                                                      fontWeight:
                                                          FontWeight.w400),
                                                ),
                                              ]),
                                              Padding(
                                                padding:
                                                    EdgeInsets.only(right: 15),
                                                child: new Text(
                                                  new String.fromCharCodes(
                                                          new Runes('\u0024')) +
                                                      widget
                                                          .modifierGroupList[
                                                              index]
                                                          .modifierList[j]
                                                          .price
                                                          .toString(),
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 14,
                                                      fontFamily: 'Poppins',
                                                      fontWeight:
                                                          FontWeight.w400),
                                                ),
                                              ),
                                              /* Expanded(
                                                flex: 1,
                                                child: SizedBox(
                                                  height: 30,
                                                  child: Theme(
                                                    data: ThemeData(unselectedWidgetColor: Colors.grey),child:CheckboxListTile(
                                                    contentPadding: EdgeInsets.all(0),
                                                    value: _group_checkslist[index][j],
                                                    //selected: checkvalue,
                                                    onChanged: (value) {
                                                      setState(() {
                                                        _group_checkslist[index][j] = value;
                                                        print(value);
                                                      });
                                                    },
                                                    title: new Text(
                                                      widget.modifierGroupList[index].modifierList[j].modifierName.toString(),
                                                      style: TextStyle(
                                                          color: Colors.black, fontSize: 14, fontFamily: 'Poppins', fontWeight: FontWeight.w400),
                                                    ),
                                                    controlAffinity: ListTileControlAffinity.leading,
                                                    activeColor: Colors.lightBlueAccent,
                                                  ),
                                                )),
                                              ),
                                              Expanded(
                                                flex: 1,
                                                child: new Text(
                                                  widget.modifierGroupList[index].modifierList[j].price.toString(),
                                                  style: TextStyle(
                                                      color: Colors.black, fontSize: 14, fontFamily: 'Poppins', fontWeight: FontWeight.w400),
                                                ),
                                              ),*/
                                            ],
                                          ));
                                        }),
                                  ],
                                ),
                              );
                            },
                          ),
                        ],
                      ))
                  : SizedBox(),

              Container(
                margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                color: modifiers_bg,
                child: Expanded(
                  child: Column(
                    children: [
                      Container(
                          child: Column(
                        children: [
                          Padding(
                            padding:
                                EdgeInsets.only(left: 0, top: 10, right: 0),
                            child: Text(
                              "Special Request",
                              style: TextStyle(
                                  color: login_passcode_text,
                                  fontSize: 15,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w700),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          special_request_arraylist.length > 0
                              ? ListView.builder(
                                  shrinkWrap: true,
                                  physics: ClampingScrollPhysics(),
                                  scrollDirection: Axis.vertical,
                                  itemCount: special_request_arraylist.length,
                                  itemBuilder: (context, i) {
                                    return Container(
                                      margin: EdgeInsets.all(2),
                                      width: MediaQuery.of(context).size.width *
                                          0.3,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Expanded(
                                              flex: 5,
                                              child: Padding(
                                                  padding: EdgeInsets.only(
                                                      left: 15, right: 0),
                                                  child: Text(
                                                    special_request_arraylist[i]
                                                        .name
                                                        .toString(),
                                                    style: TextStyle(
                                                        color: coupontext,
                                                        fontSize: 14,
                                                        fontFamily: 'Poppins',
                                                        fontWeight:
                                                            FontWeight.w400),
                                                    textAlign: TextAlign.start,
                                                  ))),
                                          Expanded(
                                              flex: 1,
                                              child: Padding(
                                                padding: EdgeInsets.only(
                                                    left: 0, right: 0),
                                                child: Text(
                                                  new String.fromCharCodes(
                                                          new Runes('\u0024')) +
                                                      special_request_arraylist[
                                                              i]
                                                          .requestPrice
                                                          .toStringAsFixed(2),
                                                  style: TextStyle(
                                                      color: coupontext,
                                                      fontSize: 14,
                                                      fontFamily: 'Poppins',
                                                      fontWeight:
                                                          FontWeight.w400),
                                                  textAlign: TextAlign.start,
                                                ),
                                              )),
                                          InkWell(
                                              child: Expanded(
                                                  flex: 1,
                                                  child: Padding(
                                                      padding: EdgeInsets.only(
                                                          left: 0, right: 5),
                                                      child: Image.asset(
                                                        'images/cancel.png',
                                                        height: 20,
                                                        width: 20,
                                                      ))),
                                              onTap: () {
                                                setState(() {
                                                  special_request_arraylist
                                                      .removeAt(i);
                                                  //_savecartList(__cart_items_list);
                                                });
                                              })
                                        ],
                                      ),
                                    );
                                  })
                              : SizedBox(),
                          Container(
                              margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                              child: new Form(
                                  key: _formKey,
                                  child: Expanded(
                                      child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Container(
                                        width: 100,
                                        child: TextFormField(
                                            validator: (val) {
                                              if (val.isEmpty)
                                                return 'Enter Itemname';
                                              return null;
                                            },
                                            controller:
                                                text_specialitem_controller,
                                            obscureText: false,
                                            keyboardType: TextInputType.text,
                                            decoration: InputDecoration(
                                                filled: true,
                                                fillColor: Colors.white,
                                                hintText: "Item",
                                                contentPadding: EdgeInsets.only(
                                                  bottom: 20 / 2,
                                                  left: 20 /
                                                      2, // HERE THE IMPORTANT PART
                                                  // HERE THE IMPORTANT PART
                                                ),
                                                border: OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            5.0)))),
                                      ),
                                      Container(
                                        width: 100,
                                        child: TextFormField(
                                            validator: (val) {
                                              if (val.isEmpty)
                                                return 'Enter Price';
                                              return null;
                                            },
                                            controller:
                                                text_specialitemprice_controller,
                                            obscureText: false,
                                            keyboardType: TextInputType.number,
                                            decoration: InputDecoration(
                                                filled: true,
                                                fillColor: Colors.white,
                                                hintText: "Price",
                                                contentPadding: EdgeInsets.only(
                                                  bottom: 30 / 2,
                                                  left: 50 /
                                                      2, // HERE THE IMPORTANT PART
                                                  // HERE THE IMPORTANT PART
                                                ),
                                                border: OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            5.0)))),
                                      ),
                                      Padding(
                                          padding: EdgeInsets.only(
                                            left: 0,
                                            right: 15,
                                          ),
                                          child: InkWell(
                                            child: FlatButton(
                                                minWidth: 30,
                                                color: Colors.lightBlueAccent,
                                                child: Text("Add",
                                                    style: TextStyle(
                                                        fontSize: 14,
                                                        fontFamily: 'Poppins',
                                                        fontWeight:
                                                            FontWeight.w800,
                                                        color:
                                                            add_food_item_bg)),
                                                onPressed: () {
                                                  if (_formKey.currentState
                                                      .validate()) {
                                                    _formKey.currentState
                                                        .save();
                                                    print(text_specialitem_controller
                                                            .text
                                                            .toString() +
                                                        "-----" +
                                                        text_specialitemprice_controller
                                                            .text
                                                            .toString());
                                                    setState(() {
                                                      special_request_arraylist.add(
                                                          SpecialRequestListApi(
                                                              text_specialitem_controller
                                                                  .text,
                                                              int.parse(
                                                                  text_specialitemprice_controller
                                                                      .text)));

                                                      text_specialitem_controller
                                                          .text = "";
                                                      text_specialitemprice_controller
                                                          .text = "";
                                                    });

                                                    print("SPECIALREQUESTLENGTH" +
                                                        special_request_arraylist
                                                            .length
                                                            .toString());
                                                  }
                                                }),
                                          ))
                                    ],
                                  )))),
                        ],
                      )),
                      _get_All_Discounts.length > 0
                          ? Container(
                              child: Column(
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: 0, top: 10, right: 0),
                                  child: Text(
                                    "Discount Item",
                                    style: TextStyle(
                                        color: login_passcode_text,
                                        fontSize: 16,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.w700),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                                discount_type_api != "open"
                                    ? Container(
                                        margin:
                                            EdgeInsets.fromLTRB(10, 10, 10, 0),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              flex: 1,
                                              child: SizedBox(
                                                height: 120,
                                                child:
                                                    /*RadioListBuilder(
                                    num: 5,
                                  )*/
                                                    Container(
                                                        margin: EdgeInsets.only(
                                                            top: 10,
                                                            left: 10,
                                                            right: 10,
                                                            bottom: 10),
                                                        child: Expanded(
                                                          flex: 1,
                                                          child:
                                                              ListView.builder(
                                                            itemCount:
                                                                _get_All_Discounts
                                                                    .length,
                                                            itemBuilder:
                                                                (context,
                                                                    index) {
                                                              return SizedBox(
                                                                  height: 40,
                                                                  child:
                                                                      RadioListTile(
                                                                    value: _get_All_Discounts[
                                                                        index],
                                                                    groupValue:
                                                                        _get_All_Discounts[index]
                                                                            .orderValue,
                                                                    onChanged: (value) =>
                                                                        setState(
                                                                            () {
                                                                      _get_All_Discounts[index]
                                                                              .orderValue =
                                                                          value;
                                                                    }),

                                                                    /* onChanged: (ind){ setState(() => _get_All_Discounts[index].orderValue = ind);
                                            },*/
                                                                    title: Text(new String.fromCharCodes(new Runes(
                                                                            '\u0024')) +
                                                                        _get_All_Discounts[index]
                                                                            .maxDiscountAmount
                                                                            .toString()),
                                                                  ));
                                                            },
                                                          ),
                                                        )),
                                              ),
                                            ),
                                          ],
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceAround,
                                        ),
                                      )
                                    : SizedBox(),
                                Container(
                                  margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: SizedBox(
                                          height: 80,
                                          child: Container(
                                              margin: EdgeInsets.only(
                                                  top: 10,
                                                  left: 10,
                                                  right: 10,
                                                  bottom: 10),
                                              child: Expanded(
                                                flex: 1,
                                                child: ListView.builder(
                                                  itemCount:
                                                      _get_All_Discounts.length,
                                                  itemBuilder:
                                                      (context, index) {
                                                    return SizedBox(
                                                        height: 40,
                                                        child: RadioListTile(
                                                          value: 1,
                                                          groupValue:
                                                              _get_All_Discounts[
                                                                      index]
                                                                  .orderValue,
                                                          onChanged: (value) =>
                                                              setState(() {
                                                            _get_All_Discounts[
                                                                        index]
                                                                    .orderValue =
                                                                value;

                                                            open_discount_type =
                                                                _get_All_Discounts[
                                                                        index]
                                                                    .discountValueType
                                                                    .toString();
                                                            open_discount_id =
                                                                _get_All_Discounts[
                                                                        index]
                                                                    .id
                                                                    .toString();
                                                            open_discount_name =
                                                                _get_All_Discounts[
                                                                        index]
                                                                    .name
                                                                    .toString();
                                                            open_discount_maxamount =
                                                                double.parse(_get_All_Discounts[
                                                                        index]
                                                                    .maxDiscountAmount
                                                                    .toString());

                                                            print("DISCOUNTCHECKED" +
                                                                value
                                                                    .toString() +
                                                                "--------" +
                                                                open_discount_type +
                                                                "-----" +
                                                                open_discount_id +
                                                                "-----" +
                                                                open_discount_name);
                                                          }),
                                                          /* onChanged: (ind){ setState(() => _get_All_Discounts[index].orderValue = ind);
                                            },*/
                                                          title: Text(
                                                              _get_All_Discounts[
                                                                      index]
                                                                  .type
                                                                  .toString()),
                                                        ));
                                                  },
                                                ),
                                              )),
                                        ),
                                      ),
                                      Container(
                                          margin:
                                              EdgeInsets.fromLTRB(0, 0, 50, 10),
                                          child: new Form(
                                              key: _formKey2,
                                              child: Expanded(
                                                  child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceEvenly,
                                                children: [
                                                  Container(
                                                    width: 100,
                                                    child: TextFormField(
                                                        /* validator: (val) {
                                                      if (val.isEmpty)
                                                        return 'Enter Amount';
                                                      return null;
                                                    },*/
                                                        controller:
                                                            text_discount_amount_controller,
                                                        obscureText: false,
                                                        keyboardType:
                                                            TextInputType
                                                                .number,
                                                        decoration:
                                                            InputDecoration(
                                                                filled: true,
                                                                fillColor:
                                                                    Colors
                                                                        .white,
                                                                hintText:
                                                                    "Discount",
                                                                contentPadding:
                                                                    EdgeInsets
                                                                        .only(
                                                                  bottom:
                                                                      20 / 2,
                                                                  left: 20 /
                                                                      2, // HERE THE IMPORTANT PART
                                                                  // HERE THE IMPORTANT PART
                                                                ),
                                                                border: OutlineInputBorder(
                                                                    borderRadius:
                                                                        BorderRadius.circular(
                                                                            5.0)))),
                                                  ),
                                                ],
                                              )))),
                                    ],
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                  ),
                                ),
                              ],
                            ))
                          : SizedBox(),
                    ],
                  ),
                ),
              ),
            ],
          ))))),
          Align(
              alignment: Alignment.bottomCenter,
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10, top: 10, right: 10),
                    child: TextField(
                      controller: text_description_controller,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          color: login_passcode_text,
                          fontSize: 14,
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.w400),
                      maxLines: 2,
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.all(20.0),
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0)),
                        hintText:
                            "Dressing on the side? No pickles? Let us know here.",
                      ),
                    ),
                  ),
                  Container(
                    // margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                    alignment: Alignment.topLeft,
                    child: Padding(
                      padding: EdgeInsets.only(left: 15, top: 10, right: 10),
                      child: Text(
                        "Quantity",
                        style: TextStyle(
                            color: login_passcode_text,
                            fontSize: 16,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w700),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Card(
                          color: Colors.white,
                          elevation: 5,
                          child: Row(
                            children: [
                              _decrementButton(),
                              Container(
                                  color: Colors.white,
                                  width: 40,
                                  child: Padding(
                                    padding: EdgeInsets.all(6),
                                    child: Text(
                                      '${counter_value}',
                                      style: TextStyle(
                                          color: login_passcode_text,
                                          fontSize: 16,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w600),
                                      textAlign: TextAlign.center,
                                    )
                                    /*Expanded(child: TextField(controller: cart_count_text_controller,decoration: InputDecoration(hintText: '${numbers[0]}'),style:TextStyle(
                                        color: login_passcode_text,
                                        fontSize: 16,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.w600) ,),)*/
                                    ,
                                  )),
                              _incrementButton(),
                            ],
                          ),
                        ),
                        Container(
                          alignment: Alignment.centerRight,
                          child: Padding(
                              padding: EdgeInsets.only(
                                left: 0,
                                right: 5,
                              ),
                              child: FlatButton(
                                  minWidth: 100,
                                  color: Colors.lightBlueAccent,
                                  child: Text("Add to cart",
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w600,
                                          color: addtocart_text)),
                                  onPressed: () {
                                    if (counter_value >= 1) {
                                      print(_group_checkslist);
                                      for (int i = 0;
                                          i < _group_checkslist.length;
                                          i++) {
                                        //print("GROUPCHECKS" +_group_checkslist[i].toString());
                                        for (int j = 0;
                                            j < _group_checkslist[i].length;
                                            j++) {
                                          if (_group_checkslist[i][j] == true) {
                                            modifier_arraylist.add(
                                                ModifierListApi(
                                                    widget
                                                        .modifierGroupList[i]
                                                        .modifierList[j]
                                                        .modifierId,
                                                    widget
                                                        .modifierGroupList[i]
                                                        .modifierList[j]
                                                        .modifierName,
                                                    widget
                                                        .modifierGroupList[i]
                                                        .modifierList[j]
                                                        .price));
                                          }
                                        }
                                      }

                                      var total_price =
                                          double.parse(main_item_price) *
                                              counter_value;

                                      print("TOTALPRICE-----" +
                                          total_price.toString() +
                                          "-------" +
                                          modifier_arraylist.length.toString());
                                      print("MAINITEMNAME-----" +
                                          main_item_name +
                                          "-------" +
                                          main_item_price);

                                      if (open_discount_type == "percentage") {
                                        discount_type_item = "item";
                                        discount_maxamount =
                                            open_discount_maxamount;

                                        total_discountprice = double.parse(
                                                main_item_price) *
                                            (double.parse(
                                                    text_discount_amount_controller
                                                        .text) /
                                                100);

                                        //total_discountitemprice = double.parse(main_item_price) - total_discountprice;
                                        discount_type = open_discount_type;
                                        discount_name = open_discount_name;
                                        discount_id = open_discount_id;
                                        print("MAXDISCOUNTBEFORE" +
                                            total_discountprice.toString() +
                                            "-----------" +
                                            discount_maxamount.toString());
                                        if (total_discountprice ==
                                            discount_maxamount) {
                                          total_discountprice = double.parse(
                                              discount_maxamount.toString());
                                          print("MAXDISCOUNTINSIDEIF" +
                                              total_discountprice.toString());
                                        } else {
                                          total_discountprice = double.parse(
                                                  main_item_price) *
                                              (double.parse(
                                                      text_discount_amount_controller
                                                          .text) /
                                                  100);
                                          print("MAXDISCOUNTINSIDEELSE" +
                                              total_discountprice.toString());
                                        }
                                        // total_discountprice_double = double.parse(total_discountprice.toString());
                                        // main_item_price = total_discountitemprice.toString();
                                      } else {
                                        discount_type_item = "fixed";
                                      }
                                      discount_type_item = "fixed";

                                      //TAX CALCULATION
                                      print("TAXINCLUDEOPTION" +
                                          widget.taxIncludeOption.toString());
                                      if (widget.taxIncludeOption == true) {
                                        if (widget.applicableTaxrates.length >
                                            0) {
                                          print("TAXRATELENGTH" +
                                              widget.applicableTaxrates.length
                                                  .toString());
                                          for (int g = 0;
                                              g <
                                                  widget.applicableTaxrates
                                                      .length;
                                              g++) {
                                            taxType = widget
                                                .applicableTaxrates[g].taxType
                                                .toString();
                                            var taxrate = widget
                                                .applicableTaxrates[g].taxRate
                                                .toString();
                                            if (taxType == "0") {
                                              taxType = "disable";
                                              taxTypeId = 0;
                                              tax = 0.00;
                                              taxType = taxTypeId.toString();
                                            } else if (taxType == "1") {
                                              print("TAXTYPE" + taxType);
                                              var taxRate = total_price *
                                                  (widget.applicableTaxrates[g]
                                                          .taxRate
                                                          .toDouble() /
                                                      100);

                                              print("ROUNDING" +
                                                  widget.applicableTaxrates[g]
                                                      .roundingOptions
                                                      .toString());
                                              if (widget.applicableTaxrates[g]
                                                      .roundingOptions ==
                                                  1) {
                                                print("NUMBERROUNDUP" +
                                                    taxRate.toString());
                                                print("ROUNDINGhalfEVEN" +
                                                    halfEven(taxRate, 2)
                                                        .toString());
                                                taxRate = halfEven(taxRate, 2);
                                              } else if (widget
                                                      .applicableTaxrates[g]
                                                      .roundingOptions ==
                                                  2) {
                                                print("ROUNDINGhalfup" +
                                                    halfUp(taxRate, 2)
                                                        .toString());
                                                taxRate = halfUp(taxRate, 2);
                                              } else if (widget
                                                      .applicableTaxrates[g]
                                                      .roundingOptions ==
                                                  3) {
                                                print("ROUNDINGdown" +
                                                    alwaysDown(taxRate, 2)
                                                        .toString());
                                                taxRate =
                                                    alwaysDown(taxRate, 2);
                                              } else if (widget
                                                      .applicableTaxrates[g]
                                                      .roundingOptions ==
                                                  4) {
                                                print("ROUNDINGALWAYSUP" +
                                                    getNumber(alwaysUp(taxRate),
                                                            precision: 2)
                                                        .toString());
                                                taxRate = getNumber(
                                                    alwaysUp(taxRate),
                                                    precision: 2);
                                              }

                                              taxType = "percent";
                                              taxTypeId = 1;
                                              taxType = taxTypeId.toString();
                                              tax = taxRate;
                                              print("TAXTYPE" +
                                                  taxType +
                                                  "=========" +
                                                  tax.toStringAsFixed(2) +
                                                  "========" +
                                                  widget.applicableTaxrates[g]
                                                      .taxid);
                                            } else if (taxType == "2") {
                                              taxType = "fixed";
                                              taxTypeId = 2;
                                              tax = total_price *
                                                  widget.applicableTaxrates[g]
                                                      .taxRate
                                                      .toDouble();
                                              taxType = taxTypeId.toString();
                                            } else if (taxType == "3") {
                                              taxType = "taxTable";
                                              taxTypeId = 3;
                                              if (widget.applicableTaxrates[g]
                                                      .taxTable.length >
                                                  0) {
                                                for (int h = 0;
                                                    h <
                                                        widget
                                                            .applicableTaxrates[
                                                                h]
                                                            .taxTable
                                                            .length;
                                                    h++) {
                                                  if (total_price >=
                                                          double.parse(widget
                                                              .applicableTaxrates[
                                                                  g]
                                                              .taxTable[h]
                                                              .from) &&
                                                      total_price <=
                                                          double.parse(widget
                                                              .applicableTaxrates[
                                                                  g]
                                                              .taxTable[h]
                                                              .to)) {
                                                    tax += double.parse(widget
                                                        .applicableTaxrates[g]
                                                        .taxTable[h]
                                                        .taxApplied);
                                                  }
                                                }
                                              } else {
                                                tax = 0.00;
                                              }
                                              taxType = taxTypeId.toString();
                                            }

                                            var sendTaxRates =
                                                SendTaxRatesModel(
                                                    widget.applicableTaxrates[g]
                                                        .enableTakeOutRate,
                                                    widget.applicableTaxrates[g]
                                                        .importId,
                                                    widget.applicableTaxrates[g]
                                                        .orderValue,
                                                    widget.applicableTaxrates[g]
                                                        .roundingOptions,
                                                    widget.applicableTaxrates[g]
                                                        .status,
                                                    widget.applicableTaxrates[g]
                                                        .taxName,
                                                    widget.applicableTaxrates[g]
                                                        .taxRate,
                                                    int.parse(taxType),
                                                    /* taxTypeId,*/
                                                    tax_table_arraylist,
                                                    widget.applicableTaxrates[g]
                                                        .taxid,
                                                    widget.applicableTaxrates[g]
                                                        .uniqueNumber,
                                                    tax);

                                            taxes_arraylist.add(sendTaxRates);
                                          }
                                        } else {
                                          taxes_arraylist = new List();
                                        }
                                      } else {
                                        taxes_arraylist = new List();
                                      }

                                      print("TAXESARRAYLIST" +
                                          taxes_arraylist.length.toString());

                                      getCartItemModelapi.add(MenuCartItemapi(
                                          widget.cart_item_id,
                                          widget.cart_menu_id,
                                          widget.cart_menugroup_id,
                                          main_item_name,
                                          main_item_price,
                                          counter_value.toString(),
                                          total_price.toString(),
                                          text_description_controller.text
                                              .toString(),
                                          modifier_arraylist,
                                          special_request_arraylist,
                                          "normal",
                                          discount_id,
                                          discount_name,
                                          discount_type_item,
                                          discount_type,
                                          0,
                                          total_discountprice,
                                          "",
                                          "",
                                          false,
                                          "",
                                          "",
                                          false,
                                          false,
                                          false,
                                          taxes_arraylist));
                                      //addcart_session.add(getCartItemModel);
                                      print("getCartItemModelapi" +
                                          getCartItemModelapi.length
                                              .toString());

                                      _savecartList(getCartItemModelapi);

                                      Navigator.pop(context, "Success");

                                      Toast.show("Added to Cart", context,
                                          duration: Toast.LENGTH_SHORT,
                                          gravity: Toast.BOTTOM);
                                    } else {
                                      Toast.show(
                                          "Minimum Cart Value is 1", context,
                                          duration: Toast.LENGTH_SHORT,
                                          gravity: Toast.BOTTOM);
                                    }
                                  })),
                        )
                      ],
                    ),
                  )
                ],
              ))
        ],
      ),
    );
  }

  bool isCurrentDateInRange(DateTime startDate, DateTime endDate) {
    final currentDate = DateTime.now();
    print(currentDate);
    DateTime  tempDatefrom = new DateFormat("HH:mm").parse(time);


  //  DateTime  tempDatefrom = new DateFormat("HH:mm").parse(DateTime.now().timeZoneName);
    return tempDatefrom.isAfter(startDate) && tempDatefrom.isBefore(endDate);
  }



  GetAllDiscountsApi(String item_id) {
    GetAllDiscountsItemRepository()
        .getalldiscountitems(item_id, widget.user_id, widget.restaurant_id)
        .then((result_allmenus) {
      setState(() {
        //isLoading = true;
        _get_All_Discounts = result_allmenus;
        discount_id_api = _get_All_Discounts[0].id;
        discount_type_api = _get_All_Discounts[0].type;
        print("GETALLDISCOUNTS" +
            discount_type +
            "----" +
            discount_id.toString());
      });
    });
  }
}

_savecartList(cartlist) async {
  final prefs = await SharedPreferences.getInstance();
  final key1 = 'save_to_cartlist';
  final cartlist_value = jsonEncode(cartlist);
  prefs.setString(key1, cartlist_value);
  print('savedCART $cartlist_value');
}

class CancelDialogs {
  static Future<void> showLoadingDialog(
      BuildContext context, GlobalKey key) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(
                  key: key,
                  backgroundColor: Colors.black54,
                  children: <Widget>[
                    Center(
                      child: Column(children: [
                        CircularProgressIndicator(),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Cancelling....",
                          style: TextStyle(color: Colors.lightBlueAccent),
                        )
                      ]),
                    )
                  ]));
        });
  }
}

class RadioListBuilder extends StatefulWidget {
  final int num;

  const RadioListBuilder({Key key, this.num}) : super(key: key);

  @override
  RadioListBuilderState createState() {
    return RadioListBuilderState();
  }
}

class RadioListBuilderState extends State<RadioListBuilder> {
  int value;

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
        child: Expanded(
          flex: 1,
          child: ListView.builder(
            itemCount: widget.num,
            itemBuilder: (context, index) {
              return SizedBox(
                  height: 40,
                  child: RadioListTile(
                    value: index,
                    groupValue: value,
                    onChanged: (value) {
                      (ind) => setState(() => value = ind);
                      /*setState(() {
                  (ind) => setState(() => value = ind);
                 // print("DISCOUNTCHECKED"+(ind) => setState(() => value = ind));
                });*/
                    },
                    title: Text(
                        /*new String.fromCharCodes(
                  new Runes(
                      '\u0024')) +*/
                        "$index"),
                  ));
            },
          ),
        ));
  }
}

/*class RadioListBuilder extends StatefulWidget {
  final int num;

  const RadioListBuilder({Key key, this.num}) : super(key: key);

  @override
  RadioListBuilderState createState() {
    return RadioListBuilderState();
  }
}

class RadioListBuilderState extends State<RadioListBuilder> {
  int value;

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(top:10,left: 10, right: 10,bottom: 10),
        child: Expanded(flex:1,child: ListView.builder(
          itemCount: widget.num,
          itemBuilder: (context, index) {
            return SizedBox(height:40,child: RadioListTile(
              value: index,
              groupValue: _get_All_Discounts[index].name,
              onChanged: (ind) => setState(() => _get_All_Discounts[index].name = ind),
              title: Text(new String.fromCharCodes(
                  new Runes(
                      '\u0024')) +_get_All_Discounts[index].maxDiscountAmount.toString()),
            ));
          },

        ),));
  }
}*/
