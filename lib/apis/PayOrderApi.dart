import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:waiter/model/loginresponse.dart';
import 'package:waiter/utils/all_constans.dart';
import 'package:http/http.dart' as http;
class PayOrderRepository {
  Future<loginresponse> payOrder(String posOrderId, String cashDrawerId,
      double amount, double amountTendered, int changeAmount,
      int paymentType, String createdBy, String restaurantId,int tableNumber,int noOfGuest,double tipAmount,String giftcardnumber,int preCardNo) async {
    var body = json.encode({
      'posOrderId': posOrderId,
      'cashDrawerId': cashDrawerId,
      'amount': amount,
      'amountTendered': amountTendered,
      'changeAmount': changeAmount,
      'paymentType': paymentType,
      'createdBy': createdBy,
      'restaurantId': restaurantId,
      'tableNumber': tableNumber,
      'noOfGuest': noOfGuest,
      'tipAmount': tipAmount,
      'giftCardNo': giftcardnumber,
      'preCardNo': preCardNo
    });
    print(base_url + "Payment_REQUEST" + body.toString());

    dynamic response = await Requests.post(base_url + "pay_order",
        body: body, headers: {'Content-type': 'application/json'});


    loginresponse loginresult_data =
    loginresponse.fromJson(jsonDecode(response));
    return loginresult_data;
  }
}
