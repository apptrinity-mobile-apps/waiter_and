class GetItemModifiersResponse {
  List<ModifiersGroupsListApi> modifiersGroupsList;
  int responseStatus;
  String result;

  GetItemModifiersResponse({this.modifiersGroupsList, this.responseStatus, this.result});

  GetItemModifiersResponse.fromJson(Map<String, dynamic> json) {
    if (json['modifiers_groups_list'] != null) {
      modifiersGroupsList = new List<ModifiersGroupsListApi>();
      json['modifiers_groups_list'].forEach((v) {
        modifiersGroupsList.add(new ModifiersGroupsListApi.fromJson(v));
      });
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.modifiersGroupsList != null) {
      data['modifiers_groups_list'] =
          this.modifiersGroupsList.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class ModifiersGroupsListApi {
  String itemImage;
  int maxSelections;
  int minSelections;
  String modifierGroupId;
  String modifierGroupName;
  String modifierGroupPrice;
  List<ModifierListApi> modifierList;
  int pricing;
  int quantity;

  ModifiersGroupsListApi(
      {this.itemImage,
        this.maxSelections,
        this.minSelections,
        this.modifierGroupId,
        this.modifierGroupName,
        this.modifierGroupPrice,
        this.modifierList,
        this.pricing,
        this.quantity});

  ModifiersGroupsListApi.fromJson(Map<String, dynamic> json) {
    itemImage = json['itemImage'];
    maxSelections = json['maxSelections'];
    minSelections = json['minSelections'];
    modifierGroupId = json['modifierGroupId'];
    modifierGroupName = json['modifierGroupName'];
    modifierGroupPrice = json['modifierGroupPrice'];
    if (json['modifiersList'] != null) {
      modifierList = new List<ModifierListApi>();
      json['modifiersList'].forEach((v) {
        modifierList.add(new ModifierListApi.fromJson(v));
      });
    }
    pricing = json['pricing'];
    quantity = json['quantity'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['itemImage'] = this.itemImage;
    data['maxSelections'] = this.maxSelections;
    data['minSelections'] = this.minSelections;
    data['modifierGroupId'] = this.modifierGroupId;
    data['modifierGroupName'] = this.modifierGroupName;
    data['modifierGroupPrice'] = this.modifierGroupPrice;
    if (this.modifierList != null) {
      data['modifierList'] = this.modifierList.map((v) => v.toJson()).toList();
    }
    data['pricing'] = this.pricing;
    data['quantity'] = this.quantity;
    return data;
  }
}

class ModifierListApi {
  String modifierId;
  String modifierName;
  String modifierTotalPrice;

  ModifierListApi(this.modifierId, this.modifierName, this.modifierTotalPrice);

  ModifierListApi.fromJson(Map<String, dynamic> json) {
    modifierId = json['modifierId'];
    modifierName = json['modifierName'];
    modifierTotalPrice = json['modifierTotalPrice'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['modifierId'] = this.modifierId;
    data['modifierName'] = this.modifierName;
    data['modifierTotalPrice'] = this.modifierTotalPrice;
    return data;
  }


}

class SpecialRequestListApi {
  String name;
  int requestPrice;

  SpecialRequestListApi(this.name, this.requestPrice);

  SpecialRequestListApi.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    requestPrice = json['requestPrice'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['requestPrice'] = this.requestPrice;
    return data;
  }
}