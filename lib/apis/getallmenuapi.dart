import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:waiter/model/getallmenus.dart';
import 'package:waiter/utils/all_constans.dart';

class GetAllMenu {
  Future<List<MenuList>> getmenu(
      String userid,
      String res_id,
      ) async {

    var body = json.encode({
      'userId': userid,
      'restaurantId': res_id
    });
    print(body);
    dynamic response = await Requests.post(base_url + "get_all_menus",
        body: body, headers: {'Content-type': 'application/json'});

    final res = json.decode(response);
    Iterable list = res['menuList'];
    return list.map((model) => MenuList.fromJson(model)).toList();

  }
}