import 'dart:ui';

import 'package:async/async.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pinput/pin_put/pin_put.dart';
import 'package:toast/toast.dart';
import 'package:waiter/session/userRepository.dart';
import 'package:waiter/utils/DialogClass.dart';
import 'package:waiter/utils/all_constans.dart';
import 'package:waiter/utils/sizeconfig.dart';

import 'login.dart';

class RestauranLoginPaascode extends StatefulWidget {

  final otp_from_api;
  final restaurant_id_from_api;
  const RestauranLoginPaascode(this.otp_from_api,this.restaurant_id_from_api, {Key key})
      : super(key: key);

  @override
  _RestauranLoginPaascodeState createState() => _RestauranLoginPaascodeState();
}



class _RestauranLoginPaascodeState extends State<RestauranLoginPaascode> {
  final _formKey = GlobalKey<FormState>();
  final _pinPutFocusNode = FocusNode();
  final _pinPutController = TextEditingController();
  CancelableOperation cancelableOperation;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  var passval_bool = false;
  var passval_otp = "";
  double logomargin_height;
  @override
  void initState() {
    super.initState();
    print("Height"+SizeConfig.screenHeight.toString());
    if(SizeConfig.screenHeight > 580 && SizeConfig.screenHeight < 800){
      logomargin_height = 120;
    }
    if(SizeConfig.screenHeight > 800){
      logomargin_height = 220;
    }
    print("APIOTP "+widget.otp_from_api.toString()+"-----"+widget.restaurant_id_from_api);


    //Auto Set otp For Pinput
    _pinPutController.text =widget.otp_from_api.toString();
    if(_pinPutController.text.length == 6){
      passval_bool = true;
      passval_otp = widget.otp_from_api.toString();
    }


  }

  final BoxDecoration pinPutDecoration = BoxDecoration(
    color: const Color.fromRGBO(235, 236, 237, 1),
    borderRadius: BorderRadius.circular(0.0),
  );

  TextEditingController username_Controller = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {


    return Scaffold(
      backgroundColor: Colors.black,
      body: /*new Stack(
        children: <Widget>[
          new Container(
            decoration: new BoxDecoration(
              image: new DecorationImage(
                image: new AssetImage("images/splash_bg.png"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          new Center(
            child: Expanded(
                child: Column(
              children: [
                Container(
                    margin: EdgeInsets.fromLTRB(50, 175, 0, 0),
                    alignment: Alignment.centerLeft,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image(
                          image: AssetImage('images/logo_dashboard.png'),
                          //fit: BoxFit.fitWidth,
                          //width: double.infinity,
                        ),
                        Padding(
                            padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                            child: Text(
                              'The Waiter App',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.normal,
                                  fontSize: 20.0),
                            ))
                      ],
                    )),

                Container(
                  margin: EdgeInsets.fromLTRB(30, 20, 30, 0),
                  child: Column(
                    children: [_passcode_widget()],
                  ),
                )
              ],
            )),
          ),

        ],
      )*/ Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
            image: DecorationImage(
                image: ExactAssetImage("images/splash_bg.png"),
                fit: BoxFit.cover)),
        child: SingleChildScrollView(child:Column(
          // shrinkWrap: true,
          children: [
            Container(
                margin: EdgeInsets.fromLTRB(40, logomargin_height, 0, 0),
                alignment: Alignment.centerLeft,
                //height: MediaQuery.of(context).size.height * 1.0,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image(
                      image: AssetImage('images/logo_dashboard.png'),
                      //fit: BoxFit.fitWidth,
                      width: 150,
                      height: 110,
                    ),
                    Padding(
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: Text(
                          'The Waiter App',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: SizeConfig.blockSizeHorizontal*4,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w400,
                          ),
                        )),

                  ],
                )),
            Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.fromLTRB(50.00, 40.00, 0, 0.00),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'ONE TIME PASSWORD',
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Poppins',
                        fontSize: SizeConfig.blockSizeHorizontal*4,
                        fontWeight: FontWeight.w700,),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(50.00, 5.00, 50.00, 0.00),
                    child: Text(
                      'Please enter the high security password sent to your registered Mobile number or Email ID',
                      style: TextStyle(
                          color: Colors.white,
                        fontSize: SizeConfig.blockSizeHorizontal*2.8,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w400,),
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
                      alignment: Alignment.center,
                      //height: MediaQuery.of(context).size.height * 1.0,
                      child: Column(
                        children: [
                          /*Container(
                              margin: EdgeInsets.fromLTRB(35, 0, 35, 0),
                              alignment: Alignment.topLeft,
                              child: Text(
                                'Restaurant code',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: SizeConfig.blockSizeHorizontal*3.2,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w400,
                                ),
                              )),*/
                          Container(
                            margin: EdgeInsets.fromLTRB(35, 5, 35, 0),
                            child: _passcode_widget(),
                          )
                        ],
                      )),

                ],
              ),
            )
          ],
        )),
      ),
    );
  }

  Widget _passcode_widget() {
    return Container(
        child: Column(children: [
      Container(
          margin: EdgeInsets.fromLTRB(0.00, 0.00, 0.00, 0.00),
          child: GestureDetector(
            onLongPress: () {
              print(_formKey.currentState.validate());
            },
            child: PinPut(
              //autovalidateMode: AutovalidateMode.onUserInteraction,
              animationCurve:Curves.linear,
              animationDuration:const Duration(milliseconds: 160),
              pinAnimationType:PinAnimationType.slide,
              separator:const SizedBox(width: 1.0),
              withCursor: false,
              fieldsCount: 6,
              fieldsAlignment: MainAxisAlignment.center,
              textStyle: const TextStyle(
                fontSize: 25.0,
                color: login_passcode_star,
              ),
              //  eachFieldMargin: EdgeInsets.all(0),
              eachFieldWidth: 50.0,
              eachFieldHeight: 50.0,
              //  onSubmit: (String pin) => _showSnackBar(pin),
              focusNode: _pinPutFocusNode,
              controller: _pinPutController,
              submittedFieldDecoration: pinPutDecoration.copyWith(
                color: Colors.white,
                border: Border.all(
                  width: 0,
                  //color: const Color.fromRGBO(160, 215, 220, 1),
                ),
              ),
              selectedFieldDecoration: pinPutDecoration.copyWith(
                color: Colors.white,
                border: Border.all(
                  width: 0,
                  color: login_passcode_box,
                ),
              ),
              followingFieldDecoration: pinPutDecoration.copyWith(
                color: Colors.white,
                border: Border.all(
                  width: 0,
                  color: login_passcode_box,
                ),
              ),
              //pinAnimationType: PinAnimationType.fade,
              obscureText: "*",
              onChanged: (value) {
                if (value.length == 6) {
                  passval_bool = true;
                  passval_otp = value;
                  print("PASSWORD " + value);
                }
              },
            ),
          )),
      Container(
          margin: EdgeInsets.fromLTRB(15, 15, 15, 0),
          height: 50,
          width: double.infinity,
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: login_passcode_bg1,
              borderRadius: BorderRadius.circular(0)),
          child: InkWell(
              child: FlatButton(
                  minWidth: double.infinity,
                  height: double.infinity,
                  child: Text("SUBMIT",
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.w800,
                          color: Colors.white)),
                  onPressed: () {
                    {
                      SignDialogs.showLoadingDialog(
                          context, "Signing in", _keyLoader);
                      cancelableOperation?.cancel();
                      CancelableOperation.fromFuture(
                          Future.delayed(Duration(seconds: 1), () {

                        if (passval_bool == true) {
                          if(passval_otp == widget.otp_from_api){
                            UserRepository.save_otp(widget.restaurant_id_from_api);
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => MyApp()),
                            );
                          }else{
                            Navigator.of(_keyLoader.currentContext,
                                rootNavigator: true)
                                .pop();
                            Toast.show("Enter Correct Otp", context,
                                duration: Toast.LENGTH_SHORT,
                                gravity: Toast.BOTTOM);
                          }

                        } else {
                          Navigator.of(_keyLoader.currentContext,
                                  rootNavigator: true)
                              .pop();
                          Toast.show("Enter Otp", context,
                              duration: Toast.LENGTH_SHORT,
                              gravity: Toast.BOTTOM);
                        }
                      }));
                    }
                  })))
    ]));
  }

  void _showToast(BuildContext context) {
    final scaffold = Scaffold.of(context);
    scaffold.showSnackBar(
      SnackBar(
        content: const Text('Added to favorite'),
        action: SnackBarAction(
            label: 'UNDO', onPressed: scaffold.hideCurrentSnackBar),
      ),
    );
  }
}
