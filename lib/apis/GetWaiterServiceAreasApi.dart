import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:waiter/model/getalldinningoptionsresponse.dart';
import 'package:waiter/model/getwaiterservicearearesponse.dart';
import 'package:waiter/model/loginresponse.dart';
import 'package:waiter/model/waitergenerateotp.dart';
import 'package:waiter/utils/all_constans.dart';
import 'package:http/http.dart' as http;
class GetWaiterServiceAreasApiRepository {

  Future<List<ServiceAreasList>> getWaiterServiceArea(String restaurant_id) async {
    var body = json.encode({'restaurantId':restaurant_id});
    print(base_url + "get_waiter_service_area"+ body.toString());

    dynamic response = await Requests.post(base_url + "get_waiter_service_areas",
        body: body, headers: {'Content-type': 'application/json'});

    print(base_url + "WAITERSERVICEAREARESPONSE"+ response.toString());
    final res = json.decode(response);
    Iterable list = res['service_areasList'];
    return list.map((model) => ServiceAreasList.fromJson(model)).toList();

  }


}


/*
class LoginRepository {
  Future checklogin(String email, String password) async {
    Map<String, dynamic> body = {'email': email, 'password': password};
    print("reviworder----email " + email + " password " + password);

    final response = await http.post(base_url + "userLogin",
        body: body,
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/x-www-form-urlencoded"
        },
        encoding: Encoding.getByName("utf-8"));

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      print("login page " + response.body);
      Map<String, dynamic> user = json.decode(response.body);
      if (user['response'] == true) {
        //print("login page "+user['userId']);

        return user['userId'];
      } else {
        return "failed";
        // print("Failed ");
      }
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }
}*/
