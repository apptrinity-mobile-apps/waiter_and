import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:page_indicator/page_indicator.dart';
import 'package:toast/toast.dart';
import 'package:waiter/payment2.dart';
import 'package:waiter/receipt.dart';
import 'package:waiter/session/userRepository.dart';
import 'package:waiter/splitorderscreen.dart';
import 'package:waiter/utils/all_constans.dart';
import 'package:waiter/utils/sizeconfig.dart';

import 'addfood_screen.dart';
import 'apis/getTableInformationApi.dart';
import 'model/gettableinformationresponse.dart';

class GetTableInformation extends StatefulWidget {

  final String orderId;
  final String tableNumber;
  const GetTableInformation(this.orderId,this.tableNumber, {Key key})
      : super(key: key);
  @override
  _GetTableInformationState createState() => _GetTableInformationState();
}

class _GetTableInformationState extends State<GetTableInformation> {

  PageController controller;

  GlobalKey<PageContainerState> key = GlobalKey();
  int selected_pos = -1;


  List<TableInfo> __cart_items_list = new List();
  int split_length = 0;
  double _itemsubTotal = 0.0;
  var _itemGrandTotalPrice = "";
  var cart_count_value = 1;
  int cart_count = 0;
  var table_number ="";
  var order_id ="";
  var no_of_guest = "";
  var restaurant_id = "";
  var employee_id = "";
  double screenheight = 0.0;
  double cardview_height = 0.0;
  double toolbar_height = 0.0;
  bool _loading = true;
  String selected_order_id = "";
  String selected_unique_id = "";
  String grand_total = "";
  String check_payment_status = "";
  String receipt_no = "";
  String check_payment_type = "";
  String payment_status = "";
  TextEditingController tablenumber_Controller = TextEditingController();
  TextEditingController no_of_guest_Controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    // print(widget.main_order_id.toString());
    controller = PageController();
    TextEditingController grand_total_text_controller = new TextEditingController();

    print("Height"+SizeConfig.screenHeight.toString());
    if(SizeConfig.screenHeight >= 580 && SizeConfig.screenHeight < 800){
      toolbar_height = 56;
      screenheight = 460.0;
      cardview_height = 245.0;
    }
    if(SizeConfig.screenHeight >= 800){
      toolbar_height = 66;
      screenheight = 680.0;
      cardview_height = 450.0;
    }
    UserRepository().getuserdetails().then((userdetails) {
      print("userdata" + userdetails.length.toString());
      setState(() {
        employee_id = userdetails[0];
        //user_id = userdetails[1];
        UserRepository().getGenerateOtpDetails().then((restaurantdetails) {
          setState(() {
            restaurant_id = restaurantdetails[0];

            print("TABLEINFORMATION"+"TABLENUMBER==="+widget.orderId.toString()+" EMPLOYEEID==="+employee_id+" RESTAURANTID===="+restaurant_id);

            Future.delayed(Duration(seconds: 2), () async {
              setState(() {
                GetTableInformationApiRepository().checktableinformation(widget.orderId,employee_id,restaurant_id).then((value){
                  print("TABLEINFORMATION RESPSTATUS  "+value.responseStatus.toString());
                  debugPrint(base_url + ""+ value.result.toString());
                  if(value.responseStatus == 1){
                    setState(
                            () {
                          _loading = false;
                          //split_length = value.tableInfo.length;
                          table_number = value.orderInfo[0].tableNumber.toString();

                          order_id = value.orderInfo[0].orderId.toString();
                          no_of_guest = value.orderInfo[0].noOfGuest.toString();
                          tablenumber_Controller.text = table_number;
                          no_of_guest_Controller.text = no_of_guest;

                          print(table_number+"======"+no_of_guest+"===="+tablenumber_Controller.text+"_____"+no_of_guest_Controller.text.toString());
                          if(no_of_guest == null || no_of_guest == "null"){
                            no_of_guest = "0";
                          }
                          //_itemsubTotal = double.parse(value.tableInfo.subTotal);
                          //_itemGrandTotalPrice = value.tableInfo.totalAmount.toString() ;
                          //_itemsTotal = int.parse(value.tableInfo.totalAmount);

                          for (int t = 0; t < value.orderInfo.length; t++) {

                            __cart_items_list.add(value.orderInfo[t]);
                            print("itemslength"+value.orderInfo[t].itemsList.length.toString() +""+ payment_status);
                            if(value.orderInfo[t].itemsList.length>0){
                              split_length = split_length + 1;
                            }
                            check_payment_status = __cart_items_list[t].paymentStatus.toString();
                            receipt_no = __cart_items_list[t].receiptNumber.toString();
                          }



                          print("Table info length"+split_length.toString()+"------"+__cart_items_list.length.toString());

                          Toast.show("SUCCESS TABLE INFORMATION", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
                        }
                    );
                  }else if(value.responseStatus == 3){
                    setState(
                        (){
                          _loading = false;
                          Toast.show("NoneType' object has no attribute 'id'", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
                        });


                  } else if(value.responseStatus == 0){
                    setState(() {
                      _loading = false;
                      Toast.show("Invalid Credentials", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
                    });
                  }
                });

              });
            });



          });
        });
      });
    });


  }

  void refresh() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: toolbar_height,
        automaticallyImplyLeading: false,
        elevation: 0.0,
        backgroundColor: Colors.white,
        centerTitle: false,
        title: Text("Order",
            style: new TextStyle(
                color: login_passcode_text,
                fontSize: 18.0,
                fontWeight: FontWeight.w700)),
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              padding: EdgeInsets.only(left: 10.0),
              icon: Image.asset("images/back_arrow.png", width: 22, height: 22),
              onPressed: () {
                Navigator.of(context).pop();
              },
            );
          },
        ),
        actions: [
          Container(
              margin: const EdgeInsets.fromLTRB(15,15,24,15),
              height: 35,
              padding: const EdgeInsets.fromLTRB(10,0,10,0),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: dashboard_bg,
                  borderRadius: BorderRadius.circular(0)),
              child: InkWell(
                  child:Text("Split",
                      style: TextStyle(
                          fontSize:SizeConfig.safeBlockHorizontal * 4,
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.w400,
                          color: text_split)),
                  onTap: () {
                    print("SPLITORDERID"+order_id);
                    Navigator.push(
                        context,_createRoute(order_id));
                  }))
        ],
      ),
      bottomSheet:  Container(height:50,child: check_payment_status == "0" ? Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [Expanded(child:Container(
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: dashboard_quick_order,
                borderRadius: BorderRadius.circular(0)),
            child: InkWell(
                child:Text("ADD ORDER",
                    style: TextStyle(
                        fontSize:SizeConfig.safeBlockHorizontal * 4,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w800,
                        color: Colors.white)),
                onTap: () {

                  print("selected_order_id--"+selected_order_id);
                  UserRepository.save_OrderId(selected_order_id);
                  UserRepository.save_TablenumbernGuests(table_number, no_of_guest);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              AddFood(false,table_number,no_of_guest)));
                }))),
          Expanded(child:Container(
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: login_passcode_bg1,
                  borderRadius: BorderRadius.circular(0)),
              child: InkWell(
                  child: FlatButton(
                      child: Text("BILL PAY",
                          style: TextStyle(
                              fontSize:
                              SizeConfig.safeBlockHorizontal * 4,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w800,
                              color: Colors.white)),
                      onPressed: () {
                        var header_bool = false;
                        print("PAYMENTSCREEN"+selected_order_id+"---"+grand_total+"---"+tablenumber_Controller.text.toString()+"-----"+no_of_guest_Controller.text.toString());
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Payment2(tablenumber_Controller.text.toString(),no_of_guest_Controller.text.toString(),selected_order_id,grand_total.toString(),selected_unique_id)));
                      }))))],):Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [Expanded(child:Container(
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: dashboard_quick_order,
                borderRadius: BorderRadius.circular(0)),
            child: InkWell(
                child:Text("Reciept",
                    style: TextStyle(
                        fontSize:SizeConfig.safeBlockHorizontal * 4,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w800,
                        color: Colors.white)),
                onTap: () {
                  //TODO add Reciept Number
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              Receipt(receipt_no)));
                }))),
          ],)),
      body: _loading
          ? Center(
        child: SpinKitFadingCircle(color: Colors.lightBlueAccent),
      )
          :Container(
        //padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
        color: Colors.white,
        child: Directionality(
          textDirection: TextDirection.ltr,
          child: Column(
            children: <Widget>[
              Container(
                color: dashboard_bg,
                child: SingleChildScrollView(child:Container(
                  height: screenheight,
                  child: PageIndicatorContainer(
                    key: key,
                    child: PageView.builder(
                      onPageChanged: (value) {
                        print(value);
                        setState(() {
                          check_payment_status = __cart_items_list[value].paymentStatus.toString();
                          receipt_no = __cart_items_list[value].receiptNumber.toString();
                        print("slide_PAYMENTSTATUS-"+__cart_items_list[value].paymentStatus.toString()+"======"+receipt_no);
                        });
                      },
                      itemCount: split_length,
                      physics: BouncingScrollPhysics(),
                      itemBuilder: (BuildContext context, int pindex) {
                        return  Container(
                            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                            color: Colors.white,
                            child: Column(
                              children: [
                                Container(
                                  color: dashboard_bg,
                                  margin: EdgeInsets.only(left: 0, right: 0),
                                  padding:
                                  EdgeInsets.symmetric(horizontal: 12.0, vertical: 12.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          new Image.asset(
                                            "images/round_table.png",
                                            height: 46,
                                            width: 46,
                                          ),
                                          Padding(
                                            padding: EdgeInsets.only(left: 8, bottom: 0),
                                            child: Column(children: [Text("Table no",
                                                style: TextStyle(
                                                    color: login_passcode_bg1,
                                                    fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                    fontFamily: 'Poppins',
                                                    fontWeight: FontWeight.w600)),

                                              Container(
                                                width: 40,
                                                height: 20,// do it in both Container
                                                child: TextField( controller: tablenumber_Controller,
                                                  keyboardType: TextInputType.number,
                                                    decoration: InputDecoration(
                                                        filled: false,
                                                        fillColor: Colors.white,
                                                        hintText: table_number,
                                                        hintStyle: TextStyle(color: text_split, fontSize: SizeConfig.blockSizeHorizontal *3,
                                                          fontFamily: 'Poppins',
                                                          fontWeight: FontWeight.w400,
                                                        ),
                                                        contentPadding: EdgeInsets.only(
                                                          bottom: 0 / 2,
                                                          left: 25 / 2,
                                                          // HERE THE IMPORTANT PART
                                                        ),
                                                        border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0)))
                                                ),
                                              ),

                                            ],)/*RichText(
                                                text: TextSpan(children: [
                                                  TextSpan(
                                                      text: "Table no\n",
                                                      style: TextStyle(
                                                          color: text_split,
                                                          fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                          fontFamily: 'Poppins',
                                                          fontWeight: FontWeight.w400)),


                                                  TextSpan(
                                                      text: table_number,
                                                      style: new TextStyle(
                                                          fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                          color: text_split,
                                                          fontFamily: 'Poppins',
                                                          fontWeight: FontWeight.w700))
                                                ]))*/,
                                          ),
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          new Image.asset(
                                            "images/round_customers_icon.png",
                                            height: 46,
                                            width: 46,
                                          ),
                                          Padding(
                                            padding: EdgeInsets.only(left: 8, bottom: 0),
                                            child: Column(children: [Text("Customers",
                                                style: TextStyle(
                                                    color: login_passcode_bg1,
                                                    fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                    fontFamily: 'Poppins',
                                                    fontWeight: FontWeight.w600)),
                                              Container(
                                                width: 40,
                                                height: 20, // do it in both Container
                                                child: TextField( controller: no_of_guest_Controller,
                                                  keyboardType: TextInputType.number,
                                                    decoration: InputDecoration(
                                                        filled: false,
                                                        fillColor: Colors.white,
                                                        hintText: no_of_guest,
                                                        hintStyle: TextStyle(color: text_split, fontSize: SizeConfig.blockSizeHorizontal *3,
                                                          fontFamily: 'Poppins',
                                                          fontWeight: FontWeight.w400,),
                                                        contentPadding: EdgeInsets.only(
                                                          bottom: 0 / 2,
                                                          left: 25 / 2, // HERE THE IMPORTANT PART
                                                          // HERE THE IMPORTANT PART
                                                        ),
                                                        border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0)))
                                                ),
                                              ),

                                            ],)

                                            /*RichText(
                                                text: TextSpan(children: [
                                                  TextSpan(
                                                      text: "Customers\n",
                                                      style: TextStyle(
                                                          color: text_split,
                                                          fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                          fontFamily: 'Poppins',
                                                          fontWeight: FontWeight.w400)),
                                                  TextSpan(
                                                      text: no_of_guest,
                                                      style: new TextStyle(
                                                          fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                          color: text_split,
                                                          fontFamily: 'Poppins',
                                                          fontWeight: FontWeight.w700))
                                                ]))*/,
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                    color: dashboard_bg,
                                    //margin: EdgeInsets.all(15),
                                    height: cardview_height,
                                    child: Card(
                                      margin: EdgeInsets.only(left:10,right:10),
                                      elevation: 5,
                                      child: Container(
                                        color: Colors.white,
                                        child: Column(children: [
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            children: <Widget>[
                                              Padding(
                                                padding: EdgeInsets.only(
                                                    top: 10.0, left: 20, bottom: 0,right: 35),
                                                child: Text("Orders",
                                                    style: TextStyle(
                                                        color: cart_text,
                                                        fontSize: SizeConfig.safeBlockHorizontal * 5,
                                                        fontFamily: 'Poppins',
                                                        fontWeight: FontWeight.w600)),
                                              ),
                                            ],
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: <Widget>[Column(children: [ Padding(
                                              padding: EdgeInsets.only(
                                                  top: 10.0, left: 20, bottom: 0,right: 35),
                                              child: Text("Order Id",
                                                  style: TextStyle(
                                                      color: cart_text,
                                                      fontSize: SizeConfig.safeBlockHorizontal * 3,
                                                      fontFamily: 'Poppins',
                                                      fontWeight: FontWeight.w600)),
                                            ),Padding(
                                              padding: EdgeInsets.only(
                                                  top: 0.0, left: 20, bottom: 0,right: 35),
                                              child: Text("#"+__cart_items_list[pindex].orderUniqueId.toString(),
                                                  style: TextStyle(
                                                      color: login_passcode_text,
                                                      fontSize: SizeConfig.safeBlockHorizontal * 3.5,
                                                      fontFamily: 'Poppins',
                                                      fontWeight: FontWeight.w600)),
                                            ),],),Column(children: [ Padding(
                                              padding: EdgeInsets.only(
                                                  top: 10.0, left: 20, bottom: 0,right: 20),
                                              child: Text("CheckNumber",
                                                  style: TextStyle(
                                                      color: cart_text,
                                                      fontSize: SizeConfig.safeBlockHorizontal * 3,
                                                      fontFamily: 'Poppins',
                                                      fontWeight: FontWeight.w600)),
                                            ),Padding(
                                              padding: EdgeInsets.only(
                                                  top: 0.0, left: 20, bottom: 0,right: 20),
                                              child: Text(__cart_items_list[pindex].checkNumber.toString(),
                                                  style: TextStyle(
                                                      color: login_passcode_text,
                                                      fontSize: SizeConfig.safeBlockHorizontal * 3.5,
                                                      fontFamily: 'Poppins',
                                                      fontWeight: FontWeight.w600)),
                                            ),],)

                                            ],
                                          ),
                                          Container(
                                            margin: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 0.0),
                                            child: Divider(
                                              color: cart_viewline,
                                            ),
                                          ),
                                          Expanded(
                                            child: ListView.builder(
                                                shrinkWrap: true,
                                                physics: ClampingScrollPhysics(),
                                                scrollDirection: Axis.vertical,
                                                itemCount: __cart_items_list[pindex].itemsList.length,
                                                itemBuilder: (context, index) {
                                                  selected_order_id = __cart_items_list[pindex].orderId;
                                                  selected_unique_id = __cart_items_list[pindex].orderUniqueId;
                                                  grand_total = __cart_items_list[pindex].totalAmount;

                                                  print("CHECKGRANDTOTAL"+grand_total.toString()+"----"+check_payment_status.toString()+"-----"+selected_order_id);
                                                  return Container(
                                                      margin: EdgeInsets.all(5),
                                                      width:
                                                      MediaQuery.of(context).size.width * 0.3,
                                                      child: Column(children: [
                                                        Row(
                                                          mainAxisAlignment:
                                                          MainAxisAlignment.spaceBetween,
                                                          children: <Widget>[
                                                            __cart_items_list[pindex].itemsList[index].itemStatus == 1?
                                                            Expanded(
                                                              flex: 0,
                                                              child: Padding(
                                                                  padding:
                                                                  EdgeInsets.only(left: 5,right: 5),child:Image.asset(
                                                                'images/check_fulfill.png',
                                                                height: 20,
                                                                width: 20,
                                                              ))):SizedBox(),
                                                            Expanded(flex:3,child: Padding(
                                                                padding:
                                                                EdgeInsets.only(left: 0,right: 15),
                                                                child: Text(
                                                                  __cart_items_list[pindex].itemsList[index].itemName.toString(),
                                                                  style: TextStyle(
                                                                      color:
                                                                      login_passcode_text,
                                                                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                                      fontFamily: 'Poppins',
                                                                      fontWeight:
                                                                      FontWeight.w600),
                                                                  textAlign: TextAlign.start,
                                                                )),),

                                                            Padding(
                                                                padding:
                                                                EdgeInsets.only(left: 15,right: 0),
                                                                child: Text(
                                                                  "x "+__cart_items_list[pindex].itemsList[index].quantity.toString(),
                                                                  style: TextStyle(
                                                                      color:
                                                                      coupontext,
                                                                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                                      fontFamily: 'Poppins',
                                                                      fontWeight:
                                                                      FontWeight.w400),
                                                                  textAlign: TextAlign.start,
                                                                )),
                                                            Padding(
                                                                padding:
                                                                EdgeInsets.only(left: 15,right: 15),
                                                                child: Text(
                                                                  new String.fromCharCodes(
                                                                      new Runes(
                                                                          '\u0024')) +(__cart_items_list[pindex].itemsList[index].quantity * double.parse(__cart_items_list[pindex].itemsList[index].unitPrice)).toString(),
                                                                  style: TextStyle(
                                                                      color:
                                                                      login_passcode_text,
                                                                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                                      fontFamily: 'Poppins',
                                                                      fontWeight:
                                                                      FontWeight.w700),
                                                                  textAlign: TextAlign.start,
                                                                )),

                                                          ],
                                                        ),

                                                        __cart_items_list[pindex].itemsList[index].modifiersList.length>0?ListView.builder(
                                                            shrinkWrap: true,
                                                            physics: ClampingScrollPhysics(),
                                                            scrollDirection: Axis.vertical,
                                                            itemCount: __cart_items_list[pindex].itemsList[index].modifiersList.length,
                                                            itemBuilder: (context, i) {
                                                              return Container(
                                                                margin: EdgeInsets.all(2),
                                                                width:
                                                                MediaQuery.of(context).size.width * 0.3,
                                                                child: Row(
                                                                  mainAxisAlignment:
                                                                  MainAxisAlignment.spaceBetween,
                                                                  children: <Widget>[
                                                                    Padding(
                                                                        padding:
                                                                        EdgeInsets.only(left: 30,right: 20),
                                                                        child: Text(
                                                                          __cart_items_list[pindex].itemsList[index].modifiersList[i].modifierName.toString(),
                                                                          style: TextStyle(
                                                                              color:
                                                                              coupontext,
                                                                              fontSize: SizeConfig.safeBlockHorizontal * 3.5,
                                                                              fontFamily: 'Poppins',
                                                                              fontWeight:
                                                                              FontWeight.w400),
                                                                          textAlign: TextAlign.start,
                                                                        )),
                                                                    Padding(
                                                                      padding:
                                                                      EdgeInsets.only(left: 10,right: 20),
                                                                      child: Text(
                                                                        new String.fromCharCodes(
                                                                            new Runes(
                                                                                '\u0024')) +__cart_items_list[pindex].itemsList[index].modifiersList[i].modifierTotalPrice.toString(),
                                                                        style: TextStyle(
                                                                            color:
                                                                            coupontext,
                                                                            fontSize: 13,
                                                                            fontFamily: 'Poppins',
                                                                            fontWeight:
                                                                            FontWeight.w400),
                                                                        textAlign: TextAlign.right,
                                                                      ),
                                                                    ),
                                                                  ],

                                                                ),
                                                              );
                                                            }):SizedBox(),
                                                        __cart_items_list[pindex].itemsList[index].specialRequestList.length>0?ListView.builder(
                                                            shrinkWrap: true,
                                                            physics: ClampingScrollPhysics(),
                                                            scrollDirection: Axis.vertical,
                                                            itemCount: __cart_items_list[pindex].itemsList[index].specialRequestList.length,

                                                            itemBuilder: (context, i) {
                                                              return Container(
                                                                margin: EdgeInsets.all(2),
                                                                child: Row(
                                                                  mainAxisAlignment:
                                                                  MainAxisAlignment.spaceBetween,
                                                                  children: <Widget>[
                                                                    Padding(
                                                                        padding:
                                                                        EdgeInsets.only(left: 30,right: 20),
                                                                        child: Text(
                                                                          __cart_items_list[pindex].itemsList[index].specialRequestList[i].name.toString(),
                                                                          style: TextStyle(
                                                                              color:
                                                                              coupontext,
                                                                              fontSize: 13,
                                                                              fontFamily: 'Poppins',
                                                                              fontWeight:
                                                                              FontWeight.w400),
                                                                          textAlign: TextAlign.start,
                                                                        )),
                                                                    Padding(
                                                                      padding:
                                                                      EdgeInsets.only(left: 10,right: 20),
                                                                      child: Text(
                                                                        new String.fromCharCodes(
                                                                            new Runes(
                                                                                '\u0024')) +__cart_items_list[pindex].itemsList[index].specialRequestList[i].requestPrice.toString(),
                                                                        style: TextStyle(
                                                                            color:
                                                                            coupontext,
                                                                            fontSize: 13,
                                                                            fontFamily: 'Poppins',
                                                                            fontWeight:
                                                                            FontWeight.w400),
                                                                        textAlign: TextAlign.right,
                                                                      ),
                                                                    ),
                                                                  ],

                                                                ),
                                                              );
                                                            }):SizedBox()
                                                        ,
                                                        Container(
                                                          margin: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 0.0),
                                                          child: Divider(
                                                            color: cart_viewline,
                                                          ),
                                                        ),],)
                                                  );
                                                }),
                                          ),
                                        ]),
                                      ),
                                    )),
                                Container(
                                  color: dashboard_bg,
                                  margin: EdgeInsets.fromLTRB(0.0, 8, 0.0, 0.0),
                                  alignment: Alignment.centerRight,
                                  child: Padding(
                                    padding: EdgeInsets.only(left: 15, right: 15),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        RichText(
                                            text: TextSpan(
                                              children: [
                                                TextSpan(
                                                    text: "Bill Amount : ",
                                                    style: new TextStyle(
                                                        fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                        color: text_split,
                                                        fontFamily: 'Poppins',
                                                        fontWeight: FontWeight.w500))
                                              ],
                                            )),
                                        RichText(
                                            text: TextSpan(children: [
                                              TextSpan(
                                                  text: new String.fromCharCodes(
                                                      new Runes(
                                                          '\u0024')) + __cart_items_list[pindex].subTotal,
                                                  style: new TextStyle(
                                                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                      color: text_split,
                                                      fontFamily: 'Poppins',
                                                      fontWeight: FontWeight.w500))
                                            ]))
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  color: dashboard_bg,
                                  margin: EdgeInsets.fromLTRB(5.0, 0.0, 15.0, 0.0),
                                  alignment: Alignment.topRight,
                                  child: Padding(
                                    padding: EdgeInsets.only(left: 15, bottom: 0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        RichText(
                                            text: TextSpan(
                                              children: [
                                                TextSpan(
                                                    text: "Tax Total : ",
                                                    style: new TextStyle(
                                                        fontSize: 16,
                                                        color: text_split,
                                                        fontFamily: 'Poppins',
                                                        fontWeight: FontWeight.w500))
                                              ],
                                            )),
                                        RichText(
                                            text: TextSpan(children: [
                                              TextSpan(
                                                  text: new String.fromCharCodes(
                                                      new Runes(
                                                          '\u0024')) +__cart_items_list[pindex].taxAmount,
                                                  style: new TextStyle(
                                                      fontSize: 16,
                                                      color: text_split,
                                                      fontFamily: 'Poppins',
                                                      fontWeight: FontWeight.w500))
                                            ]))
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  color: dashboard_bg,
                                  margin: EdgeInsets.fromLTRB(0.0, 0, 0.0, 0.0),
                                  alignment: Alignment.centerRight,
                                  child: Padding(
                                    padding: EdgeInsets.only(left: 15, top: 5,right: 15),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        RichText(
                                            text: TextSpan(
                                              children: [
                                                TextSpan(
                                                    text: "Discount : ",
                                                    style: new TextStyle(
                                                        fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                        color: text_split,
                                                        fontFamily: 'Poppins',
                                                        fontWeight: FontWeight.w500))
                                              ],
                                            )),
                                        RichText(
                                            text: TextSpan(children: [
                                              TextSpan(
                                                  text: new String.fromCharCodes(
                                                      new Runes(
                                                          '\u0024')) +__cart_items_list[pindex].discountAmount.toStringAsFixed(2),
                                                  style: new TextStyle(
                                                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                      color: text_split,
                                                      fontFamily: 'Poppins',
                                                      fontWeight: FontWeight.w500))
                                            ]))
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  color: dashboard_bg,
                                  //margin: EdgeInsets.fromLTRB(0.0, 8, 0.0, 0.0),
                                  alignment: Alignment.centerRight,
                                  child: Padding(
                                    padding: EdgeInsets.only(left: 15, top:5,bottom:10,right: 15),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        RichText(
                                            text: TextSpan(
                                              children: [
                                                TextSpan(
                                                    text: "Grand Total : ",
                                                    style: new TextStyle(
                                                        fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                        color: login_passcode_text,
                                                        fontFamily: 'Poppins',
                                                        fontWeight: FontWeight.w500))
                                              ],
                                            )),
                                        RichText(
                                            text: TextSpan(children: [
                                              TextSpan(
                                                  text: new String.fromCharCodes(
                                                      new Runes(
                                                          '\u0024')) + __cart_items_list[pindex].totalAmount,
                                                  style: new TextStyle(
                                                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                      color: login_passcode_text,
                                                      fontFamily: 'Poppins',
                                                      fontWeight: FontWeight.w700))
                                            ])),




                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            )); // you forgot this
                      },
                      controller: controller,
                      reverse: false,
                    ),
                    align: IndicatorAlign.bottom,
                    indicatorColor: Colors.black12,
                    indicatorSelectorColor: Colors.lightBlueAccent,
                    length: split_length,
                    indicatorSpace: 10.0,
                  ),
                )),
              ),
            ],
          ),
        ),
      ),
    );
  }
}



class CancelDialogs {
  static Future<void> showLoadingDialog(
      BuildContext context, GlobalKey key) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(
                  key: key,
                  backgroundColor: Colors.black54,
                  children: <Widget>[
                    Center(
                      child: Column(children: [
                        CircularProgressIndicator(),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Cancelling....",
                          style: TextStyle(color: Colors.lightBlueAccent),
                        )
                      ]),
                    )
                  ]));
        });
  }
}
Route _createRoute(String table_no) {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => SplitScreen(table_no),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      var begin = Offset(0.0, 1.0);
      var end = Offset.zero;
      var curve = Curves.ease;

      var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

      return SlideTransition(
        position: animation.drive(tween),
        child: child,
      );
    },
  );
}