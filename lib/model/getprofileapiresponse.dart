

import 'package:waiter/model/SendTaxRatesModel.dart';
import 'package:waiter/model/getmenuitems.dart';



class getprofileapiresponse {
  ProfileDict profileDict;
  int responseStatus;
  String result;

  getprofileapiresponse({this.profileDict, this.responseStatus, this.result});

  getprofileapiresponse.fromJson(Map<String, dynamic> json) {
    profileDict = json['profileDict'] != null
        ? new ProfileDict.fromJson(json['profileDict'])
        : null;
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.profileDict != null) {
      data['profileDict'] = this.profileDict.toJson();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class ProfileDict {
  String firstName;
  String lastName;
  String image;

  ProfileDict({this.firstName,this.lastName,this.image});

  ProfileDict.fromJson(Map<String, dynamic> json) {
    firstName = json['firstName'];
    lastName = json['lastName'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['firstName'] = this.firstName;
    data['lastName'] = this.lastName;
    data['image'] = this.image;
    return data;
  }
}