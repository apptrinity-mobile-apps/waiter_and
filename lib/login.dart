import 'dart:io';
import 'dart:ui';

import 'package:async/async.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pinput/pin_put/pin_put.dart';
import 'package:toast/toast.dart';
import 'package:waiter/apis/GetWaiterServiceAreasApi.dart';
import 'package:waiter/apis/loginapi.dart';
import 'package:waiter/coupons.dart';
import 'package:waiter/model/getwaiterservicearearesponse.dart';
import 'package:waiter/session/userRepository.dart';
import 'package:waiter/utils/DialogClass.dart';
import 'package:waiter/utils/all_constans.dart';
import 'package:waiter/utils/sizeconfig.dart';
import 'apis/GetAllDiningOptionsApi.dart';
import 'dashboard.dart';
import 'model/getalldinningoptionsresponse.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: MyHomePage(title: 'LOGIN WITH'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  final _formKey = GlobalKey<FormState>();
  final _pinPutFocusNode = FocusNode();
  final _pinPutController = TextEditingController();
  List<Color> _colors = [login_passcode_bg1, login_passcode_bg2];
  List<double> _stops = [0.0, 0.7];
  CancelableOperation cancelableOperation;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  String restaurant_id = "";
  List<DinningOptionsList> _getalldineoptions;
  List<ServiceAreasList> _getwaitersservicearea;
  bool _loading = true;
  String deviceName = "";
  int _currentIndex = 0;
  String passcode_selected = "passcode_selected.png";
  String username_selected = "username_unselected.png";
  String is_platformtype = "";

  List<String> widgetList = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '', '0', '*'];
  final BoxDecoration pinPutDecoration = BoxDecoration(
    color: const Color.fromRGBO(235, 236, 237, 1),
    borderRadius: BorderRadius.circular(0.0),
  );

  TextEditingController username_Controller = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 2);

    initPlatformState();
  }

  Future<void> initPlatformState() async {
    Map<String, dynamic> deviceData;
    try {
      if (Platform.isAndroid) {
        is_platformtype = "android";
        //print("DEVICEDATA"+deviceData.toString());
        var build = await deviceInfoPlugin.androidInfo;
        deviceName = build.device.toString();
        print("DEVICENAME" + deviceName.toString() + "--------" + is_platformtype);
      } else if (Platform.isIOS) {
        is_platformtype = "ios";
        var build = await deviceInfoPlugin.iosInfo;
        deviceName = build.name.toString();
        print("DEVICENAME" + deviceName.toString() + "-------" + is_platformtype);
      }
    } on PlatformException {
      deviceData = <String, dynamic>{'Error:': 'Failed to get platform version.'};
    }
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Scaffold(
        body: Center(
      child: Column(
        children: <Widget>[
          SizedBox(height: 100),
          Text(
            'LOGIN WITH',
            style: TextStyle(
              color: add_food_item_bg,
              fontSize: SizeConfig.blockSizeHorizontal * 6,
              fontFamily: 'Poppins',
              fontWeight: FontWeight.w600,
            ),
          ),
          SizedBox(height: 20),
          Container(
            height: 100,
            margin: EdgeInsets.fromLTRB(30.00, 10.00, 30.00, 0.00),
            child: TabBar(
              onTap: (index) {
                setState(() {
                  _currentIndex = index;
                  print(index);
                  if (index == 0) {
                    passcode_selected = 'passcode_selected.png';
                    username_selected = 'username_unselected.png';
                  } else {
                    passcode_selected = 'passcode_unselected.png';
                    username_selected = 'username_selected.png';
                  }
                });
              },
              controller: _tabController,
              indicatorWeight: 0.1,

              indicatorPadding: EdgeInsets.zero,
              //indicatorSize: TabBarIndicatorSize.label,
              indicatorColor: Colors.grey,
              indicator: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.3),
                    blurRadius: 5,
                    offset: Offset(0, 10), // changes position of shadow
                  ),
                ],
                gradient: LinearGradient(begin: Alignment.topCenter, end: Alignment.bottomCenter, colors: [login_passcode_bg1, login_passcode_bg2]),
                borderRadius: BorderRadius.horizontal(left: Radius.circular(3.00), right: Radius.circular(3.00)),
                /* color: Colors.lightBlueAccent*/
              ),
              tabs: [
                Tab(
                  child: Container(
                    height: 100,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.horizontal(left: Radius.circular(3.00), right: Radius.zero),
                    ),
                    child: Column(
                      children: <Widget>[
                        Expanded(
                            child: Image.asset(
                          'images/' + passcode_selected,
                          height: 80,
                        )),
                        Text('Passcode',
                            style: TextStyle(
                              color: _currentIndex == 0 ? Colors.white : coupontextdesc,
                              fontSize: SizeConfig.blockSizeHorizontal * 2.8,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w400,
                            ))
                      ],
                    ),
                  ),
                ),
                Tab(
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.horizontal(left: Radius.circular(3.00), right: Radius.zero),
//border: Border.all(color: Colors.grey),
                    ),
                    child: Column(
                      children: <Widget>[
                        Expanded(
                            child: Image.asset(
                          'images/'+username_selected,
                          height: 60,
                        )),
                        Text("Username/Password",
                            style: TextStyle(
                              color: _currentIndex == 1 ? Colors.white : coupontextdesc,
                              fontSize: SizeConfig.blockSizeHorizontal * 2.8,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w400,
                            )),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(child: _currentIndex == 0   ? _passcode_widget() : _username_widget())
          /* Expanded(
                    child: TabBarView(
                      physics: NeverScrollableScrollPhysics(),
                      children: [
                        _passcode_widget(),
                        _username_widget(),
                      ],
                    ),
                  ),*/
        ],
      ),
    ) //),
        //),

        // ],
        //),
        //),

        );
  }

  getGridViewSelectedItem(BuildContext context, String gridItem) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text(gridItem),
          actions: <Widget>[
            FlatButton(
              child: new Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _passcode_widget() {
    return SingleChildScrollView(child:Container(
        margin: EdgeInsets.fromLTRB(10.00, 30.00, 10.00, 0.00),
        child: Column(children: [
          SizedBox(height: 20),
          Container(
            child: Text(
              'Passcode',
              style: TextStyle(color: login_passcode_text, fontSize: SizeConfig.blockSizeHorizontal * 3.5,
                fontFamily: 'Poppins',
                fontWeight: FontWeight.w400,),
            ),
          ),
          SizedBox(height: 10),
          GestureDetector(
            onLongPress: () {
              print(_formKey.currentState.validate());
            },
            child: PinPut(
              withCursor: false,
              fieldsCount: 6,
              fieldsAlignment: MainAxisAlignment.center,
              textStyle: const TextStyle(
                fontSize: 25.0,
                color: login_passcode_star,
              ),
              //  eachFieldMargin: EdgeInsets.all(0),
              eachFieldWidth: 50.0,
              eachFieldHeight: 50.0,
              //  onSubmit: (String pin) => _showSnackBar(pin),
              focusNode: _pinPutFocusNode,
              controller: _pinPutController,
              submittedFieldDecoration: pinPutDecoration.copyWith(
                color: Colors.white,
                border: Border.all(
                  width: 1,
                  color: const Color.fromRGBO(160, 215, 220, 1),
                ),
              ),
              selectedFieldDecoration: pinPutDecoration.copyWith(
                color: Colors.white,
                border: Border.all(
                  width: 0,
                  color: login_passcode_box,
                ),
              ),
              followingFieldDecoration: pinPutDecoration.copyWith(
                color: Colors.white,
                border: Border.all(
                  width: 0,
                  color: login_passcode_box,
                ),
              ),
              pinAnimationType: PinAnimationType.fade,
              obscureText: "*",
              onChanged: (value) {
                if (value.length == 6) {
                  UserRepository().getGenerateOtpDetails().then((otpdetails) {
                    setState(() {
                      restaurant_id = otpdetails[0];
                      print("SESSIONRESTAURANTIDPASSCODE" + restaurant_id);
                      LoginRepository()
                          .loginwithpasscode(restaurant_id, value, deviceName, is_platformtype,
                              "f-euc5J9SVCZpjeQpWyFLG:APA91bH9k1KNnH8Ht4Kw5lGyrxT2mpQWwDvEjxJiwt_80Lq_KquqCLXfOcXTT-MwXmkyQ6APaKBBZ2oajDQfnyb23SdEOc2vIKUQDzZV5RBu9gUjR_OziswTH5hCeQBNuUV4aqH6H-ZU")
                          .then((result) {
                        //print(result);
                        if (result.responseStatus == 0) {
                          Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
                          Toast.show("Invalid Credentials", context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                        } else {
                          print("LOGINRESPONSEPASSCODE " + result.userDetails.id + "--" + result.userDetails.firstName + "--" + result.userDetails.lastName);

                          setState(() {
                            UserRepository.save_userid(result.userDetails.id, result.userDetails.firstName, result.userDetails.lastName, "true");

                            print("DINNINGREQUESTDATA" + result.userDetails.id + "-----------" + restaurant_id);
                            GetAllDinningOptionsApiRepository().getAllDinningOptions(result.userDetails.id, restaurant_id).then((getalldineoptionlist) {
                              setState(() {
                                _getalldineoptions = getalldineoptionlist;
                                setState(() {
                                  _loading = false;
                                  print("GETALLDINEOPTIONSLIST" + _getalldineoptions.length.toString());
                                  UserRepository.save_dineinoption_id(_getalldineoptions[0].id);
                                });
                              });
                            });

                            GetWaiterServiceAreasApiRepository().getWaiterServiceArea(restaurant_id).then((getwaiterservicearea) {
                              setState(() {
                                _getwaitersservicearea = getwaiterservicearea;
                                setState(() {
                                  _loading = false;
                                  print("GETWAITERSERVICEAREALIST" + _getwaitersservicearea.length.toString());
                                  UserRepository.save_servicearea_id(_getwaitersservicearea[0].id, _getwaitersservicearea[0].revenueCenter.toString());
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => HomePage()),
                                  );
                                });
                              });
                            });
                          });
                        }
                      });
                    });
                  });
                }
              },
            ),
          )
        ])));
  }

  Widget _username_widget() {
    return SingleChildScrollView(
        child: Container(
            margin: EdgeInsets.fromLTRB(10.00, 30.00, 10.00, 0.00),
            child: Column(children: [
              Container(
                child: new Form(
                  key: _formKey, //padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
                  child: new Column(
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                height: 5.0,
                              ),
                              Container(
                                  margin: EdgeInsets.only(bottom: 8),
                                  child: Text(
                                    'User Name',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(color: text_hint_color, fontSize: SizeConfig.blockSizeHorizontal * 2.8,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w500,),
                                  )),
                          TextFormField(
                                validator: (val) {
                                  if (val.isEmpty) return 'Enter Username';
                                  return null;
                                },
                                controller: username_Controller,
                                obscureText: false,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(0.0),
                                      borderSide: BorderSide(
                                        color: login_passcode_bg2,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(0.0),
                                      borderSide: BorderSide(
                                        color: login_form_hint,
                                        width: 1.0,
                                      ),
                                    ),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: "Enter User Name",
                                    hintStyle: TextStyle(color: login_form_hint, fontSize: SizeConfig.blockSizeHorizontal *2.8,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w400,),
                                    contentPadding: EdgeInsets.only(
                                      bottom: 30 / 2,
                                      left: 50 / 2, // HERE THE IMPORTANT PART
                                      // HERE THE IMPORTANT PART
                                    ),
                                    border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0))),


                              ),
                              Container(
                                  margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
                                  child: Text(
                                    'Password',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(color: text_hint_color, fontSize: SizeConfig.blockSizeHorizontal * 2.8,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w500,),
                                  )),
                              SizedBox(
                                height: 5.0,
                              ),
                              TextFormField(
                                validator: (val) {
                                  if (val.isEmpty) return 'Enter Password';
                                  return null;
                                },
                                controller: passwordController,
                                obscureText: true,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(0.0),
                                      borderSide: BorderSide(
                                        color: login_passcode_bg2,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(0.0),
                                      borderSide: BorderSide(
                                        color: login_form_hint,
                                        width: 1.0,
                                      ),
                                    ),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: "Enter Password",
                                    hintStyle: TextStyle(color: login_form_hint, fontSize: SizeConfig.blockSizeHorizontal *2.8,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w400,),
                                    contentPadding: EdgeInsets.only(
                                      bottom: 30 / 2,
                                      left: 50 / 2, // HERE THE IMPORTANT PART
                                      // HERE THE IMPORTANT PART
                                    ),
                                    border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0))),
                              ),
                            ],
                          )),
                      Container(
                          margin: EdgeInsets.fromLTRB(25, 25, 25, 0),
                          height: 50,
                          width: double.infinity,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(color: login_passcode_bg1, borderRadius: BorderRadius.circular(0)),
                          child: InkWell(
                              child: FlatButton(
                                  minWidth: double.infinity,
                                  height: double.infinity,
                                  child: Text("SUBMIT", style: TextStyle(fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w800, color: Colors.white)),
                                  onPressed: () {
                                    if (_formKey.currentState.validate()) {
                                      _formKey.currentState.save();
                                     // SignDialogs.showLoadingDialog(context, "Signing in", _keyLoader);
                                      cancelableOperation?.cancel();
                                      CancelableOperation.fromFuture(Future.delayed(Duration(seconds: 1), () {
                                        UserRepository().getGenerateOtpDetails().then((otpdetails) {
                                          setState(() {
                                            restaurant_id = otpdetails[0];
                                            print("SESSIONRESTAURANTID" + restaurant_id);
                                            LoginRepository()
                                                .checklogin(restaurant_id, username_Controller.text, passwordController.text, deviceName, is_platformtype,
                                                    "f-euc5J9SVCZpjeQpWyFLG:APA91bH9k1KNnH8Ht4Kw5lGyrxT2mpQWwDvEjxJiwt_80Lq_KquqCLXfOcXTT-MwXmkyQ6APaKBBZ2oajDQfnyb23SdEOc2vIKUQDzZV5RBu9gUjR_OziswTH5hCeQBNuUV4aqH6H-ZU")
                                                .then((value) {
                                              print(value);
                                              if (value.responseStatus == 0) {
                                                Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
                                                Toast.show("Invalid Credentials", context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                                              } else {
                                                print("LOGINRESPONSE " + "--" + value.userDetails.id + "--" + value.userDetails.firstName + "--" + value.userDetails.lastName);

                                                setState(() {
                                                  UserRepository.save_userid(value.userDetails.id, value.userDetails.firstName, value.userDetails.lastName, "true");
                                                  print("DINNINGREQUESTDATA" + value.userDetails.id + "-----------" + restaurant_id);
                                                  GetAllDinningOptionsApiRepository().getAllDinningOptions(value.userDetails.id, restaurant_id).then((getalldineoptionlist) {
                                                    setState(() {
                                                      _getalldineoptions = getalldineoptionlist;
                                                      setState(() {
                                                        _loading = false;
                                                        print("GETALLDINEOPTIONSLIST" + _getalldineoptions.length.toString());
                                                        UserRepository.save_dineinoption_id(_getalldineoptions[0].id);
                                                      });
                                                    });
                                                  });

                                                  GetWaiterServiceAreasApiRepository().getWaiterServiceArea(restaurant_id).then((getwaiterservicearea) {
                                                    setState(() {
                                                      _getwaitersservicearea = getwaiterservicearea;
                                                      setState(() {
                                                        _loading = false;
                                                        print("GETWAITERSERVICEAREALIST" + _getwaitersservicearea.length.toString());
                                                        UserRepository.save_servicearea_id(_getwaitersservicearea[0].id, _getwaitersservicearea[0].revenueCenter.toString());
                                                        Navigator.push(
                                                          context,
                                                          MaterialPageRoute(builder: (context) => HomePage()),
                                                        );
                                                      });
                                                    });
                                                  });
                                                });
                                              }
                                            });
                                          });
                                        });
                                      }));
                                    }
                                  })))
                    ],
                  ),
                ),
              ),
            ])));
  }

  void _showToast(BuildContext context) {
    final scaffold = Scaffold.of(context);
    scaffold.showSnackBar(
      SnackBar(
        content: const Text('Added to favorite'),
        action: SnackBarAction(label: 'UNDO', onPressed: scaffold.hideCurrentSnackBar),
      ),
    );
  }
}
