import 'dart:convert';
import 'dart:math';

import 'package:async/async.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:requests/requests.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:waiter/apis/CartsRepository.dart';
import 'package:waiter/session/userRepository.dart';
import 'package:waiter/utils/Globals.dart';
import 'package:waiter/utils/all_constans.dart';
import 'package:waiter/model/get_items_by_menugroupid.dart' as slist;

import 'addcomboitems_screen.dart';
import 'appbar_back_arrow.dart';
import 'model/SendTaxRatesModel.dart';
import 'model/addbogoitemstatus.dart';
import 'model/cartmodelitemsapi.dart';
import 'model/get_all_bogo_model.dart';
import 'model/get_items_by_menugroupid.dart';
import 'model/getmenuitemdata.dart';
import 'model/getmenuitems.dart';

class GetAllBogo {
  Future<List<DiscountsBogo>> getbogo(
    String user_id,
    String res_id,
  ) async {
    var body = json.encode({'userId': user_id, 'restaurantId': res_id});
    print(body);
    dynamic response = await Requests.post(base_url + "get_all_bogo_disounts", body: body, headers: {'Content-type': 'application/json'});
    print("resultcomboitem---" + response);
    final res = json.decode(response);
    //print("result---"+res);
    if (res['responseStatus'] == 1) {
      Iterable list = res['discounts'];
      return list.map((model) => DiscountsBogo.fromJson(model)).toList();
    } else {
      return [];
    }
  }
}

class GetItemsbyMenuGroupID {
  Future<List<MenuItems>> getitems(
    String menu_group_id,
    String res_id,
  ) async {
    var body = json.encode({'menuGroupId': menu_group_id, 'restaurantId': res_id});
    print(body);
    dynamic response = await Requests.post(base_url + "get_menu_items_on_menu_group", body: body, headers: {'Content-type': 'application/json'});
    print("resultget_menu_items_on_menu_group---" + response);
    final res = json.decode(response);
    //print("result---"+res);
    if (res['responseStatus'] == 1) {
      Iterable list = res['menuItems'];
      return list.map((model) => MenuItems.fromJson(model)).toList();
    } else {
      return [];
    }
  }
}

class AddBogoItem extends StatefulWidget {
  final String menu_name;
  final String menu_id;
  final String menu_groupid;
  final String table_number;
  final String no_of_guests;
  final bool header_value;

  const AddBogoItem(this.menu_name, this.menu_id, this.menu_groupid, this.table_number, this.no_of_guests, this.header_value, {Key key})
      : super(key: key);

  @override
  _AddBogoItemState createState() => _AddBogoItemState();
}

class _AddBogoItemState extends State<AddBogoItem> {
  List<DiscountsBogo> get_all_bogo;
  List<MenuCartItemapi> __cart_items_list = new List();
  List<String> selectedCheckbox = [];
  bool _loading = true;
  String item_id = "";
  String item_menugroup_id = "";
  String item_menu_id = "";
  String item_name = "";
  String table_number_session = "";
  String no_of_guests_session = "";

  String userid = "";
  String res_id = "";
  bool checkvalue = false;

  int cart_count = 0;
  List<String> bogoid_type;
  List<addbogoitemstatus> bogoid_with_types = new List();
  CancelableOperation cancelableOperation;
  String returnVal = "";
  String returngetVal = "";

  @override
  void initState() {
    super.initState();
    // print(widget.main_order_id.toString());
    UserRepository().saveselectedbuyitems([]);
    UserRepository().getuserdetails().then((userdetails) {
      setState(() {
        userid = userdetails[0];
        UserRepository().getGenerateOtpDetails().then((restaurantdetails) {
          setState(() {
            res_id = restaurantdetails[0];
            print(userid+" restaurant_id " + res_id);
            print("FOODITEMS " + widget.menu_id + "--" + widget.menu_groupid + "--" + res_id);
            GetAllBogo().getbogo(userid, res_id).then((result_allcomboitems) {
              setState(() {
                //isLoading = true;
                get_all_bogo = result_allcomboitems;
                print(get_all_bogo.length);
                for (int g = 0; g < get_all_bogo.length; g++) {
                  print(get_all_bogo[g].buyItems);
                  for (int h = 0; h < get_all_bogo[g].buyItems.length; h++) {
                    print(get_all_bogo[g].buyItems[h].bitems);
                    bogoid_type = new List();
                    if (get_all_bogo[g].buyItems[h].bitems.length > 0) {
                      for (int i = 0; i < get_all_bogo[g].buyItems[h].bitems.length; i++) {
                        print(get_all_bogo[g].buyItems[h].bitems[i].type);
                        bogoid_type.add("0");
                      }
                      bogoid_with_types.add(addbogoitemstatus(get_all_bogo[g].buyItems[h].bitems[0].id, bogoid_type));
                    }
                  }
                }
                UserRepository().savebogo_with_types(bogoid_with_types);
                Future.delayed(Duration(seconds: 2), () async {
                  setState(() {
                    CartsRepository().getcartslisting().then((cartList) {
                      setState(() {
                        __cart_items_list = cartList;
                        cart_count = __cart_items_list.length;

                        UserRepository().getGenerateTablenumbernGuests().then((tableservicedetails) {
                          setState(() {
                            table_number_session = tableservicedetails[0];
                            no_of_guests_session = tableservicedetails[1];
                            print("TABLESERVICEDETAILS" + table_number_session + "---------" + no_of_guests_session);
                          });
                        });

                        //_loading = false;
                      });
                    });
                  });
                });
                _loading = false;
              });
            });
          });
        });

        //userid = "60893ea9180d0231ab2bbbbf";
        //res_id = "6083c539b054fccb93b9cd82";

      });
    });
  }

  @override
  Widget build(BuildContext context) {
    var _crossAxisSpacing = 8;
    var _screenWidth = MediaQuery.of(context).size.width;
    var _crossAxisCount = 2;
    var _width = (_screenWidth - ((_crossAxisCount - 1) * _crossAxisSpacing)) / _crossAxisCount;
    var cellHeight = 60;
    var _aspectRatio = _width / cellHeight;
    return Scaffold(
        appBar: YourAppbarBackArrow(
          count: cart_count,
          title_text: widget.menu_name,
        ),
        body: _loading
            ? Center(
                child: SpinKitFadingCircle(color: Colors.lightBlueAccent),
              )
            : Container(
                color: Colors.white,
                child: Column(
                  children: [
                    Expanded(
                      child: Container(
                          margin: EdgeInsets.only(left: 10, right: 10),
                          padding: EdgeInsets.all(15),
                          //height: MediaQuery.of(context).size.height * 0.2,
                          color: dashboard_bg,
                          child: ListView.separated(
                              separatorBuilder: (context, index) => Divider(
                                    color: dashboard_bg,
                                    height: 10,
                                  ),
                              scrollDirection: Axis.vertical,
                              itemCount: get_all_bogo.length,
                              shrinkWrap: true,
                              itemBuilder: (context, index) {
                                return Card(
                                    elevation: 3,
                                    child: Container(
                                        margin: EdgeInsets.all(0),
                                        width: MediaQuery.of(context).size.width * 0.3,
                                        child: InkWell(
                                          onTap: () {},
                                          child: Container(
                                            color: Colors.white,
                                            //padding: EdgeInsets.all(15),
                                            //height: 250,
                                            child: get_all_bogo[index].buyItems.length > 0
                                                ? Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                                    Container(
                                                        padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                                                        margin: EdgeInsets.only(right: 10),
                                                        color: dashboard_quick_order,
                                                        width: double.infinity,
                                                        height: 35,
                                                        alignment: Alignment.centerLeft,
                                                        child: Text(
                                                          "BUY ITEMS",
                                                          style: TextStyle(
                                                              color: Colors.lightBlueAccent,
                                                              fontSize: 14,
                                                              fontFamily: 'Poppins',
                                                              fontWeight: FontWeight.w500),
                                                          textAlign: TextAlign.start,
                                                        )),
                                                    ListView.builder(
                                                      shrinkWrap: true,
                                                      itemCount: get_all_bogo[index].buyItems.length,
                                                      itemBuilder: (context, b) {
                                                        //final item = searchList[index];
                                                        return get_all_bogo[index].buyItems[b].bitems.length > 0
                                                            ? Container(
                                                                color: dashboard_bg,
                                                                //padding: EdgeInsets.only(left: 2.0, right: 2.0, top: 0, bottom: 0),
                                                                margin: EdgeInsets.all(5),
                                                                child: GridView.builder(
                                                                  shrinkWrap: true,
                                                                  itemCount: get_all_bogo[index].buyItems[b].bitems.length,
                                                                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                                                      crossAxisCount: _crossAxisCount, childAspectRatio: _aspectRatio),
                                                                  itemBuilder: (context, c) {
                                                                    //final item = searchList[index];
                                                                    return Container(
                                                                        color: dashboard_bg,
                                                                        padding: EdgeInsets.only(left: 2.0, right: 2.0, top: 0, bottom: 0),
                                                                        margin: EdgeInsets.only(left: 5, right: 5),
                                                                        child: ListTile(
                                                                          title: Text(get_all_bogo[index].buyItems[b].bitems[c].name,
                                                                              style: TextStyle(
                                                                                  fontSize: 14,
                                                                                  fontFamily: 'Poppins',
                                                                                  fontWeight: FontWeight.w600,
                                                                                  color: add_food_item_bg)),
                                                                          onTap: () {
                                                                            setState(() {
                                                                              gewinner(index, b, c);
                                                                            });
                                                                          },
                                                                        ));
                                                                  },
                                                                ),
                                                              )
                                                            : SizedBox();
                                                      },
                                                    ),
                                                    Container(
                                                        padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                                                        margin: EdgeInsets.only(right: 10),
                                                        color: dashboard_quick_order,
                                                        width: double.infinity,
                                                        height: 35,
                                                        alignment: Alignment.centerLeft,
                                                        child: Text(
                                                          "GET ITEMS",
                                                          style: TextStyle(
                                                              color: kToolbarTitleColor,
                                                              fontSize: 14,
                                                              fontFamily: 'Poppins',
                                                              fontWeight: FontWeight.w500),
                                                          textAlign: TextAlign.start,
                                                        )),
                                                    Container(
                                                        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                                                        child: Text("Get one of the following items for 60% off",
                                                            maxLines: 1,
                                                            overflow: TextOverflow.ellipsis,
                                                            style: TextStyle(
                                                                fontSize: 12,
                                                                fontFamily: 'Poppins',
                                                                fontWeight: FontWeight.w400,
                                                                color: add_food_item_bg))),
                                                    ListView.builder(
                                                      shrinkWrap: true,
                                                      itemCount: get_all_bogo[index].getItems.length,
                                                      itemBuilder: (context, b) {
                                                        //final item = searchList[index];
                                                        return get_all_bogo[index].getItems[b].gitems.length > 0
                                                            ? Container(
                                                                color: dashboard_bg,
                                                                //padding: EdgeInsets.only(left: 2.0, right: 2.0, top: 0, bottom: 0),
                                                                margin: EdgeInsets.all(5),
                                                                child: ListView.builder(
                                                                  shrinkWrap: true,
                                                                  itemCount: get_all_bogo[index].getItems[b].gitems.length,
                                                                  itemBuilder: (context, c) {
                                                                    //final item = searchList[index];
                                                                    return Container(
                                                                        color: dashboard_bg,
                                                                        padding: EdgeInsets.only(left: 2.0, right: 2.0, top: 0, bottom: 0),
                                                                        margin: EdgeInsets.only(left: 5, right: 5),
                                                                        child: ListTile(
                                                                          title: Text(get_all_bogo[index].getItems[b].gitems[c].name,
                                                                              style: TextStyle(
                                                                                  fontSize: 14,
                                                                                  fontFamily: 'Poppins',
                                                                                  fontWeight: FontWeight.w600,
                                                                                  color: add_food_item_bg)),
                                                                          onTap: () {
                                                                            setState(() {
                                                                              getitemswinner(index, b, c);
                                                                            });
                                                                          },
                                                                        ));
                                                                  },
                                                                ),
                                                              )
                                                            : SizedBox();
                                                      },
                                                    ),
                                                    /*Padding(
                                                  padding: EdgeInsets.only(
                                                    left: 0,
                                                    right: 0,
                                                  ),
                                                  child: InkWell(
                                                    child: FlatButton(
                                                        minWidth: 30,
                                                        height: 30,
                                                        color: Colors.lightBlueAccent,
                                                        child: Text("Add",
                                                            style: TextStyle(
                                                                fontSize: 14,
                                                                fontFamily: 'Poppins',
                                                                fontWeight: FontWeight.w600,
                                                                color: add_food_item_bg)),
                                                        onPressed: () {}),
                                                  ))*/
                                                  ])
                                                : SizedBox(),
                                          ),
                                        )));
                              })),
                    ),
                  ],
                )));
  }

  Future gewinner(int index, int bpos, int cpos) async {
    returnVal = await showDialog(
        context: context,
        builder: (context) {
          return _MyDialog(bogoid: get_all_bogo[index].id, all_buyitems_list: get_all_bogo[index].buyItems[bpos].bitems);
        });
    print("RETURNVALUEINSIDE" + returnVal);

    if (returnVal == "Success") {
      Future.delayed(Duration(seconds: 2), () async {
        setState(() {
          //print(getBogobuyitemadded.length);
        });
      });
    }
  }

  Future getitemswinner(int index, int bpos, int cpos) async {
    returngetVal = await showDialog(
        context: context,
        builder: (context) {
          return _GetItemsDialog(
              bogoid: get_all_bogo[index].id,
              all_getitems_list: get_all_bogo[index].getItems[bpos].gitems,
              getDiscountValue: get_all_bogo[index].getDiscountValue,
              discountValueType: get_all_bogo[index].discountValueType);
        });
    print("RETURNVALUEINSIDE" + returngetVal);

    if (returngetVal == "Success") {
      Future.delayed(Duration(seconds: 2), () async {
        setState(() {
          Future.delayed(Duration(seconds: 2), () async {
            setState(() {
              CartsRepository().getcartslisting().then((cartList) {
                setState(() {
                  __cart_items_list = cartList;
                  cart_count = __cart_items_list.length;
                  print(cart_count);
                  final mapped = __cart_items_list.fold<Map<String, Map<String, dynamic>>>({}, (p, v) {
                    final name = v.itemId;
                    if (p.containsKey(name)) {
                      p[name]["NumberOfItems"] += int.parse(v.quantity);
                    } else {
                      p[name] = {"NumberOfItems": int.parse(v.quantity)};
                    }
                    return p;
                  });
                  print(mapped.length);
                  print(mapped);
                  //_loading = false;
                });
              });
            });
          });
        });
      });
    }
  }
}

/////BUY ITEMS/////
class _MyDialog extends StatefulWidget {
  _MyDialog({
    this.bogoid,
    this.all_buyitems_list,
  });

  final List<Bitems> all_buyitems_list;
  final String bogoid;

  @override
  _MyDialogState createState() => _MyDialogState();
}

class _MyDialogState extends State<_MyDialog> {
  List<MenuItems> buyitems_arraylist = new List();
  List<List<MenuItems>> items_arraymenulist = new List();
  List<List<int>> totalsizelist_selectpos = new List();
  List<int> sizelist_selectpos;
  List<addbogoitemstatus> get_savedbogoselected = new List();
  String default_sizeitem = "";
  int item_index_pos = 0;
  int selected_pos = 0;
  bool _loading = true;
  List<MenuCartItemapi> addselecteditemwithsize = new List();
  var taxType = "";
  var taxTypeId = 0;
  var tax = 0.0;
  String roundingOptions = "";
  int roundingOptionId = 0;
  List<SendTaxRatesModel> taxes_arraylist = new List();
  List<TaxTable> tax_table_arraylist = new List();
  String res_id;

  @override
  void initState() {
    super.initState();
    UserRepository().getGenerateOtpDetails().then((restaurantdetails) {
      setState(() {
        res_id = restaurantdetails[0];
    print(widget.all_buyitems_list[0].id);
    for (int g = 0; g < widget.all_buyitems_list.length; g++) {
      //print(widget.all_comboitems_list[g].comboId);

      if (widget.all_buyitems_list[g].type == "Menu Group")
      {
        //print(widget.all_comboitems_list[g].type);

        GetItemsbyMenuGroupID().getitems(widget.all_buyitems_list[g].id, res_id).then((result_allmenuitems) {
          setState(() {
            //print(result_allmenuitems);
            buyitems_arraylist = result_allmenuitems;
            items_arraymenulist.add(buyitems_arraylist);
            sizelist_selectpos = new List();
            for (int h = 0; h < buyitems_arraylist.length; h++) {
              if (h == 0) {
                String item_size = "";
                double total_price;
                if (buyitems_arraylist[h].sizeList.length > 0) {
                  item_size = buyitems_arraylist[h].sizeList[0].sizeName;
                  if (buyitems_arraylist[h].basePrice == "0.00" || buyitems_arraylist[h].basePrice == null) {
                    total_price = buyitems_arraylist[h].sizeList[0].price;
                  } else {
                    total_price = double.parse(buyitems_arraylist[h].basePrice);
                  }
                } else {
                  item_size = "";
                  total_price = double.parse(buyitems_arraylist[h].basePrice);
                }

                print("TOTALPRICE-----" + total_price.toString());

                //TAX CALCULATION
                print("TAXINCLUDEOPTION" + buyitems_arraylist[h].taxIncludeOption.toString());
                if (buyitems_arraylist[h].taxIncludeOption == true) {
                  if (buyitems_arraylist[h].taxesList.length > 0) {
                    /* print("TAXRATELENGTH" +
                        buyitems_arraylist[h].taxesList.length
                            .toString());*/
                    for (int g = 0; g < buyitems_arraylist[h].taxesList.length; g++) {
                      taxType = buyitems_arraylist[h].taxesList[g].taxType.toString();
                      var taxrate = buyitems_arraylist[h].taxesList[g].taxRate.toString();
                      if (taxType == "0") {
                        taxType = "disable";
                        taxTypeId = 0;
                        tax = 0.00;
                        taxType = taxTypeId.toString();
                      } else if (taxType == "1") {
                        var taxRate = total_price * (buyitems_arraylist[h].taxesList[g].taxRate.toDouble() / 100);

                        if (buyitems_arraylist[h].taxesList[g].roundingOptions == 1) {
                          taxRate = halfEven(taxRate, 2);
                        } else if (buyitems_arraylist[h].taxesList[g].roundingOptions == 2) {
                          taxRate = halfUp(taxRate, 2);
                        } else if (buyitems_arraylist[h].taxesList[g].roundingOptions == 3) {
                          taxRate = alwaysDown(taxRate, 2);
                        } else if (buyitems_arraylist[h].taxesList[g].roundingOptions == 4) {
                          taxRate = getNumber(alwaysUp(taxRate), precision: 2);
                        }

                        taxType = "percent";
                        taxTypeId = 1;
                        taxType = taxTypeId.toString();
                        tax = taxRate;
                      } else if (taxType == "2") {
                        taxType = "fixed";
                        taxTypeId = 2;
                        tax = total_price * buyitems_arraylist[h].taxesList[g].taxRate.toDouble();
                        taxType = taxTypeId.toString();
                      } else if (taxType == "3") {
                        taxType = "taxTable";
                        taxTypeId = 3;
                        if (buyitems_arraylist[h].taxesList[g].taxTable.length > 0) {
                          for (int t = 0; t < buyitems_arraylist[h].taxesList[g].taxTable.length; t++) {
                            if (total_price >= double.parse(buyitems_arraylist[h].taxesList[g].taxTable[t].from) &&
                                total_price <= double.parse(buyitems_arraylist[h].taxesList[g].taxTable[t].to)) {
                              tax += double.parse(buyitems_arraylist[h].taxesList[g].taxTable[t].taxApplied);
                            }
                          }
                        } else {
                          tax = 0.00;
                        }
                        taxType = taxTypeId.toString();
                      }

                      var sendTaxRates = SendTaxRatesModel(
                          buyitems_arraylist[h].taxesList[g].enableTakeOutRate,
                          buyitems_arraylist[h].taxesList[g].importId,
                          buyitems_arraylist[h].taxesList[g].orderValue,
                          buyitems_arraylist[h].taxesList[g].roundingOptions,
                          buyitems_arraylist[h].taxesList[g].status,
                          buyitems_arraylist[h].taxesList[g].taxName,
                          buyitems_arraylist[h].taxesList[g].taxRate,
                          int.parse(taxType),
                          tax_table_arraylist,
                          buyitems_arraylist[h].taxesList[g].taxid,
                          buyitems_arraylist[h].taxesList[g].uniqueNumber,
                          tax);

                      taxes_arraylist.add(sendTaxRates);
                    }
                  } else {
                    taxes_arraylist = new List();
                  }
                } else {
                  taxes_arraylist = new List();
                }
                addselecteditemwithsize.add(MenuCartItemapi(
                    buyitems_arraylist[h].itemId,
                    buyitems_arraylist[h].menuId,
                    buyitems_arraylist[h].menuGroupId,
                    buyitems_arraylist[h].itemName + " " + item_size,
                    total_price.toString(),
                    buyitems_arraylist[h].quantity.toString(),
                    total_price.toString(),
                    "",
                    [],
                    [],
                    buyitems_arraylist[h].itemType,
                    buyitems_arraylist[h].discountId,
                    buyitems_arraylist[h].discountName,
                    "",
                    buyitems_arraylist[h].discountType,
                    buyitems_arraylist[h].discountValue,
                    double.parse(buyitems_arraylist[h].discountAmount.toString()),
                    "",
                    "",
                    false,
                    widget.bogoid,
                    "",
                    true,
                    false,
                    false,
                    taxes_arraylist));
              }

              if (buyitems_arraylist[h].sizeList.length > 0) {
                for (int i = 0; i < buyitems_arraylist[h].sizeList.length; i++) {
                  if (i == 0) {
                    sizelist_selectpos.add(0);
                  }
                }
              } else {
                //print(h.toString() + "else");
                sizelist_selectpos.add(-1);
              }
            }
            //print("abcddd---"+sizelist_selectpos.length.toString());
            totalsizelist_selectpos.add(sizelist_selectpos);

            _loading = false;
          });
        });
      }
      else {
        //print(widget.all_comboitems_list[g].type + "--" + widget.all_comboitems_list[g].id);
        GetItemsbyMenuData().getitems(widget.all_buyitems_list[g].id, res_id).then((resultitems) {
          setState(() {
            MenuItemData result_allmenuitems = resultitems.menuItemData;
            //print("result----" + result_allmenuitems.toString());
            //print("Menu Itesm size" + result_allmenuitems.sizeList.length.toString());
            List<slist.SizeList> test = new List();
            for (int s = 0; s < result_allmenuitems.sizeList.length; s++) {
              test.add(slist.SizeList(result_allmenuitems.sizeList[s].price, result_allmenuitems.sizeList[s].sizeName));
            }

            buyitems_arraylist.add(MenuItems(
              result_allmenuitems.basePrice,
              result_allmenuitems.diningOptionTaxException,
              result_allmenuitems.diningTaxOption,
              result_allmenuitems.discountAmount,
              result_allmenuitems.discountId,
              result_allmenuitems.discountName,
              result_allmenuitems.discountType,
              result_allmenuitems.discountValue,
              result_allmenuitems.isBogo,
              result_allmenuitems.isCombo,
              result_allmenuitems.itemId,
              result_allmenuitems.itemName,
              result_allmenuitems.itemType,
              result_allmenuitems.menuGroupId,
              result_allmenuitems.menuId,
              result_allmenuitems.pricingStrategy,
              result_allmenuitems.quantity,
              test,
              result_allmenuitems.taxIncludeOption,
              result_allmenuitems.type,
            ));
            items_arraymenulist.add(buyitems_arraylist);
            sizelist_selectpos = new List();
            for (int h = 0; h < buyitems_arraylist.length; h++) {
              if (h == 0) {
                String item_size = "";
                double total_price;

                if (buyitems_arraylist[h].sizeList.length > 0) {
                  item_size = buyitems_arraylist[h].sizeList[0].sizeName;
                  if (buyitems_arraylist[h].basePrice == "0.00" || buyitems_arraylist[h].basePrice == null) {
                    total_price = buyitems_arraylist[h].sizeList[0].price;
                  } else {
                    total_price = double.parse(buyitems_arraylist[h].basePrice);
                  }
                } else {
                  item_size = "";
                  total_price = double.parse(buyitems_arraylist[h].basePrice);
                }

                if (buyitems_arraylist[h].taxIncludeOption == true) {
                  if (buyitems_arraylist[h].taxesList.length > 0) {
                    /* print("TAXRATELENGTH" +
                        buyitems_arraylist[h].taxesList.length
                            .toString());*/
                    for (int g = 0; g < buyitems_arraylist[h].taxesList.length; g++) {
                      taxType = buyitems_arraylist[h].taxesList[g].taxType.toString();
                      var taxrate = buyitems_arraylist[h].taxesList[g].taxRate.toString();
                      if (taxType == "0") {
                        taxType = "disable";
                        taxTypeId = 0;
                        tax = 0.00;
                        taxType = taxTypeId.toString();
                      } else if (taxType == "1") {
                        var taxRate = total_price * (buyitems_arraylist[h].taxesList[g].taxRate / 100);

                        if (buyitems_arraylist[h].taxesList[g].roundingOptions == 1) {
                          taxRate = halfEven(taxRate, 2);
                        } else if (buyitems_arraylist[h].taxesList[g].roundingOptions == 2) {
                          taxRate = halfUp(taxRate, 2);
                        } else if (buyitems_arraylist[h].taxesList[g].roundingOptions == 3) {
                          taxRate = alwaysDown(taxRate, 2);
                        } else if (buyitems_arraylist[h].taxesList[g].roundingOptions == 4) {
                          taxRate = getNumber(alwaysUp(taxRate), precision: 2);
                        }

                        taxType = "percent";
                        taxTypeId = 1;
                        taxType = taxTypeId.toString();
                        tax = taxRate;
                        /* print("TAXTYPE" +
                            taxType +
                            "=========" +
                            tax.toStringAsFixed(2) +
                            "========" +
                            buyitems_arraylist[h].taxesList[g]
                                .taxid);*/
                      } else if (taxType == "2") {
                        taxType = "fixed";
                        taxTypeId = 2;
                        tax = total_price * buyitems_arraylist[h].taxesList[g].taxRate.toDouble();
                        taxType = taxTypeId.toString();
                      } else if (taxType == "3") {
                        taxType = "taxTable";
                        taxTypeId = 3;
                        if (buyitems_arraylist[h].taxesList[g].taxTable.length > 0) {
                          for (int t = 0; t < buyitems_arraylist[h].taxesList[g].taxTable.length; t++) {
                            if (total_price >= double.parse(buyitems_arraylist[h].taxesList[g].taxTable[t].from) &&
                                total_price <= double.parse(buyitems_arraylist[h].taxesList[g].taxTable[t].to)) {
                              tax += double.parse(buyitems_arraylist[h].taxesList[g].taxTable[t].taxApplied);
                            }
                          }
                        } else {
                          tax = 0.00;
                        }
                        taxType = taxTypeId.toString();
                      }

                      var sendTaxRates = SendTaxRatesModel(
                          buyitems_arraylist[h].taxesList[g].enableTakeOutRate,
                          buyitems_arraylist[h].taxesList[g].importId,
                          buyitems_arraylist[h].taxesList[g].orderValue,
                          buyitems_arraylist[h].taxesList[g].roundingOptions,
                          buyitems_arraylist[h].taxesList[g].status,
                          buyitems_arraylist[h].taxesList[g].taxName,
                          buyitems_arraylist[h].taxesList[g].taxRate,
                          int.parse(taxType),
                          tax_table_arraylist,
                          buyitems_arraylist[h].taxesList[g].taxid,
                          buyitems_arraylist[h].taxesList[g].uniqueNumber,
                          tax);

                      taxes_arraylist.add(sendTaxRates);
                    }
                  } else {
                    taxes_arraylist = new List();
                  }
                } else {
                  taxes_arraylist = new List();
                }
                addselecteditemwithsize.add(MenuCartItemapi(
                    buyitems_arraylist[h].itemId,
                    buyitems_arraylist[h].menuId,
                    buyitems_arraylist[h].menuGroupId,
                    buyitems_arraylist[h].itemName + " " + item_size,
                    total_price.toString(),
                    buyitems_arraylist[h].quantity.toString(),
                    total_price.toString(),
                    "",
                    [],
                    [],
                    buyitems_arraylist[h].itemType,
                    buyitems_arraylist[h].discountId,
                    buyitems_arraylist[h].discountName,
                    "",
                    buyitems_arraylist[h].discountType,
                    buyitems_arraylist[h].discountValue,
                    double.parse(buyitems_arraylist[h].discountAmount.toString()),
                    "",
                    "",
                    false,
                    widget.bogoid,
                    "",
                    true,
                    false,
                    false,
                    taxes_arraylist));
              }
              //print(h.toString() + "chaitanya" + comboitems_arraylist[h].sizeList.length.toString());
              if (buyitems_arraylist[h].sizeList.length > 0) {
                for (int i = 0; i < buyitems_arraylist[h].sizeList.length; i++) {
                  if (i == 0) {
                    sizelist_selectpos.add(0);
                  }
                }
              } else {
                sizelist_selectpos.add(-1);
              }
            }
            //print("abcddd---"+sizelist_selectpos.length.toString());
            totalsizelist_selectpos.add(sizelist_selectpos);

            //print("tetstttt---"+totalsizelist_selectpos[0].length.toString());
            //print("tetstt---"+totalsizelist_selectpos[0][0].toString());
          });
        });
      }
    }
      });
    });
    UserRepository().getbogo_with_types().then((cartList) {
      setState(() {
        get_savedbogoselected = cartList;
        //print("sirirni" + get_savedcomboselected.length.toString());
        item_index_pos = get_savedbogoselected.indexWhere((element) => element.bogoid == widget.all_buyitems_list[0].id);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
        backgroundColor: Colors.white,
        insetPadding: EdgeInsets.all(20),
        child: Column(children: [
          Expanded(
              child: SingleChildScrollView(
                  child: Container(
                      //height: MediaQuery.of(context).size.height,
                      child: Expanded(
                          child: Column(
            children: <Widget>[
              Container(
                  margin: EdgeInsets.only(left: 10, top: 10, right: 10),
                  //padding: EdgeInsets.all(5),
                  //height: MediaQuery.of(context).size.height * 0.2,
                  color: dashboard_bg,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: widget.all_buyitems_list.length,
                        itemBuilder: (BuildContext context1, index) {
                          return Container(
                            margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Padding(
                                        padding: EdgeInsets.only(left: 5, bottom: 10),
                                        child: Text(
                                          widget.all_buyitems_list[index].name,
                                          style:
                                              TextStyle(color: login_passcode_text, fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w700),
                                          textAlign: TextAlign.start,
                                        )),
                                  ],
                                ),
                                _loading
                                    ? Center(
                                        child: SpinKitFadingCircle(color: Colors.lightBlueAccent),
                                      )
                                    : ListView.builder(
                                        physics: NeverScrollableScrollPhysics(),
                                        shrinkWrap: true,
                                        itemCount: items_arraymenulist[index].length,
                                        itemBuilder: (BuildContext context1, j) {
                                          return Column(
                                            children: [
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  Expanded(
                                                      flex: 3,
                                                      child: Padding(
                                                          padding: EdgeInsets.only(
                                                            left: 0,
                                                            right: 5,
                                                          ),
                                                          child: Text(
                                                            items_arraymenulist[index][j].itemName.toString(),
                                                            style: TextStyle(
                                                                color: login_passcode_text,
                                                                fontSize: 16,
                                                                fontFamily: 'Poppins',
                                                                fontWeight: FontWeight.w500),
                                                            textAlign: TextAlign.left,
                                                          ))),
                                                  get_savedbogoselected[item_index_pos].menuItemStatus[index] == j.toString()
                                                      ? IconButton(
                                                          padding: EdgeInsets.only(left: 0.0),
                                                          icon: Image.asset("images/check.png", width: 24, height: 24),
                                                          onPressed: () {
                                                            Scaffold.of(context).openDrawer();
                                                          },
                                                        )
                                                      : Expanded(
                                                          flex: 1,
                                                          child: Padding(
                                                              padding: EdgeInsets.only(
                                                                left: 0,
                                                                right: 5,
                                                              ),
                                                              child: FlatButton(
                                                                  //minWidth: 30,
                                                                  color: Colors.lightBlueAccent,
                                                                  child: Text("Add",
                                                                      style: TextStyle(
                                                                          fontSize: 14,
                                                                          fontFamily: 'Poppins',
                                                                          fontWeight: FontWeight.w800,
                                                                          color: add_food_item_bg)),
                                                                  onPressed: () {
                                                                    print(j.toString() + "jjjjj" + index.toString());
                                                                    setState(() {
                                                                      get_savedbogoselected[item_index_pos].menuItemStatus[index] = j.toString();
                                                                      UserRepository().savecombo_with_types(get_savedbogoselected);
                                                                    });
                                                                    String _itemname = "";
                                                                    if (totalsizelist_selectpos[index][j] == -1) {
                                                                      //print("bcbccggcgc"+"--"+items_arraymenulist[index][j].itemName);
                                                                      _itemname = items_arraymenulist[index][j].itemName;
                                                                    } else {
                                                                      //print(totalsizelist_selectpos[index][j].toString()+"--"+items_arraymenulist[index][j].itemName+"--"+items_arraymenulist[index][j].sizeList[totalsizelist_selectpos[index][j]].sizeName);
                                                                      _itemname = items_arraymenulist[index][j].itemName +
                                                                          " " +
                                                                          items_arraymenulist[index][j]
                                                                              .sizeList[totalsizelist_selectpos[index][j]]
                                                                              .sizeName;
                                                                    }

                                                                    //print(widget.all_comboitems_list[index]);
                                                                    print(_itemname);
                                                                    double total_price = items_arraymenulist[index][j]
                                                                        .sizeList[totalsizelist_selectpos[index][j]]
                                                                        .price;
                                                                    print("TOTALPRICE-----" + total_price.toString());

                                                                    //TAX CALCULATION
                                                                    print("TAXINCLUDEOPTION" +
                                                                        items_arraymenulist[index][j].taxIncludeOption.toString());
                                                                    if (items_arraymenulist[index][j].taxIncludeOption == true) {
                                                                      if (items_arraymenulist[index][j].taxesList.length > 0) {
                                                                        /* print("TAXRATELENGTH" +
                        items_arraymenulist[index][j].taxesList.length
                            .toString());*/
                                                                        for (int g = 0; g < items_arraymenulist[index][j].taxesList.length; g++) {
                                                                          taxType = items_arraymenulist[index][j].taxesList[g].taxType.toString();
                                                                          var taxrate = items_arraymenulist[index][j].taxesList[g].taxRate.toString();
                                                                          if (taxType == "0") {
                                                                            taxType = "disable";
                                                                            taxTypeId = 0;
                                                                            tax = 0.00;
                                                                            taxType = taxTypeId.toString();
                                                                          } else if (taxType == "1") {
                                                                            var taxRate = total_price *
                                                                                (items_arraymenulist[index][j].taxesList[g].taxRate.toDouble() / 100);

                                                                            if (items_arraymenulist[index][j].taxesList[g].roundingOptions == 1) {
                                                                              taxRate = halfEven(taxRate, 2);
                                                                            } else if (items_arraymenulist[index][j].taxesList[g].roundingOptions ==
                                                                                2) {
                                                                              taxRate = halfUp(taxRate, 2);
                                                                            } else if (items_arraymenulist[index][j].taxesList[g].roundingOptions ==
                                                                                3) {
                                                                              taxRate = alwaysDown(taxRate, 2);
                                                                            } else if (items_arraymenulist[index][j].taxesList[g].roundingOptions ==
                                                                                4) {
                                                                              taxRate = getNumber(alwaysUp(taxRate), precision: 2);
                                                                            }

                                                                            taxType = "percent";
                                                                            taxTypeId = 1;
                                                                            taxType = taxTypeId.toString();
                                                                            tax = taxRate;
                                                                          } else if (taxType == "2") {
                                                                            taxType = "fixed";
                                                                            taxTypeId = 2;
                                                                            tax = total_price *
                                                                                items_arraymenulist[index][j].taxesList[g].taxRate.toDouble();
                                                                            taxType = taxTypeId.toString();
                                                                          } else if (taxType == "3") {
                                                                            taxType = "taxTable";
                                                                            taxTypeId = 3;
                                                                            if (items_arraymenulist[index][j].taxesList[g].taxTable.length > 0) {
                                                                              for (int t = 0;
                                                                                  t < items_arraymenulist[index][j].taxesList[g].taxTable.length;
                                                                                  t++) {
                                                                                if (total_price >=
                                                                                        double.parse(items_arraymenulist[index][j]
                                                                                            .taxesList[g]
                                                                                            .taxTable[t]
                                                                                            .from) &&
                                                                                    total_price <=
                                                                                        double.parse(items_arraymenulist[index][j]
                                                                                            .taxesList[g]
                                                                                            .taxTable[t]
                                                                                            .to)) {
                                                                                  tax += double.parse(items_arraymenulist[index][j]
                                                                                      .taxesList[g]
                                                                                      .taxTable[t]
                                                                                      .taxApplied);
                                                                                }
                                                                              }
                                                                            } else {
                                                                              tax = 0.00;
                                                                            }
                                                                            taxType = taxTypeId.toString();
                                                                          }

                                                                          var sendTaxRates = SendTaxRatesModel(
                                                                              items_arraymenulist[index][j].taxesList[g].enableTakeOutRate,
                                                                              items_arraymenulist[index][j].taxesList[g].importId,
                                                                              items_arraymenulist[index][j].taxesList[g].orderValue,
                                                                              items_arraymenulist[index][j].taxesList[g].roundingOptions,
                                                                              items_arraymenulist[index][j].taxesList[g].status,
                                                                              items_arraymenulist[index][j].taxesList[g].taxName,
                                                                              items_arraymenulist[index][j].taxesList[g].taxRate,
                                                                              int.parse(taxType),
                                                                              tax_table_arraylist,
                                                                              items_arraymenulist[index][j].taxesList[g].taxid,
                                                                              items_arraymenulist[index][j].taxesList[g].uniqueNumber,
                                                                              tax);

                                                                          taxes_arraylist.add(sendTaxRates);
                                                                        }
                                                                      } else {
                                                                        taxes_arraylist = new List();
                                                                      }
                                                                    } else {
                                                                      taxes_arraylist = new List();
                                                                    }
                                                                    addselecteditemwithsize[index].unitPrice = total_price.toString();
                                                                    addselecteditemwithsize[index].totalPrice = total_price.toString();
                                                                    addselecteditemwithsize[index].itemId = items_arraymenulist[index][j].itemId;
                                                                    addselecteditemwithsize[index].menuId = items_arraymenulist[index][j].menuId;
                                                                    addselecteditemwithsize[index].menuGroupId =
                                                                        items_arraymenulist[index][j].menuGroupId;
                                                                    addselecteditemwithsize[index].itemName = _itemname;
                                                                    addselecteditemwithsize[index].taxesList = taxes_arraylist;
                                                                  })))
                                                ],
                                              ),
                                              items_arraymenulist[index][j].sizeList.length > 0
                                                  ? Container(
                                                      margin: EdgeInsets.only(left: 0, top: 5, right: 10),
                                                      //padding: EdgeInsets.all(5),
                                                      height: MediaQuery.of(context).size.height * 0.1,
                                                      color: dashboard_bg,
                                                      child: ListView.separated(
                                                          separatorBuilder: (context, i) => Divider(
                                                                color: Colors.grey,
                                                                height: 0.1,
                                                              ),
                                                          scrollDirection: Axis.horizontal,
                                                          itemCount: items_arraymenulist[index][j].sizeList.length,
                                                          itemBuilder: (context, i) {
                                                            return InkWell(
                                                              onTap: () {
                                                                setState(() {
                                                                  totalsizelist_selectpos[index][j] = i;
                                                                  default_sizeitem = items_arraymenulist[index][j].sizeList[i].sizeName.toString();
                                                                  String _itemname = "";
                                                                  double total_price;
                                                                  if (totalsizelist_selectpos[index][j] == -1) {
                                                                    _itemname = items_arraymenulist[index][j].itemName;
                                                                    total_price = double.parse(items_arraymenulist[index][j].basePrice);
                                                                  } else {
                                                                    _itemname = items_arraymenulist[index][j].itemName +
                                                                        " " +
                                                                        items_arraymenulist[index][j]
                                                                            .sizeList[totalsizelist_selectpos[index][j]]
                                                                            .sizeName;
                                                                    total_price = items_arraymenulist[index][j]
                                                                        .sizeList[totalsizelist_selectpos[index][j]]
                                                                        .price;
                                                                  }
                                                                  print(_itemname);
                                                                  print("TOTALPRICE-----" + total_price.toString());

                                                                  //TAX CALCULATION
                                                                  print(
                                                                      "TAXINCLUDEOPTION" + items_arraymenulist[index][j].taxIncludeOption.toString());
                                                                  if (items_arraymenulist[index][j].taxIncludeOption == true) {
                                                                    if (items_arraymenulist[index][j].taxesList.length > 0) {
                                                                      for (int g = 0; g < items_arraymenulist[index][j].taxesList.length; g++) {
                                                                        taxType = items_arraymenulist[index][j].taxesList[g].taxType.toString();
                                                                        var taxrate = items_arraymenulist[index][j].taxesList[g].taxRate.toString();
                                                                        if (taxType == "0") {
                                                                          taxType = "disable";
                                                                          taxTypeId = 0;
                                                                          tax = 0.00;
                                                                          taxType = taxTypeId.toString();
                                                                        } else if (taxType == "1") {
                                                                          var taxRate = total_price *
                                                                              (items_arraymenulist[index][j].taxesList[g].taxRate.toDouble() / 100);

                                                                          if (items_arraymenulist[index][j].taxesList[g].roundingOptions == 1) {
                                                                            taxRate = halfEven(taxRate, 2);
                                                                          } else if (items_arraymenulist[index][j].taxesList[g].roundingOptions ==
                                                                              2) {
                                                                            taxRate = halfUp(taxRate, 2);
                                                                          } else if (items_arraymenulist[index][j].taxesList[g].roundingOptions ==
                                                                              3) {
                                                                            taxRate = alwaysDown(taxRate, 2);
                                                                          } else if (items_arraymenulist[index][j].taxesList[g].roundingOptions ==
                                                                              4) {
                                                                            taxRate = getNumber(alwaysUp(taxRate), precision: 2);
                                                                          }

                                                                          taxType = "percent";
                                                                          taxTypeId = 1;
                                                                          taxType = taxTypeId.toString();
                                                                          tax = taxRate;
                                                                        } else if (taxType == "2") {
                                                                          taxType = "fixed";
                                                                          taxTypeId = 2;
                                                                          tax = total_price *
                                                                              items_arraymenulist[index][j].taxesList[g].taxRate.toDouble();
                                                                          taxType = taxTypeId.toString();
                                                                        } else if (taxType == "3") {
                                                                          taxType = "taxTable";
                                                                          taxTypeId = 3;
                                                                          if (items_arraymenulist[index][j].taxesList[g].taxTable.length > 0) {
                                                                            for (int t = 0;
                                                                                t < items_arraymenulist[index][j].taxesList[g].taxTable.length;
                                                                                t++) {
                                                                              if (total_price >=
                                                                                      double.parse(items_arraymenulist[index][j]
                                                                                          .taxesList[g]
                                                                                          .taxTable[t]
                                                                                          .from) &&
                                                                                  total_price <=
                                                                                      double.parse(items_arraymenulist[index][j]
                                                                                          .taxesList[g]
                                                                                          .taxTable[t]
                                                                                          .to)) {
                                                                                tax += double.parse(items_arraymenulist[index][j]
                                                                                    .taxesList[g]
                                                                                    .taxTable[t]
                                                                                    .taxApplied);
                                                                              }
                                                                            }
                                                                          } else {
                                                                            tax = 0.00;
                                                                          }
                                                                          taxType = taxTypeId.toString();
                                                                        }

                                                                        var sendTaxRates = SendTaxRatesModel(
                                                                            items_arraymenulist[index][j].taxesList[g].enableTakeOutRate,
                                                                            items_arraymenulist[index][j].taxesList[g].importId,
                                                                            items_arraymenulist[index][j].taxesList[g].orderValue,
                                                                            items_arraymenulist[index][j].taxesList[g].roundingOptions,
                                                                            items_arraymenulist[index][j].taxesList[g].status,
                                                                            items_arraymenulist[index][j].taxesList[g].taxName,
                                                                            items_arraymenulist[index][j].taxesList[g].taxRate,
                                                                            int.parse(taxType),
                                                                            tax_table_arraylist,
                                                                            items_arraymenulist[index][j].taxesList[g].taxid,
                                                                            items_arraymenulist[index][j].taxesList[g].uniqueNumber,
                                                                            tax);

                                                                        taxes_arraylist.add(sendTaxRates);
                                                                      }
                                                                    } else {
                                                                      taxes_arraylist = new List();
                                                                    }
                                                                  } else {
                                                                    taxes_arraylist = new List();
                                                                  }
                                                                  addselecteditemwithsize[index].unitPrice = total_price.toString();
                                                                  addselecteditemwithsize[index].totalPrice = total_price.toString();
                                                                  addselecteditemwithsize[index].itemId = items_arraymenulist[index][j].itemId;
                                                                  addselecteditemwithsize[index].menuId = items_arraymenulist[index][j].menuId;
                                                                  addselecteditemwithsize[index].menuGroupId =
                                                                      items_arraymenulist[index][j].menuGroupId;
                                                                  addselecteditemwithsize[index].itemName = _itemname;
                                                                  addselecteditemwithsize[index].taxesList = taxes_arraylist;
                                                                });
                                                              },
                                                              child: Card(
                                                                elevation: 2,
                                                                color: totalsizelist_selectpos[index][j] == i ? Colors.lightBlueAccent : Colors.white,
                                                                child: Container(
                                                                  decoration: BoxDecoration(
                                                                    borderRadius:
                                                                        BorderRadius.horizontal(left: Radius.circular(3.00), right: Radius.zero),
                                                                  ),
                                                                  child: Column(
                                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                                    children: <Widget>[
                                                                      Padding(
                                                                          padding: EdgeInsets.only(left: 10, top: 5, right: 10, bottom: 0),
                                                                          child: Text(
                                                                            items_arraymenulist[index][j].sizeList[i].sizeName,
                                                                            style: TextStyle(
                                                                                fontSize: 16,
                                                                                fontFamily: 'Poppins',
                                                                                fontWeight: FontWeight.w500,
                                                                                color: Colors.black),
                                                                          )),
                                                                    ],
                                                                  ),
                                                                ),
                                                              ),
                                                            );
                                                          }))
                                                  : SizedBox(
                                                      height: 10,
                                                    ),
                                            ],
                                          );
                                        }),
                              ],
                            ),
                          );
                        },
                      ),
                    ],
                  ))
            ],
          ))))),
          Align(
              alignment: Alignment.bottomCenter,
              child: InkWell(
                child: Container(
                  // margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                  height: 45,
                  color: Colors.lightBlueAccent,
                  alignment: Alignment.center,
                  child: Padding(
                    padding: EdgeInsets.only(left: 15, top: 0, right: 10),
                    child: Text(
                      "Done",
                      style: TextStyle(color: login_passcode_text, fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w700),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ),
                onTap: () {
                  setState(() {
                    List<MenuCartItemapi> getBogobuyitemadded = new List();
                    for (int i = 0; i < addselecteditemwithsize.length; i++) {
                      print(addselecteditemwithsize[i].itemName);

                      getBogobuyitemadded.add(addselecteditemwithsize[i]);
                    }
                    UserRepository().saveselectedbuyitems(getBogobuyitemadded);
                  });

                  Navigator.pop(context, "Success");

                  Toast.show("Buy Item Selected", context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                },
              ))
        ]));
  }
}

/////GET ITEMS/////
class _GetItemsDialog extends StatefulWidget {
  _GetItemsDialog({
    this.bogoid,
    this.all_getitems_list,
    this.getDiscountValue,
    this.discountValueType,
  });

  final List<Gitems> all_getitems_list;
  final String bogoid;
  final String getDiscountValue;
  final String discountValueType;

  @override
  _GetItemsDialogState createState() => _GetItemsDialogState();
}

class _GetItemsDialogState extends State<_GetItemsDialog> {
  List<MenuItems> getitems_arraylist = new List();
  List<MenuCartItemapi> getBogobuyitemadded = new List();
  List<MenuCartItemapi> setBogogetitemadded = new List();
  int selected_pos = 0;
  String bogo_uniqueid;
  bool _loading = true;
  var taxType = "";
  var taxTypeId = 0;
  var tax = 0.0;
  String roundingOptions = "";
  String res_id = "";
  int roundingOptionId = 0;
  List<SendTaxRatesModel> taxes_arraylist = new List();
  List<TaxTable> tax_table_arraylist = new List();

  @override
  void initState() {
    super.initState();
    // print(widget.main_order_id.toString());

    UserRepository().getselectedbuyitems().then((buyitems) {
      setState(() {
        getBogobuyitemadded = buyitems;
        print("buyitems" + getBogobuyitemadded.length.toString());
      });
    });

    var random = Random();
    var n1 = random.nextInt(9999);
    bogo_uniqueid = "bogo_" + n1.toString();
    print("get itemsrandom" + n1.toString());
    print(widget.all_getitems_list.length);
    print(widget.all_getitems_list[0].type);
    UserRepository().getGenerateOtpDetails().then((restaurantdetails) {
      setState(() {
        res_id = restaurantdetails[0];
    if (widget.all_getitems_list[0].type == "Menu Group")
    {
      //print(widget.all_comboitems_list[g].type);

      GetItemsbyMenuGroupID().getitems(widget.all_getitems_list[0].id, res_id).then((result_allmenuitems) {
        setState(() {
          print(result_allmenuitems);
          getitems_arraylist = result_allmenuitems;
          print(getitems_arraylist.length);
          _loading = false;
        });
      });
    }
    else {
      //print(widget.all_comboitems_list[g].type + "--" + widget.all_comboitems_list[g].id);
      GetItemsbyMenuData().getitems(widget.all_getitems_list[0].id, res_id).then((resultitems) {
        setState(() {
          MenuItemData result_allmenuitems = resultitems.menuItemData;
          //print("result----" + result_allmenuitems.toString());
          //print("Menu Itesm size" + result_allmenuitems.sizeList.length.toString());
          List<slist.SizeList> test = new List();
          for (int s = 0; s < result_allmenuitems.sizeList.length; s++) {
            test.add(slist.SizeList(result_allmenuitems.sizeList[s].price, result_allmenuitems.sizeList[s].sizeName));
          }

          getitems_arraylist.add(MenuItems(
              result_allmenuitems.basePrice,
              result_allmenuitems.diningOptionTaxException,
              result_allmenuitems.diningTaxOption,
              result_allmenuitems.discountAmount,
              result_allmenuitems.discountId,
              result_allmenuitems.discountName,
              result_allmenuitems.discountType,
              result_allmenuitems.discountValue,
              result_allmenuitems.isBogo,
              result_allmenuitems.isCombo,
              result_allmenuitems.itemId,
              result_allmenuitems.itemName,
              result_allmenuitems.itemType,
              result_allmenuitems.menuGroupId,
              result_allmenuitems.menuId,
              result_allmenuitems.pricingStrategy,
              result_allmenuitems.quantity,
              test,
              result_allmenuitems.taxIncludeOption,
              result_allmenuitems.type));
          _loading = false;
        });
      });
    }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
        backgroundColor: Colors.white,
        insetPadding: EdgeInsets.all(20),
        child: SingleChildScrollView(
            child: Container(
                child: _loading
                    ? Center(
                        child: SpinKitFadingCircle(color: Colors.lightBlueAccent),
                      )
                    : Expanded(
                        child: Column(
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.all(10),
                              //padding: EdgeInsets.all(5),
                              //height: MediaQuery.of(context).size.height * 0.2,
                              color: dashboard_bg,
                              child: ListView.builder(
                                  physics: NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: getitems_arraylist.length,
                                  itemBuilder: (BuildContext context1, j) {
                                    return Column(
                                      children: [
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Expanded(
                                                flex: 3,
                                                child: Padding(
                                                    padding: EdgeInsets.only(
                                                      left: 0,
                                                      right: 5,
                                                    ),
                                                    child: Text(
                                                      getitems_arraylist[j].itemName.toString(),
                                                      style: TextStyle(
                                                          color: login_passcode_text,
                                                          fontSize: 16,
                                                          fontFamily: 'Poppins',
                                                          fontWeight: FontWeight.w500),
                                                      textAlign: TextAlign.left,
                                                    ))),
                                            Expanded(
                                                flex: 1,
                                                child: Padding(
                                                    padding: EdgeInsets.only(
                                                      left: 0,
                                                      right: 5,
                                                    ),
                                                    child: FlatButton(
                                                        //minWidth: 30,
                                                        color: Colors.lightBlueAccent,
                                                        child: Text("Add",
                                                            style: TextStyle(
                                                                fontSize: 14,
                                                                fontFamily: 'Poppins',
                                                                fontWeight: FontWeight.w800,
                                                                color: add_food_item_bg)),
                                                        onPressed: () {
                                                          if (getBogobuyitemadded.length > 0) {
                                                            setState(() {
                                                              for (int i = 0; i < getBogobuyitemadded.length; i++) {
                                                                print(getBogobuyitemadded[i].itemName);
                                                                getBogobuyitemadded[i].comboUniqueId = bogo_uniqueid;
                                                                getCartItemModelapi.add(getBogobuyitemadded[i]);
                                                              }
                                                              String _itemname = "";
                                                              double total_price;
                                                              if (getitems_arraylist[j].sizeList.length > 0) {
                                                                _itemname = getitems_arraylist[j].itemName +
                                                                    " " +
                                                                    getitems_arraylist[j].sizeList[selected_pos].sizeName;
                                                                total_price = getitems_arraylist[j].sizeList[selected_pos].price;
                                                              } else {
                                                                _itemname = getitems_arraylist[j].itemName;
                                                                total_price = double.parse(getitems_arraylist[j].basePrice);
                                                              }
                                                              double discount_value;
                                                              print("GETITEMS TOTALPRICE-----" +
                                                                  total_price.toString() +
                                                                  "-----" +
                                                                  widget.getDiscountValue +
                                                                  "-----" +
                                                                  widget.discountValueType);
                                                              if (widget.discountValueType == "percentage") {
                                                                //discount_value = total_price * (double.parse(widget.getDiscountValue)/100);
                                                                discount_value =
                                                                    total_price - (total_price * (double.parse(widget.getDiscountValue) / 100));
                                                              } else {
                                                                discount_value = total_price - double.parse(widget.getDiscountValue);
                                                              }
                                                              print("Discount TOTALPRICE-----" +
                                                                  total_price.toString() +
                                                                  "-----" +
                                                                  discount_value.toString());
                                                              //TAX CALCULATION
                                                              print("TAXINCLUDEOPTION" + getitems_arraylist[j].taxIncludeOption.toString());
                                                              if (getitems_arraylist[j].taxIncludeOption == true) {
                                                                if (getitems_arraylist[j].taxesList.length > 0) {
                                                                  for (int g = 0; g < getitems_arraylist[j].taxesList.length; g++) {
                                                                    taxType = getitems_arraylist[j].taxesList[g].taxType.toString();
                                                                    var taxrate = getitems_arraylist[j].taxesList[g].taxRate.toString();
                                                                    if (taxType == "0") {
                                                                      taxType = "disable";
                                                                      taxTypeId = 0;
                                                                      tax = 0.00;
                                                                      taxType = taxTypeId.toString();
                                                                    } else if (taxType == "1") {
                                                                      var taxRate =
                                                                          total_price * (getitems_arraylist[j].taxesList[g].taxRate.toDouble() / 100);

                                                                      if (getitems_arraylist[j].taxesList[g].roundingOptions == 1) {
                                                                        taxRate = halfEven(taxRate, 2);
                                                                      } else if (getitems_arraylist[j].taxesList[g].roundingOptions == 2) {
                                                                        taxRate = halfUp(taxRate, 2);
                                                                      } else if (getitems_arraylist[j].taxesList[g].roundingOptions == 3) {
                                                                        taxRate = alwaysDown(taxRate, 2);
                                                                      } else if (getitems_arraylist[j].taxesList[g].roundingOptions == 4) {
                                                                        taxRate = getNumber(alwaysUp(taxRate), precision: 2);
                                                                      }

                                                                      taxType = "percent";
                                                                      taxTypeId = 1;
                                                                      taxType = taxTypeId.toString();
                                                                      tax = taxRate;
                                                                    } else if (taxType == "2") {
                                                                      taxType = "fixed";
                                                                      taxTypeId = 2;
                                                                      tax = total_price * getitems_arraylist[j].taxesList[g].taxRate.toDouble();
                                                                      taxType = taxTypeId.toString();
                                                                    } else if (taxType == "3") {
                                                                      taxType = "taxTable";
                                                                      taxTypeId = 3;
                                                                      if (getitems_arraylist[j].taxesList[g].taxTable.length > 0) {
                                                                        for (int t = 0; t < getitems_arraylist[j].taxesList[g].taxTable.length; t++) {
                                                                          if (total_price >=
                                                                                  double.parse(getitems_arraylist[j].taxesList[g].taxTable[t].from) &&
                                                                              total_price <=
                                                                                  double.parse(getitems_arraylist[j].taxesList[g].taxTable[t].to)) {
                                                                            tax += double.parse(
                                                                                getitems_arraylist[j].taxesList[g].taxTable[t].taxApplied);
                                                                          }
                                                                        }
                                                                      } else {
                                                                        tax = 0.00;
                                                                      }
                                                                      taxType = taxTypeId.toString();
                                                                    }

                                                                    var sendTaxRates = SendTaxRatesModel(
                                                                        getitems_arraylist[j].taxesList[g].enableTakeOutRate,
                                                                        getitems_arraylist[j].taxesList[g].importId,
                                                                        getitems_arraylist[j].taxesList[g].orderValue,
                                                                        getitems_arraylist[j].taxesList[g].roundingOptions,
                                                                        getitems_arraylist[j].taxesList[g].status,
                                                                        getitems_arraylist[j].taxesList[g].taxName,
                                                                        getitems_arraylist[j].taxesList[g].taxRate,
                                                                        int.parse(taxType),
                                                                        tax_table_arraylist,
                                                                        getitems_arraylist[j].taxesList[g].taxid,
                                                                        getitems_arraylist[j].taxesList[g].uniqueNumber,
                                                                        tax);

                                                                    taxes_arraylist.add(sendTaxRates);
                                                                  }
                                                                } else {
                                                                  taxes_arraylist = new List();
                                                                }
                                                              } else {
                                                                taxes_arraylist = new List();
                                                              }

                                                              setBogogetitemadded.add(MenuCartItemapi(
                                                                  getitems_arraylist[j].itemId,
                                                                  getitems_arraylist[j].menuId,
                                                                  getitems_arraylist[j].menuGroupId,
                                                                  _itemname,
                                                                  total_price.toString(),
                                                                  getitems_arraylist[j].quantity.toString(),
                                                                  total_price.toString(),
                                                                  "",
                                                                  [],
                                                                  [],
                                                                  getitems_arraylist[j].itemType,
                                                                  getitems_arraylist[j].discountId,
                                                                  getitems_arraylist[j].discountName,
                                                                  "",
                                                                  getitems_arraylist[j].discountType,
                                                                  getitems_arraylist[j].discountValue,
                                                                  double.parse(discount_value.toStringAsFixed(2)),
                                                                  "",
                                                                  "",
                                                                  false,
                                                                  widget.bogoid,
                                                                  bogo_uniqueid,
                                                                  true,
                                                                  false,
                                                                  false,
                                                                  taxes_arraylist));
                                                            });

                                                              for (int i = 0; i < setBogogetitemadded.length; i++) {
                                                                getCartItemModelapi.add(setBogogetitemadded[i]);
                                                              }
                                                            _savecartList(getCartItemModelapi);
                                                            Toast.show("Added to Cart", context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                                                            getBogobuyitemadded.clear();
                                                            UserRepository().saveselectedbuyitems([]);
                                                            print(getBogobuyitemadded.length);
                                                            Navigator.pop(context, "Success");
                                                          } else {
                                                            Toast.show("Select Buy Items before adding Get Items", context,
                                                                duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                                                          }
                                                        })))
                                          ],
                                        ),
                                        getitems_arraylist[j].sizeList.length > 0
                                            ? Container(
                                                margin: EdgeInsets.only(left: 0, top: 5, right: 10),
                                                //padding: EdgeInsets.all(5),
                                                height: MediaQuery.of(context).size.height * 0.1,
                                                color: dashboard_bg,
                                                child: ListView.separated(
                                                    separatorBuilder: (context, i) => Divider(
                                                          color: Colors.grey,
                                                          height: 0.1,
                                                        ),
                                                    scrollDirection: Axis.horizontal,
                                                    itemCount: getitems_arraylist[j].sizeList.length,
                                                    itemBuilder: (context, i) {
                                                      return InkWell(
                                                        onTap: () {
                                                          setState(() {
                                                            selected_pos = i;
                                                          });
                                                        },
                                                        child: Card(
                                                          elevation: 2,
                                                          color: selected_pos == i ? Colors.lightBlueAccent : Colors.white,
                                                          child: Container(
                                                            decoration: BoxDecoration(
                                                              borderRadius: BorderRadius.horizontal(left: Radius.circular(3.00), right: Radius.zero),
                                                            ),
                                                            child: Column(
                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                              children: <Widget>[
                                                                Padding(
                                                                    padding: EdgeInsets.only(left: 10, top: 5, right: 10, bottom: 0),
                                                                    child: Text(
                                                                      getitems_arraylist[j].sizeList[i].sizeName,
                                                                      style: TextStyle(
                                                                          fontSize: 16,
                                                                          fontFamily: 'Poppins',
                                                                          fontWeight: FontWeight.w500,
                                                                          color: Colors.black),
                                                                    )),
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                      );
                                                    }))
                                            : SizedBox(
                                                height: 10,
                                              ),
                                      ],
                                    );
                                  })),
                        ],
                      )))));
  }

  _savecartList(cartlist) async {
    final prefs = await SharedPreferences.getInstance();
    final key1 = 'save_to_cartlist';
    final cartlist_value = jsonEncode(cartlist);
    prefs.setString(key1, cartlist_value);
    print('savedCART $cartlist_value');
  }
}

class CancelDialogs {
  static Future<void> showLoadingDialog(BuildContext context, GlobalKey key) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(key: key, backgroundColor: Colors.black54, children: <Widget>[
                Center(
                  child: Column(children: [
                    CircularProgressIndicator(),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Cancelling....",
                      style: TextStyle(color: Colors.lightBlueAccent),
                    )
                  ]),
                )
              ]));
        });
  }
}
