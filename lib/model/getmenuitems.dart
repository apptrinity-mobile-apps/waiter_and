import 'dart:ffi';

class Getmenuitems {
  List<MenuItem> menuItem;
  int responseStatus;
  String result;

  Getmenuitems({this.menuItem, this.responseStatus, this.result});

  Getmenuitems.fromJson(Map<String, dynamic> json) {
    if (json['menuItem'] != null) {
      menuItem = new List<MenuItem>();
      json['menuItem'].forEach((v) { menuItem.add(new MenuItem.fromJson(v)); });
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.menuItem != null) {
      data['menuItem'] = this.menuItem.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class MenuItem {
  List<AplicableTaxRates> aplicableTaxRates;
  String basePrice;
  bool diningTaxOption;
  bool discountApplicable;
  String id;
  String menuGroupId;
  String menuId;
  String name;
  int priceProvider;
  int pricingStrategy;
  String restaurantId;
  List<SizeList> sizeList;
  bool taxIncludeOption;
  List<TimePriceList> timePriceList;

  MenuItem({this.aplicableTaxRates, this.basePrice, this.diningTaxOption, this.discountApplicable, this.id, this.menuGroupId, this.menuId, this.name, this.priceProvider, this.pricingStrategy, this.restaurantId, this.sizeList, this.taxIncludeOption, this.timePriceList});

  MenuItem.fromJson(Map<String, dynamic> json) {
    if (json['aplicableTaxRates'] != null) {
      aplicableTaxRates = new List<AplicableTaxRates>();
      json['aplicableTaxRates'].forEach((v) { aplicableTaxRates.add(new AplicableTaxRates.fromJson(v)); });
    }
    basePrice = json['basePrice'];
    diningTaxOption = json['diningTaxOption'];
    discountApplicable = json['discountApplicable'];
    id = json['id'];
    menuGroupId = json['menuGroupId'];
    menuId = json['menuId'];
    name = json['name'];
    priceProvider = json['priceProvider'];
    pricingStrategy = json['pricingStrategy'];
    restaurantId = json['restaurantId'];
    if (json['sizeList'] != null) {
      sizeList = new List<SizeList>();
      json['sizeList'].forEach((v) { sizeList.add(new SizeList.fromJson(v)); });
    }
    taxIncludeOption = json['taxIncludeOption'];
    if (json['timePriceList'] != null) {
      timePriceList = new List<TimePriceList>();
      json['timePriceList'].forEach((v) { timePriceList.add(new TimePriceList.fromJson(v)); });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.aplicableTaxRates != null) {
      data['aplicableTaxRates'] = this.aplicableTaxRates.map((v) => v.toJson()).toList();
    }
    data['basePrice'] = this.basePrice;
    data['diningTaxOption'] = this.diningTaxOption;
    data['discountApplicable'] = this.discountApplicable;
    data['id'] = this.id;
    data['menuGroupId'] = this.menuGroupId;
    data['menuId'] = this.menuId;
    data['name'] = this.name;
    data['priceProvider'] = this.priceProvider;
    data['pricingStrategy'] = this.pricingStrategy;
    data['restaurantId'] = this.restaurantId;
    if (this.sizeList != null) {
      data['sizeList'] = this.sizeList.map((v) => v.toJson()).toList();
    }
    data['taxIncludeOption'] = this.taxIncludeOption;
    if (this.timePriceList != null) {
      data['timePriceList'] = this.timePriceList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class AplicableTaxRates {
  /*bool default;*/
  bool enableTakeOutRate;
  String importId;
  int orderValue;
  int roundingOptions;
  int status;
  String taxName;
  double taxRate;
  List<TaxTable> taxTable;
  int taxType;
  String taxid;
  int uniqueNumber;

  AplicableTaxRates({/*this.default,*/ this.enableTakeOutRate, this.importId, this.orderValue, this.roundingOptions, this.status, this.taxName, this.taxRate, this.taxTable, this.taxType, this.taxid, this.uniqueNumber});

  AplicableTaxRates.fromJson(Map<String, dynamic> json) {
   /* default = json['default'];*/
    enableTakeOutRate = json['enableTakeOutRate'];
    importId = json['importId'];
    orderValue = json['orderValue'];
    roundingOptions = json['roundingOptions'];
    status = json['status'];
    taxName = json['taxName'];
    taxRate = json['taxRate'];
    if (json['taxTable'] != null) {
    taxTable = new List<TaxTable>();
    json['taxTable'].forEach((v) { taxTable.add(new TaxTable.fromJson(v)); });
    }
    taxType = json['taxType'];
    taxid = json['taxid'];
    uniqueNumber = json['uniqueNumber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    /*data['default'] = this.default;*/
    data['enableTakeOutRate'] = this.enableTakeOutRate;
    data['importId'] = this.importId;
    data['orderValue'] = this.orderValue;
    data['roundingOptions'] = this.roundingOptions;
    data['status'] = this.status;
    data['taxName'] = this.taxName;
    data['taxRate'] = this.taxRate;
    if (this.taxTable != null) {
    data['taxTable'] = this.taxTable.map((v) => v.toJson()).toList();
    }
    data['taxType'] = this.taxType;
    data['taxid'] = this.taxid;
    data['uniqueNumber'] = this.uniqueNumber;
    return data;
  }
}

class SizeList {
  double price;
  String sizeName;

  SizeList({this.price, this.sizeName});

  SizeList.fromJson(Map<String, dynamic> json) {
    price = json['price'];
    sizeName = json['sizeName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['price'] = this.price;
    data['sizeName'] = this.sizeName;
    return data;
  }
}


class TimePriceList {
  List<String> days;
  double price;
  String timeFrom;
  String timeTo;

  TimePriceList({this.days, this.price, this.timeFrom, this.timeTo});

  TimePriceList.fromJson(Map<String, dynamic> json) {
    days = json['days'].cast<String>();
    price = json['price'] as double;
    timeFrom = json['timeFrom'];
    timeTo = json['timeTo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['days'] = this.days;
    data['price'] = this.price;
    data['timeFrom'] = this.timeFrom;
    data['timeTo'] = this.timeTo;
    return data;
  }
}


class TaxTable {
  String from;
  Double priceDifference;
  bool repeat;
  String taxApplied;
  String to;

  TaxTable({ this.from, this.priceDifference, this.repeat, this.taxApplied,this.to});

  TaxTable.fromJson(Map<String, dynamic> json) {
    from = json['from'];
    priceDifference = json['priceDifference'] as Double;
    repeat = json['repeat'];
    taxApplied = json['taxApplied'];
    to = json['to'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['from'] = this.from;
    data['priceDifference'] = this.priceDifference;
    data['repeat'] = this.repeat;
    data['taxApplied'] = this.taxApplied;
    data['to'] = this.to;
    return data;
  }
}