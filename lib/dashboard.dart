import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:intl/intl.dart';
import 'package:toast/toast.dart';
import 'package:waiter/apis/LogOutApi.dart';
import 'package:waiter/appbar.dart';
import 'package:waiter/getOrderList.dart';
import 'package:waiter/profile.dart';
import 'package:waiter/restauran_login_with_code.dart';
import 'package:waiter/session/userRepository.dart';
import 'package:waiter/tableservice.dart';
import 'package:waiter/utils/all_constans.dart';
import 'package:waiter/utils/sizeconfig.dart';
import 'package:waiter/widget/staggered_grid_view.dart';
import 'package:waiter/widget/staggered_tile.dart';

import 'addfood_screen.dart';
import 'apis/CartsRepository.dart';
import 'apis/getallguesttablesapi.dart';
import 'gettableinformation.dart';
import 'login.dart';
import 'model/cartmodelitemsapi.dart';
import 'model/getallguesttableresponse.dart';

/*void pinword() => runApp(PinPutApp());*/

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int cart_count = 0;
  bool enable_searchlist = false;
  bool _loading = true;
  PageController _controller = new PageController();
  String userid = "";
  String employee_id = "";
  String first_name = "";
  String last_name = "";
  String restaurant_id = "";
  String servicearea_id = "";
  String user_id = "";
  double screenheight = 0.0;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  List<GuestTableList> _guesttables;
  List<MenuCartItemapi> __cart_items_list = new List();

  @override
  void initState() {
    super.initState();
    UserRepository().getuserdetails().then((userdetails) {
      print("userdata" + userdetails.length.toString());
      setState(() {
        employee_id = userdetails[0];
        first_name = userdetails[1];
        last_name = userdetails[2];
        //user_id = userdetails[1];
        print("SESSIONDATAFROMLOGIN" + employee_id+"------"+first_name+"-----"+last_name);
        UserRepository().getGenerateOtpDetails().then((restaurantdetails) {
          setState(() {
            restaurant_id = restaurantdetails[0];
            print("restaurant_id" + restaurant_id);
            UserRepository().getServiceAreaandCentreId().then((serviceareadetails) {
              setState(() {
                servicearea_id = serviceareadetails[0];

            GetAllGuestTableApiRepository()
                .checkGetAllGuestTables(
                    restaurant_id, servicearea_id, employee_id)
                .then((guesttablelist) {
              setState(() {
                _guesttables = guesttablelist;
                setState(() {
                  Future.delayed(Duration(seconds: 2), () async {
                    setState(() {
                      CartsRepository().getcartslisting().then((cartList) {
                        setState(() {
                          __cart_items_list = cartList;
                          cart_count = __cart_items_list.length;
                          //_loading = false;
                        });
                      });
                    });
                  });
                  _loading = false;
                });
              });
            });
              });
            });
          });
        });
      });
    });
  }



  @override
  Widget build(BuildContext context) {
SizeConfig().init(context);
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: login_passcode_bg1));
    /*SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp]);*/
    //final screen_width = MediaQuery.of(context).size.width;
    print("Height" + SizeConfig.screenHeight.toString());
    if (SizeConfig.screenHeight >= 600 && SizeConfig.screenHeight < 800) {
      screenheight = 400;
    }
    if (SizeConfig.screenHeight >= 550 && SizeConfig.screenHeight < 600) {
      screenheight = 278;
    }
    if (SizeConfig.screenHeight >= 800) {
      screenheight = 450;
    }
    final screen_height = MediaQuery.of(context).size.height;
    print(screen_height);

    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        appBar: YourAppbar(
          height: SizeConfig.safeBlockHorizontal * 25,
          type: "home",
          count: cart_count,
        ),
        drawer:Drawer(
          child: Container(color:Colors.white,margin:EdgeInsets.fromLTRB(0, 0, 0, 0),child: ListView(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            children: <Widget>[Container(color:login_passcode_text,child:
              DrawerHeader(child: Container(color:login_passcode_text,child: Column(mainAxisAlignment:MainAxisAlignment.spaceEvenly,children: [Padding(
                  padding: EdgeInsets.only(top:5,bottom:5,right: 0),
                  child: Container(
                    width: 60.0,
                    height: 60.0,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          fit: BoxFit.cover, image: NetworkImage('https://i.ibb.co/kSfS1pS/happy-young-waiter-holding-glass-champagne-towel.png')),
                      borderRadius: BorderRadius.all(Radius.circular(8.0)),
                      color: login_passcode_text,
                    ),
                  ),),Column(mainAxisAlignment:MainAxisAlignment.center,children: [Padding(
          padding: EdgeInsets.only(right: 15),
          child:Text(first_name+" "+last_name,
                  style: new TextStyle(
                      color: cart_text,
                      fontSize: 18.0,
                      fontWeight: FontWeight.w700))),Padding(
          padding: EdgeInsets.only(top:5,right: 15),
          child:Text("#Waiter",
                  style: new TextStyle(
                      color: Colors.white,
                      fontSize: 16.0,
                      fontWeight: FontWeight.w400)))],),],)))),
             Container(color:Colors.white,child: Column(children: [ListTile(
               title: Row(children: [Padding(
                   padding: EdgeInsets.only(right: 15),
                   child: Image.asset(
                     'images/menu_profile.png',
                     height: 20,
                     width: 20,
                   )),RichText(
                   text: TextSpan(children: [
                     TextSpan(
                         text: "Profile",
                         style: new TextStyle(
                             fontSize: 18,
                             color: login_passcode_text,
                             fontFamily: 'Poppins',
                             fontWeight: FontWeight.w500))
                   ]))],),
               onTap: () {
                 Navigator.push(
                   context,
                   MaterialPageRoute(
                       builder: (context) =>
                           Profile(
                           )),
                 );
                 Toast.show("PROFILE", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
                 // What happens after you tap the navigation item
               },
             ),
               Container(
                 margin: EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                 child: Divider(
                   color: cart_viewline,
                 ),
               ),
               ListTile(
                 title: Row(children: [Padding(
                     padding: EdgeInsets.only(right: 15),
                     child: Image.asset(
                       'images/menu_orderhistory.png',
                       height: 20,
                       width: 20,
                     )),RichText(
                     text: TextSpan(children: [
                       TextSpan(
                           text: "Order History",
                           style: new TextStyle(
                               fontSize: 18,
                               color: login_passcode_text,
                               fontFamily: 'Poppins',
                               fontWeight: FontWeight.w500))
                     ]))],),
                 onTap: () {
                   Toast.show("ORDER HISTORY", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
                   Navigator.push(
                     context,
                     MaterialPageRoute(
                         builder: (context) =>
                             GetOrderList(
                             )),
                   );
                   // What happens after you tap the navigation item
                 },
               ), Container(
                 margin: EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                 child: Divider(
                   color: cart_viewline,
                 ),
               ), ListTile(
                 title: Row(children: [Padding(
                     padding: EdgeInsets.only(right: 15),
                     child: Image.asset(
                       'images/menu_logout.png',
                       height: 20,
                       width: 20,
                     )),RichText(
                     text: TextSpan(children: [
                       TextSpan(
                           text: "Settings",
                           style: new TextStyle(
                               fontSize: 18,
                               color: login_passcode_text,
                               fontFamily: 'Poppins',
                               fontWeight: FontWeight.w500))
                     ]))],),
                 onTap: () {
                   Toast.show("Logout", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
                   // What happens after you tap the navigation item
                 },
               ), Container(
                 margin: EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                 child: Divider(
                   color: cart_viewline,
                 ),
               ),ListTile(
                 title: Row(children: [Padding(
                     padding: EdgeInsets.only(right: 15),
                     child: Image.asset(
                       'images/menu_logout.png',
                       height: 20,
                       width: 20,
                     )),RichText(
                     text: TextSpan(children: [
                       TextSpan(
                           text: "Log Out",
                           style: new TextStyle(
                               fontSize: 18,
                               color: login_passcode_text,
                               fontFamily: 'Poppins',
                               fontWeight: FontWeight.w500))
                     ]))],),
                 onTap: () {
                   LogOut();
                   // What happens after you tap the navigation item
                 },
               ),],),)
            ],
          ),),
        ),
        body: _loading
            ? Center(
                child: SpinKitFadingCircle(color: Colors.lightBlueAccent),
              )
            : /*_guesttables.length >0 ?*/ Container(
                color: Colors.white,
                child: Column(
                  children: [
                    Container(
                      color: dashboard_bg,
                      margin: EdgeInsets.all(15),
                      height: screenheight,
                      child: Column(children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                                padding: EdgeInsets.only(left: 15, top: 10),
                                child: new Image.asset(
                                  "images/dinning_dashboard.png",
                                  height: 48,
                                  width: 48,
                                )),
                            Container(alignment:Alignment.center,
                              padding: EdgeInsets.only(left: 15, top: 10),
                              child: Text("Table Situations",
                                  style: TextStyle(
                                      color: login_passcode_text,
                                      fontSize: 16,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w600)),
                            ),
                          ],
                        ),
                        Expanded(
                            flex: 1,
                            child: AnimationLimiter(
                                child: StaggeredGridView.countBuilder(
                                    padding: EdgeInsets.fromLTRB(
                                        12.0, 10, 12.0, 0.0),
                                    primary: false,
                                    crossAxisCount: 6,
                                    mainAxisSpacing: 8,
                                    crossAxisSpacing: 8,
                                    itemCount: _guesttables.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      //print(countValue);
                                      return AnimationConfiguration
                                          .staggeredGrid(
                                              position: index,
                                              duration: const Duration(
                                                  milliseconds: 600),
                                              columnCount: 3,
                                              child: ScaleAnimation(
                                                child: FadeInAnimation(
                                                  child: Container(
                                                      child: InkWell(
                                                          onTap: () {
                                                            print("DASHBOARDORDERID"+_guesttables[index]
                                                                .orderId.toString()+"==="+_guesttables[index]
                                                                    .tableNumber.toString());
                                                            Navigator.push(
                                                              context,
                                                              MaterialPageRoute(
                                                                  builder: (context) =>
                                                                      GetTableInformation(
                                                                          _guesttables[index]
                                                                              .orderId,_guesttables[index]
                                                                          .tableNumber.toString())),
                                                            );
                                                          },
                                                          child: Column(
                                                              children: <
                                                                  Widget>[
                                                                new Card(
                                                                  elevation:
                                                                      5.0,
                                                                  child:
                                                                      new Column(
                                                                    children: <
                                                                        Widget>[
                                                                      Stack(
                                                                        alignment:
                                                                            Alignment.topLeft,
                                                                        children: [
                                                                          Container(
                                                                            margin: EdgeInsets.fromLTRB(
                                                                                10,
                                                                                15,
                                                                                0,
                                                                                0),
                                                                            child:
                                                                                Image.asset(
                                                                              "images/people.png",
                                                                              height: 30,
                                                                              width: 30,
                                                                            ),
                                                                          ),
                                                                          _guesttables[index].tableNumber!=null?
                                                                          Positioned(
                                                                            child:
                                                                                Container(
                                                                              //margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                                              alignment: Alignment.topRight,
                                                                              child: Text(
                                                                                _guesttables[index].tableNumber.toString(),
                                                                                style: TextStyle(color: login_passcode_text, fontSize: SizeConfig.safeBlockHorizontal * 7, fontFamily: 'Poppins', fontWeight: FontWeight.w600),
                                                                                textAlign: TextAlign.right,
                                                                              ),
                                                                              padding: EdgeInsets.fromLTRB(10.0, 4.0, 12.0, 4.0),
                                                                            ),
                                                                          ):SizedBox(),
                                                                        ],
                                                                      ),
                                                                      Container(alignment: Alignment.centerLeft,padding: EdgeInsets.fromLTRB(10, 0, 0, 0), child: Text(_guesttables[index].noOfGuest==null?"0 People":_guesttables[index].noOfGuest.toString() + (' People'), style: TextStyle(color: login_passcode_text, fontSize: SizeConfig.safeBlockHorizontal * 4, fontFamily: 'Poppins', fontWeight: FontWeight.w500))),
                                                                      Container(alignment: Alignment.centerLeft,
                                                                          padding: EdgeInsets.fromLTRB(10, 15, 0, 10),
                                                                          child: Text(
                                                                            DateTimeConverter(_guesttables[index].createdOn.toString()),
                                                                            style: TextStyle(fontSize: SizeConfig.safeBlockHorizontal * 3.2,   color: Colors.grey, fontFamily: 'Poppins', fontWeight: FontWeight.w500),textAlign: TextAlign.left,
                                                                          )),

                                                                    ],
                                                                  ),
                                                                )
                                                              ]))),
                                                ),
                                              ));
                                    },
                                    staggeredTileBuilder: (int index) =>
                                        new StaggeredTile.fit(2))))
                      ]),
                    ),
                    Container(
                        margin: EdgeInsets.fromLTRB(75, 0, 75, 0),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            color: login_passcode_bg1,
                            borderRadius: BorderRadius.circular(0)),
                        child: InkWell(
                            child: FlatButton(
                                child: Text("TABLE SERVICE",
                                    style: TextStyle(
                                        fontSize:
                                            SizeConfig.safeBlockHorizontal * 4,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.w800,
                                        color: Colors.white)),
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              TableService()));
                                }))),
                    Container(
                        margin: EdgeInsets.fromLTRB(75, 20, 75, 0),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            color: dashboard_quick_order,
                            borderRadius: BorderRadius.circular(0)),
                        child: InkWell(
                            child: FlatButton(
                                child: Text("QUICK ORDER",
                                    style: TextStyle(
                                        fontSize:
                                            SizeConfig.safeBlockHorizontal * 4,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.w800,
                                        color: Colors.white)),
                                onPressed: () {
                                  var header_bool = false;
                                  UserRepository.save_TablenumbernGuests("0","0");
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              AddFood(false,"","")));
                                })))
                  ],
                )) /*:SizedBox()*/);
  }

  DateTimeConverter(String createddate) {
    print("CREATEDDATE"+createddate.toString());
    var now = DateFormat('E, d MMM yyyy HH:mm:ss').parse(createddate);
    String formattedTime = DateFormat.Hm().format(now);
    print("DATETIME======" + formattedTime);
    return formattedTime.toString();
  }

  LogOut() {
    LogoutRepository().logOut(restaurant_id,employee_id,"5f894acdb7c2fbf9edf286ff").then((value){
      print(value);
      if(value.responseStatus == 0){
        Navigator
            .of(
            _keyLoader
                .currentContext,
            rootNavigator: true)
            .pop();
        Toast.show("Invalid Credentials", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
      }else if(value.responseStatus == 1){
        print("LOGOUTRESPONSE "+"--"+ value.result+"--"+value.responseStatus.toString());
        setState(
                () {
                  /*Navigator
                      .of(
                      _keyLoader
                          .currentContext,
                      rootNavigator: true).pop();*/
                  Toast.show(value.result, context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
                  UserRepository.Clearall();
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => RestauranLoginCode()),
              );
            }
        );
      }
    });
  }


}
