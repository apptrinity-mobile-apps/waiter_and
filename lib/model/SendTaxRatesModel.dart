import 'getitemmodifiersresponse.dart';
import 'getitemmodifierssubmitresponse.dart';
import 'getmenuitems.dart';

class SendTaxRatesModel {
  //bool default;


  bool enableTakeOutRate;
  String importId;
  int orderValue;
  int roundingOptions;
  int status;
  String taxName;
  double taxRate;
  int taxType;
 /* int taxTypeId;*/
  List<TaxTable> taxTable;
  String taxid;
  int uniqueNumber;
  double tax;

  SendTaxRatesModel(
      this.enableTakeOutRate, this.importId, this.orderValue, this.roundingOptions, this.status, this.taxName, this.taxRate,this.taxType,/*this.taxTypeId,*/ this.taxTable,  this.taxid, this.uniqueNumber,this.tax);

  SendTaxRatesModel.fromJson(Map<String, dynamic> json) {




    enableTakeOutRate = json['enableTakeOutRate'];
    importId = json['importId'] as String;
    orderValue = json['orderValue'] as int;
    roundingOptions = json['roundingOptions'] as int;
    status = json['status'] as int;
    taxName = json['taxName'] as String;
    taxRate = json['taxRate'] as double;
    taxType = json['taxType'];
 /*   taxTypeId = json['taxTypeId'] as int;*/
    if (json['taxTable'] != null) {
      taxTable = new List<TaxTable>();
      json['taxTable'].forEach((v) {
        taxTable.add(new TaxTable.fromJson(v));
      });
    }
    taxid = json['taxid'] as String;
    uniqueNumber = json['uniqueNumber'] as int;
    tax = json['tax'] as double;
  }


  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['enableTakeOutRate'] = this.enableTakeOutRate;
    data['importId'] = this.importId;
    data['orderValue'] = this.orderValue;
    data['roundingOptions'] = this.roundingOptions;
    data['status'] = this.status;
    data['taxName'] = this.taxName;
    data['taxRate'] = this.taxRate;
    data['taxType'] = this.taxType;
  /*  data['taxTypeId'] = this.taxTypeId;*/
    if (this.taxTable != null) {
      data['taxTable'] = this.taxTable.map((v) => v.toJson()).toList();
    }
    data['taxid'] = this.taxid;
    data['uniqueNumber'] = this.uniqueNumber;
    data['tax'] = this.tax;
    return data;
  }

}




