
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:waiter/splashscreen.dart';
import 'package:waiter/utils/sizeconfig.dart';



void main() {

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.


  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        // body: PaymentScreen("","","","",""),
        body: SplashScreen(),
      ),
    );
  }
}