

import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:waiter/restauran_login_with_code.dart';
import 'package:waiter/session/userRepository.dart';

import 'dashboard.dart';
import 'login.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  String is_Login_session = "";
  String is_Login_session_waiter = "";
  @override
  void initState() {
    super.initState();

    UserRepository().getRestaurant_id().then((getIsLogin) {
      setState(() {
        UserRepository().getuserdetails().then((getIsLoginWaiter){
          setState((){
        is_Login_session = getIsLogin[1];
        is_Login_session_waiter = getIsLoginWaiter[3];
        print("ISLOGIN=====" + is_Login_session.toString() +"------"+is_Login_session_waiter);
        if(is_Login_session == "true"){
          if(is_Login_session_waiter == "true"){
            Timer(
                Duration(seconds: 3),
                    () => Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => HomePage()),
                ));
          }else if(is_Login_session_waiter == ""){
            Timer(
                Duration(seconds: 3),
                    () => Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyApp()),
                ));
          }else{
            Timer(
                Duration(seconds: 3),
                    () => Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyApp()),
                ));
          }
        }else if(is_Login_session == ""){
          print("ISFALSE=====" + is_Login_session.toString());
          Timer(
              Duration(seconds: 3),
                  () => Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (BuildContext context) => RestauranLoginCode())));
        }else{
          Timer(
              Duration(seconds: 3),
                  () => Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (BuildContext context) => RestauranLoginCode())));
        }

          });
        });

      });
    });




  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Center(
        child: Column(children: [Stack(
          children: <Widget>[
            Container(
              alignment: Alignment.center,
              child: Image(
                image: AssetImage('images/splash_bg.png'),
                height: MediaQuery.of(context).size.height * 1.0,
                fit: BoxFit.fill,
                //width: double.infinity,
              ),
            ),
            Container(
                margin: EdgeInsets.fromLTRB(50, 200, 0, 0),
                alignment: Alignment.centerLeft,
                //height: MediaQuery.of(context).size.height * 1.0,
                child: Column(mainAxisAlignment:MainAxisAlignment.center,children: [Image(
                  image: AssetImage('images/logo_dashboard.png'),
                  //fit: BoxFit.fitWidth,
                  //width: double.infinity,
                ),Text(
                  'The Waiter App',
                  style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 22.0),
                )],)),


          ],
        )],),
      ),
    );
  }
}