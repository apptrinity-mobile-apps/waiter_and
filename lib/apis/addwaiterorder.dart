import 'dart:convert';
import 'dart:developer' as dev;

import 'package:requests/requests.dart';
import 'package:waiter/model/addwaiterorderresponse.dart';
import 'package:waiter/model/loginresponse.dart';
import 'package:waiter/model/splitwaiterorderresponse.dart';
import 'package:waiter/utils/all_constans.dart';
import 'package:http/http.dart' as http;
class AddWaiterOrderRepository {

  Future<addwaiterorderresponse> addwaiterorder(orderItems,String orderNumber, String checkNumber,String tableNumber,String tipAmount,double discountAmount,double subTotal,double taxAmount,double totalAmount,String dineInOptionId,String dineInOptionName,String dineInBehaviour,String serviceAreaId,String revenueCenterId,String createdBy,String restaurantId,int creditsUsed,bool haveCustomer,int saveType) async {

    var body = json.encode({'orderItems':orderItems,'orderNumber':orderNumber,'checkNumber':checkNumber,'tableNumber':tableNumber,'tipAmount':tipAmount,'discountAmount':discountAmount,'subTotal':subTotal,'taxAmount':taxAmount,'totalAmount':totalAmount,'dineInOptionId':dineInOptionId,'dineInOptionName':dineInOptionName,'dineInBehaviour':dineInBehaviour,'serviceAreaId':serviceAreaId,'revenueCenterId':revenueCenterId,'createdBy':createdBy,'restaurantId':restaurantId,'creditsUsed':creditsUsed,'haveCustomer':haveCustomer,'saveType':saveType});

    //print(base_url + "ADDWAITERORDERREQUEST"+ body.toString());
    dev.log(base_url + "ADDWAITERORDERREQUEST"+ body.toString());
    /*log(base_url + "ADDWAITERORDERREQUEST"+ body.toString());

    final response = await http.post(base_url + "add_waiter_order",
        body: body,  headers: {
          "Accept": "application/json",
          "Content-Type": "application/x-www-form-urlencoded"
        },
        encoding: Encoding.getByName("utf-8"));

    print(response.body);
    addwaiterorderresponse loginresult_data =
    addwaiterorderresponse.fromJson(jsonDecode(response.body));*/

    dynamic response = await Requests.post(base_url + "add_waiter_order",
        body: body, headers: {'Content-type': 'application/json'});

    addwaiterorderresponse loginresult_data =
    addwaiterorderresponse.fromJson(jsonDecode(response));

    print(loginresult_data.result);
    print(loginresult_data.orderId);
    return loginresult_data;

  }


}


class SplitWaiterOrderRepository {

  Future<splitwaiterorderresponse> splitwaiterorder(body) async {



//dev.log(base_url + "SplitWAITERORDERREQUEST"+ body.toString());


    dynamic response = await Requests.post(base_url + "split_waiter_order",
        body: body, headers: {'Content-type': 'application/json'});

    splitwaiterorderresponse loginresult_data =
    splitwaiterorderresponse.fromJson(jsonDecode(response));

    print(loginresult_data.result+"--"+loginresult_data.responseStatus.toString());
//print(loginresult_data.orderId);
    return loginresult_data;

  }


}


