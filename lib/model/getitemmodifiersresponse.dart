class GetItemModifiersResponse {
  List<ModifiersGroupsList> modifiersGroupsList;
  int responseStatus;
  String result;

  GetItemModifiersResponse({this.modifiersGroupsList, this.responseStatus, this.result});

  GetItemModifiersResponse.fromJson(Map<String, dynamic> json) {
    if (json['modifiers_groups_list'] != null) {
      modifiersGroupsList = new List<ModifiersGroupsList>();
      json['modifiers_groups_list'].forEach((v) {
        modifiersGroupsList.add(new ModifiersGroupsList.fromJson(v));
      });
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.modifiersGroupsList != null) {
      data['modifiers_groups_list'] =
          this.modifiersGroupsList.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class ModifiersGroupsList {
  String itemImage;
  int maxSelections;
  int minSelections;
  String modifierGroupId;
  String modifierGroupName;
  String modifierGroupPrice;
  List<ModifierList> modifierList;
  int pricing;
  int quantity;

  ModifiersGroupsList(
      {this.itemImage,
        this.maxSelections,
        this.minSelections,
        this.modifierGroupId,
        this.modifierGroupName,
        this.modifierGroupPrice,
        this.modifierList,
        this.pricing,
        this.quantity});

  ModifiersGroupsList.fromJson(Map<String, dynamic> json) {
    itemImage = json['itemImage'];
    maxSelections = json['maxSelections'];
    minSelections = json['minSelections'];
    modifierGroupId = json['modifierGroupId'];
    modifierGroupName = json['modifierGroupName'];
    modifierGroupPrice = json['modifierGroupPrice'];
    if (json['modifierList'] != null) {
      modifierList = new List<ModifierList>();
      json['modifierList'].forEach((v) {
        modifierList.add(new ModifierList.fromJson(v));
      });
    }
    pricing = json['pricing'];
    quantity = json['quantity'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['itemImage'] = this.itemImage;
    data['maxSelections'] = this.maxSelections;
    data['minSelections'] = this.minSelections;
    data['modifierGroupId'] = this.modifierGroupId;
    data['modifierGroupName'] = this.modifierGroupName;
    data['modifierGroupPrice'] = this.modifierGroupPrice;
    if (this.modifierList != null) {
      data['modifierList'] = this.modifierList.map((v) => v.toJson()).toList();
    }
    data['pricing'] = this.pricing;
    data['quantity'] = this.quantity;
    return data;
  }
}

class ModifierList {
  String modifierId;
  String modifierName;
  String price;

  ModifierList(this.modifierId, this.modifierName, this.price);

  ModifierList.fromJson(Map<String, dynamic> json) {
    modifierId = json['modifierId'];
    modifierName = json['modifierName'];
    price = json['price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['modifierId'] = this.modifierId;
    data['modifierName'] = this.modifierName;
    data['price'] = this.price;
    return data;
  }


}

