class addcomboitemstatus {
  String comboid;
  List<String> menuItemStatus;

  addcomboitemstatus(this.comboid, this.menuItemStatus);

  addcomboitemstatus.fromJson(Map<String, dynamic> json) {
    comboid = json['comboid'];
    menuItemStatus = json['menu_item_status'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['comboid'] = this.comboid;
    data['menu_item_status'] = this.menuItemStatus;
    return data;
  }
}