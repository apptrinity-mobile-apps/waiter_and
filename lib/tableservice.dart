import 'package:async/async.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:toast/toast.dart';
import 'package:waiter/apis/addtableguestsapi.dart';
import 'package:waiter/session/userRepository.dart';
import 'package:waiter/utils/DialogClass.dart';
import 'package:waiter/utils/all_constans.dart';

import 'addfood_screen.dart';

class TableService extends StatefulWidget {
  @override
  _TableServiceState createState() => _TableServiceState();
}

class _TableServiceState extends State<TableService> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController tablenumber_Controller = TextEditingController();
  TextEditingController noofguestsController = TextEditingController();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  CancelableOperation cancelableOperation;
  String employee_id = "";
  String first_name = "";
  String last_name = "";
  String restaurant_id = "";
  String servicearea_id = "";
  String user_id = "";

  @override
  void initState() {
    super.initState();
    // print(widget.main_order_id.toString());
    UserRepository().getuserdetails().then((userdetails) {
      print("userdata" + userdetails.length.toString());
      setState(() {
        employee_id = userdetails[0];
        first_name = userdetails[1];
        last_name = userdetails[2];
        //user_id = userdetails[1];
        print("SESSIONDATAFROMLOGIN" + employee_id + "------" + first_name + "-----" + last_name);
        UserRepository().getGenerateOtpDetails().then((restaurantdetails) {
          setState(() {
            restaurant_id = restaurantdetails[0];
            print("restaurant_id" + restaurant_id);
            UserRepository().getServiceAreaandCentreId().then((serviceareadetails) {
              setState(() {
                servicearea_id = serviceareadetails[0];
              });
            });
          });
        });
      });
    });


  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomSheet: Container(
            margin: EdgeInsets.fromLTRB(40, 30, 40, 80),
            height: 60,
            width: double.infinity,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: login_passcode_bg1,
                borderRadius: BorderRadius.circular(0)),
            child: Align(
                alignment: Alignment.bottomCenter,
                child: InkWell(
                    child: FlatButton(
                        minWidth: double.infinity,
                        height: double.infinity,
                        child: Text("CONTINUE",
                            style: TextStyle(
                                fontSize: 16,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w800,
                                color: Colors.white)),
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            _formKey.currentState.save();
                            SignDialogs.showLoadingDialog(
                                context, "Loading...", _keyLoader);
                            cancelableOperation?.cancel();
                            CancelableOperation.fromFuture(
                                Future.delayed(Duration(seconds: 1), () {
                              print(restaurant_id+"--------"+employee_id+"------"+servicearea_id);
                              AddTableGuestApiRepository()
                                  .checkAddTableGuest(
                                      tablenumber_Controller.text,
                                      noofguestsController.text,
                                  employee_id,
                                  restaurant_id,
                                  servicearea_id)
                                  .then((result) {
                                print(result);
                                if (result.responseStatus == 0) {

                                  setState(() {
                                    Navigator.of(_keyLoader.currentContext,
                                        rootNavigator: false)
                                        .pop();
                                    Toast.show(result.result, context,
                                        duration: Toast.LENGTH_SHORT,
                                        gravity: Toast.BOTTOM);

                                  });

                                } else {
                                  //print("LOGINRESPONSE "+ result.userDetails.empId+"--"+ result.userDetails.id+"--"+result.userDetails.firstName+"--"+result.userDetails.lastName);
                                  // UserRepository.save_userid(result.userDetails.empId, result.userDetails.id, result.userDetails.firstName, result.userDetails.lastName);
                                  setState(() {
                                    Navigator.of(_keyLoader.currentContext,
                                        rootNavigator: true)
                                        .pop();
                                    UserRepository.save_TablenumbernGuests(tablenumber_Controller.text.toString(),noofguestsController.text.toString());
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => AddFood(true,tablenumber_Controller.text.toString(),noofguestsController.text.toString())),
                                    );
                                  });
                                }
                              });
                            }));
                          }
                        })))),
        appBar: AppBar(
          toolbarHeight: 100,
          automaticallyImplyLeading: false,
          elevation: 0.0,
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text("",
              style: new TextStyle(
                  color: Colors.black,
                  fontSize: 24.0,
                  fontWeight: FontWeight.w500)),
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                padding: EdgeInsets.only(left: 10.0),
                icon:
                    Image.asset("images/back_arrow.png", width: 22, height: 22),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              );
            },
          ),
        ),
        body: Container(
            color: Colors.white,
            margin: EdgeInsets.fromLTRB(10.00, 0.00, 10.00, 0.00),
            child: Column(children: [
              Container(
                child: new Form(
                  key: _formKey, //padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
                  child: new Column(
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 25, vertical: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                height: 5.0,
                              ),
                              Container(
                                  margin: EdgeInsets.only(bottom: 20),
                                  child: Text(
                                    'Enter the Details',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        color: login_passcode_text,
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold),
                                  )),
                              TextFormField(
                                validator: (val) {
                                  if (val.isEmpty) return 'Enter Table Number';
                                  return null;
                                },
                                controller: tablenumber_Controller,
                                obscureText: false,
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: "Enter Table Number",
                                    contentPadding: new EdgeInsets.symmetric(
                                        vertical: 20.0, horizontal: 10.0),
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(0.0))),
                              ),
                              SizedBox(
                                height: 15.0,
                              ),
                              TextFormField(
                                validator: (val) {
                                  if (val.isEmpty) return 'Enter No. of Guests';
                                  return null;
                                },
                                controller: noofguestsController,
                                obscureText: false,
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: "Enter No. of Guests",
                                    contentPadding: new EdgeInsets.symmetric(
                                        vertical: 20.0, horizontal: 10.0),
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(0.0))),
                              ),
                            ],
                          )),
                    ],
                  ),
                ),
              ),
            ])));
  }
}
