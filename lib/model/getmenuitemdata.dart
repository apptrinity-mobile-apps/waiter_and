import 'package:waiter/model/getitemmodifiersresponse.dart';
import 'package:waiter/model/gettableinformationresponse.dart';

class getmenuitemdata {
  MenuItemData menuItemData;
  int responseStatus;
  String result;

  getmenuitemdata({this.menuItemData, this.responseStatus, this.result});

  getmenuitemdata.fromJson(Map<String, dynamic> json) {
    menuItemData = json['menuItemData'] != null ? new MenuItemData.fromJson(json['menuItemData']) : null;
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.menuItemData != null) {
      data['menuItemData'] = this.menuItemData.toJson();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class MenuItemData {
  String basePrice;
  bool diningOptionTaxException;
  bool diningTaxOption;
  int discountAmount;
  String discountId;
  String discountName;
  String discountType;
  int discountValue;
  bool isBogo;
  bool isCombo;
  String itemId;
  String itemName;
  String itemType;
  String menuGroupId;
  String menuId;
  List<ModifierList> modifiersList;
  int pricingStrategy;
  int quantity;
  List<SizeList> sizeList;
  List<SpecialRequestList> specialRequestList;
  bool taxIncludeOption;
  List<TaxesList> taxesList;
  String type;

  MenuItemData({this.basePrice,this.diningOptionTaxException, this.diningTaxOption, this.discountAmount, this.discountId, this.discountName, this.discountType, this.discountValue, this.isBogo, this.isCombo, this.itemId, this.itemName, this.itemType, this.menuGroupId, this.menuId, this.modifiersList, this.pricingStrategy, this.quantity, this.sizeList, this.specialRequestList, this.taxIncludeOption, this.taxesList, this.type});

  MenuItemData.fromJson(Map<String, dynamic> json) {
    basePrice = json['basePrice'];
    diningOptionTaxException = json['diningOptionTaxException'];
    diningTaxOption = json['diningTaxOption'];
    discountAmount = json['discountAmount'];
    discountId = json['discountId'];
    discountName = json['discountName'];
    discountType = json['discountType'];
    discountValue = json['discountValue'];
    isBogo = json['isBogo'];
    isCombo = json['isCombo'];
    itemId = json['itemId'];
    itemName = json['itemName'];
    itemType = json['itemType'];
    menuGroupId = json['menuGroupId'];
    menuId = json['menuId'];
    if (json['modifiersList'] != null) {
      modifiersList = new List<Null>();
      json['modifiersList'].forEach((v) { modifiersList.add(new ModifierList.fromJson(v)); });
    }
    pricingStrategy = json['pricingStrategy'];
    quantity = json['quantity'];

    if (json['sizeList'] != null) {
      sizeList = new List<SizeList>();
      json['sizeList'].forEach((v) { sizeList.add(new SizeList.fromJson(v)); });
    }else{
      sizeList = [];
    }
    if (json['specialRequestList'] != null) {
      specialRequestList = new List<SpecialRequestList>();
      json['specialRequestList'].forEach((v) { specialRequestList.add(new SpecialRequestList.fromJson(v)); });
    }
    taxIncludeOption = json['taxIncludeOption'];
    if (json['taxesList'] != null) {
      taxesList = new List<TaxesList>();
      json['taxesList'].forEach((v) { taxesList.add(new TaxesList.fromJson(v)); });
    }
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['basePrice'] = this.basePrice;
    data['diningOptionTaxException'] = this.diningOptionTaxException;
    data['diningTaxOption'] = this.diningTaxOption;
    data['discountAmount'] = this.discountAmount;
    data['discountId'] = this.discountId;
    data['discountName'] = this.discountName;
    data['discountType'] = this.discountType;
    data['discountValue'] = this.discountValue;
    data['isBogo'] = this.isBogo;
    data['isCombo'] = this.isCombo;
    data['itemId'] = this.itemId;
    data['itemName'] = this.itemName;
    data['itemType'] = this.itemType;
    data['menuGroupId'] = this.menuGroupId;
    data['menuId'] = this.menuId;
    if (this.modifiersList != null) {
      data['modifiersList'] = this.modifiersList.map((v) => v.toJson()).toList();
    }
    data['pricingStrategy'] = this.pricingStrategy;
    data['quantity'] = this.quantity;
    if (this.sizeList != null) {
      data['sizeList'] = this.sizeList.map((v) => v.toJson()).toList();
    }
    if (this.specialRequestList != null) {
      data['specialRequestList'] = this.specialRequestList.map((v) => v.toJson()).toList();
    }
    data['taxIncludeOption'] = this.taxIncludeOption;
    if (this.taxesList != null) {
      data['taxesList'] = this.taxesList.map((v) => v.toJson()).toList();
    }
    data['type'] = this.type;
    return data;
  }
}

class SizeList {
  double price;
  String sizeName;

  SizeList(this.price, this.sizeName);

  SizeList.fromJson(Map<String, dynamic> json) {
    price = json['price'];
    sizeName = json['sizeName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['price'] = this.price;
    data['sizeName'] = this.sizeName;
    return data;
  }
}

class TaxesList {
  bool tdefault;
  bool enableTakeOutRate;
  String importId;
  int orderValue;
  int roundingOptions;
  int status;
  String taxName;
  double taxRate;
  List<TaxesList> taxTable;
  int taxType;
  String taxid;
  int uniqueNumber;

  TaxesList({this.tdefault, this.enableTakeOutRate, this.importId, this.orderValue, this.roundingOptions, this.status, this.taxName, this.taxRate, this.taxTable, this.taxType, this.taxid, this.uniqueNumber});

  TaxesList.fromJson(Map<String, dynamic> json) {
    tdefault = json['default'];
    enableTakeOutRate = json['enableTakeOutRate'];
    importId = json['importId'];
    orderValue = json['orderValue'];
    roundingOptions = json['roundingOptions'];
    status = json['status'];
    taxName = json['taxName'];
    taxRate = json['taxRate'];
    if (json['taxTable'] != null) {
    taxTable = new List<TaxesList>();
    json['taxTable'].forEach((v) { taxTable.add(new TaxesList.fromJson(v)); });
    }
    taxType = json['taxType'];
    taxid = json['taxid'];
    uniqueNumber = json['uniqueNumber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['default'] = this.tdefault;
    data['enableTakeOutRate'] = this.enableTakeOutRate;
    data['importId'] = this.importId;
    data['orderValue'] = this.orderValue;
    data['roundingOptions'] = this.roundingOptions;
    data['status'] = this.status;
    data['taxName'] = this.taxName;
    data['taxRate'] = this.taxRate;
    if (this.taxTable != null) {
    data['taxTable'] = this.taxTable.map((v) => v.toJson()).toList();
    }
    data['taxType'] = this.taxType;
    data['taxid'] = this.taxid;
    data['uniqueNumber'] = this.uniqueNumber;
    return data;
  }
}