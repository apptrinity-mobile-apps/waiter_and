import 'dart:developer';

import 'package:async/async.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:page_indicator/page_indicator.dart';
import 'package:toast/toast.dart';
import 'package:waiter/apis/getProfileApi.dart';
import 'package:waiter/payment2.dart';
import 'package:waiter/receipt.dart';
import 'package:waiter/session/userRepository.dart';
import 'package:waiter/splitorderscreen.dart';
import 'package:waiter/utils/DialogClass.dart';
import 'package:waiter/utils/all_constans.dart';
import 'package:waiter/utils/sizeconfig.dart';

import 'addfood_screen.dart';
import 'apis/getTableInformationApi.dart';
import 'editProfile.dart';
import 'model/gettableinformationresponse.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  GlobalKey<PageContainerState> key = GlobalKey();
  final _formKey = GlobalKey<FormState>();
  int selected_pos = -1;
  List<TableInfo> __cart_items_list = new List();
  int split_length = 0;
  var cart_count_value = 1;
  int cart_count = 0;
  var user_name = "";
  var first_name = "";
  var last_name = "";
  var restaurant_id = "";
  var employee_id = "";
  double screenheight = 0.0;
  double cardview_height = 0.0;
  double toolbar_height = 0.0;
  bool _loading = true;
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmpasswordController = TextEditingController();
  TextEditingController username_Controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    // print(widget.main_order_id.toString());

    print("Height" + SizeConfig.screenHeight.toString());
    if (SizeConfig.screenHeight >= 580 && SizeConfig.screenHeight < 800) {
      toolbar_height = 56;
      screenheight = 460.0;
      cardview_height = 245.0;
    }
    if (SizeConfig.screenHeight >= 800) {
      toolbar_height = 66;
      screenheight = 680.0;
      cardview_height = 450.0;
    }
    UserRepository().getuserdetails().then((userdetails) {
      print("userdata" + userdetails.length.toString());
      setState(() {
        employee_id = userdetails[0];
        //user_id = userdetails[1];
        UserRepository().getGenerateOtpDetails().then((restaurantdetails) {
          setState(() {
            restaurant_id = restaurantdetails[0];

            Future.delayed(Duration(seconds: 2), () async {
              setState(() {

                print("EMPLOYEID === "+employee_id+"RESTAURANTID ==== "+restaurant_id);

                GetProfileApiRepository().getprofiledata(employee_id,restaurant_id).then((value){
                  print("TABLEINFORMATION RESPSTATUS  "+value.responseStatus.toString());
                  debugPrint(base_url + ""+ value.result.toString());
                  if(value.responseStatus == 1){
                    setState(
                            () {
                          _loading = false;
                          //split_length = value.tableInfo.length;
                          user_name = value.profileDict.firstName+""+value.profileDict.lastName;
                          first_name = value.profileDict.firstName;
                          last_name = value.profileDict.lastName;
                          //Toast.show("SUCCESS TABLE INFORMATION", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
                        }
                    );
                  }else if(value.responseStatus == 3){
                    setState(
                            (){
                          _loading = false;
                          Toast.show("NoneType' object has no attribute 'id'", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
                        });


                  } else if(value.responseStatus == 0){
                    setState(() {
                      _loading = false;
                      Toast.show("Invalid Credentials", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
                    });
                  }
                });


              });
            });
          });
        });
      });
    });
  }

  void refresh() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomSheet: Container(
          margin: EdgeInsets.fromLTRB(40, 30, 40, 80),
          height: 60,
          width: double.infinity,
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: login_passcode_bg1,
              borderRadius: BorderRadius.circular(0)),
          child: Align(
              alignment: Alignment.bottomCenter,
              child: InkWell(
                  child: FlatButton(
                      minWidth: double.infinity,
                      height: double.infinity,
                      child: Text("Edit Profile",
                          style: TextStyle(
                              fontSize: 16,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w800,
                              color: Colors.white)),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => EditProfile(first_name,last_name)),
                        );
                      })))),
      appBar: AppBar(
        toolbarHeight: toolbar_height,
        automaticallyImplyLeading: false,
        elevation: 0.0,
        backgroundColor: Colors.white,
        centerTitle: false,
        title: Text("Profile",
            style: new TextStyle(
                color: login_passcode_text,
                fontSize: 18.0,
                fontWeight: FontWeight.w700)),
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              padding: EdgeInsets.only(left: 10.0),
              icon: Image.asset("images/back_arrow.png", width: 22, height: 22),
              onPressed: () {
                Navigator.of(context).pop();
              },
            );
          },
        ),
      ),
      body:_loading
          ? Center(
        child: SpinKitFadingCircle(color: Colors.lightBlueAccent),
      )
          :Container(
          margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
          color: Colors.white,
          child: Container(
            margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
            color: dashboard_bg,
            child:Column(children: [Container(
                constraints: new BoxConstraints.expand(
                  height: 300.0,
                ),
                padding: new EdgeInsets.only(left: 16.0, bottom: 8.0, right: 16.0),
                decoration: new BoxDecoration(
                  //color: add_food_item_bg,
                  color:Color(0xFF003E4B).withOpacity(1.0),
                  image: new DecorationImage(
                    image: new AssetImage('images/man_profile.png'),
                    fit: BoxFit.fitHeight,
                  ),
                ),
                child: new Container(alignment:Alignment.center,child: Stack(
                  children: <Widget>[
                    Image.asset(
                      'images/camera.png',
                      height: 50,
                      width: 50,
                    )
                    ,
                  ],
                ))
            ),  SingleChildScrollView(child: Container(
              child: new Form(
                key: _formKey, //padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
                child: new Column(
                  children: <Widget>[
                    Padding(
                        padding: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(
                              height: 5.0,
                            ),
                            Container(
                                margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
                                child: Text(
                                  'User Name',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(color: text_hint_color, fontSize: SizeConfig.blockSizeHorizontal * 2.8,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.w500,),
                                )),
                            SizedBox(
                              height: 5.0,
                            ),
                            TextFormField(
                              controller: confirmpasswordController,
                             // obscureText: true,
                              enabled: false, //Not clickable and not editable
                              readOnly: true,
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(0.0),
                                    borderSide: BorderSide(
                                      color: login_passcode_bg2,
                                    ),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(0.0),
                                    borderSide: BorderSide(
                                      color: login_form_hint,
                                      width: 1.0,
                                    ),
                                  ),
                                  filled: true,
                                  fillColor: Colors.white,
                                  hintText: user_name,
                                  hintStyle: TextStyle(color: add_food_item_bg, fontSize: SizeConfig.blockSizeHorizontal *2.8,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.w400,),
                                  contentPadding: EdgeInsets.only(
                                    bottom: 30 / 2,
                                    left: 50 / 2, // HERE THE IMPORTANT PART
                                    // HERE THE IMPORTANT PART
                                  ),
                                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0))),
                            ),

                          ],
                        )),
                  ],
                ),
              ),
            ))],)
          )),
    );
  }
}

class CancelDialogs {
  static Future<void> showLoadingDialog(
      BuildContext context, GlobalKey key) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(
                  key: key,
                  backgroundColor: Colors.black54,
                  children: <Widget>[
                    Center(
                      child: Column(children: [
                        CircularProgressIndicator(),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Cancelling....",
                          style: TextStyle(color: Colors.lightBlueAccent),
                        )
                      ]),
                    )
                  ]));
        });
  }
}




Route _createRoute(String table_no) {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) =>
        SplitScreen(table_no),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      var begin = Offset(0.0, 1.0);
      var end = Offset.zero;
      var curve = Curves.ease;

      var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

      return SlideTransition(
        position: animation.drive(tween),
        child: child,
      );
    },
  );
}
