import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:toast/toast.dart';
import 'package:waiter/apis/ReceiptApi.dart';
import 'package:waiter/apis/SendEmail.dart';
import 'package:waiter/model/receiptresponse.dart';
import 'package:waiter/session/userRepository.dart';
import 'package:waiter/utils/all_constans.dart';
import 'package:waiter/utils/sizeconfig.dart';

import 'dashboard.dart';

class Receipt extends StatefulWidget {

  final receipt_number;
  Receipt(this.receipt_number, {Key key})
      : super(key: key);
  @override
  _ReceiptState createState() => _ReceiptState();
}

class _ReceiptState extends State<Receipt> {
  final List<String> tiffins = [
    "idly",
    "vada",
    "poori",
    "dosa",
    "manchuraian",
    "panipoori",
    "idly",
    "idly",
    "idly",
  ];

  String employee_id ="";
  String first_name ="";
  String last_name ="";
  String restaurant_id ="";
  String table_number ="";
  String order_id ="";
  String discount_amount ="";
  String tax_amount ="";
  String tip_amount ="";
  String sub_total ="";
  String total_amount ="";
  String order_unique_id ="";
  bool _loading = true;

  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  List<ItemsList> __receipt_items_list = new List();

  @override
  void initState() {
    super.initState();

    UserRepository().getuserdetails().then((userdetails) {
      print("userdata" + userdetails.length.toString());
      setState(() {
        employee_id = userdetails[0];
        first_name = userdetails[1];
        last_name = userdetails[2];
        //user_id = userdetails[1];
        print("SESSIONDATAFROMLOGIN" + employee_id+"------"+first_name+"-----"+last_name);
        UserRepository().getGenerateOtpDetails().then((restaurantdetails) {
          setState(() {
            restaurant_id = restaurantdetails[0];
            print("restaurant_id" + restaurant_id);
            Future.delayed(Duration(seconds: 2), () async {
              setState(() {
                ReceiptApiRepository().getreceipt(widget.receipt_number,employee_id,restaurant_id).then((value){
                  print("RECEIPT RESPSTATUS  "+value.responseStatus.toString());
                  debugPrint(base_url + ""+ value.result.toString());
                  if(value.responseStatus == 1){
                    setState(
                            () {
                          _loading = false;
                          order_id = value.orderInfo.orderId.toString();
                          table_number = value.orderInfo.tableNumber.toString();
                          tax_amount = value.orderInfo.taxAmount.toString();
                          tip_amount = value.orderInfo.tipAmount.toString();
                          sub_total = value.orderInfo.subTotal.toString();
                          discount_amount = value.orderInfo.discountAmount.toString();
                          total_amount = value.orderInfo.totalAmount.toString();
                          order_unique_id = value.orderInfo.orderUniqueId.toString();
                          for (int i = 0; i < value.orderInfo.itemsList.length; i++) {
                            __receipt_items_list.add(value.orderInfo.itemsList[i]);
                            print("itemslength"+value.orderInfo.itemsList[i].toString());
                          }
                          print("RECEIPTITEMLISTlength"+__receipt_items_list.length.toString());
                          Toast.show("SUCCESS RECEIPTDATA", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
                        }
                    );
                  }else if(value.responseStatus == 3){
                    setState(
                            (){
                          _loading = false;
                          Toast.show("NoneType' object has no attribute 'id'", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
                        });


                  } else if(value.responseStatus == 0){
                    setState(() {
                      _loading = false;
                      Toast.show("Invalid Credentials", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
                    });
                  }
                });

              });
            });


          });
        });
      });
    });


    // print(widget.main_order_id.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: _loading
            ? Center(
          child: SpinKitFadingCircle(color: Colors.lightBlueAccent),
        )
            : SingleChildScrollView(
        child: Container(
            color: Colors.white,
            child: Container(
              margin: EdgeInsets.fromLTRB(20, 10, 20, 20),
              color: dashboard_bg,
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    child: Padding(
                      padding: EdgeInsets.only(top: 45, left: 15, bottom: 0),
                      child: RichText(
                          text: TextSpan(
                        children: [
                          TextSpan(
                              text: "Receipt",
                              style: new TextStyle(
                                  fontSize: 20,
                                  color: login_passcode_text,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w600))
                        ],
                      )),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(10, 20, 10, 10),
                    child: Card(
                      elevation: 5,
                      color: Colors.white,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                              alignment: Alignment.topCenter,
                              decoration: BoxDecoration(
                                color: receipt_header,
                                borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(3.00),
                                    topLeft: Radius.circular(3.00)),
                              ),
                              child: Padding(
                                padding: EdgeInsets.only(
                                    top: 10, left: 0, bottom: 10),
                                child: RichText(
                                    text: TextSpan(
                                  children: [
                                    TextSpan(
                                        text: "Restaurant Name",
                                        style: new TextStyle(
                                            fontSize: 20,
                                            color: login_passcode_text,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.w600))
                                  ],
                                )),
                              )),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.only(
                                        top: 10, left: 20, bottom: 0),
                                    child: RichText(
                                        text: TextSpan(children: [
                                      TextSpan(
                                          text: "Table no\n",
                                          style: TextStyle(
                                              color: add_food_item_bg,
                                              fontSize: 16,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w300)),
                                      TextSpan(
                                          text: table_number,
                                          style: new TextStyle(
                                              fontSize: 18,
                                              color: add_food_item_bg,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w700))
                                    ])),
                                  ),
                                ],
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.only(
                                        left: 15, right: 20, bottom: 0),
                                    child: RichText(
                                        textAlign: TextAlign.end,
                                        text: TextSpan(children: [
                                          TextSpan(
                                              text: "Order No\n",
                                              style: TextStyle(
                                                  color: add_food_item_bg,
                                                  fontSize: 16,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w300)),
                                          TextSpan(
                                              text: "#"+order_unique_id,
                                              style: new TextStyle(
                                                  fontSize: 18,
                                                  color: add_food_item_bg,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w700))
                                        ])),
                                  ),
                                ],
                              )
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(15.0, 3.0, 15.0, 0.0),
                            child: Divider(
                              color: cart_viewline,
                            ),
                          ),
                          Container(
                            child: ListView.separated(
                                shrinkWrap: true,
                                physics: ClampingScrollPhysics(),
                                separatorBuilder: (context, index) => Divider(
                                      color: Colors.grey,
                                      height: 0.1,
                                    ),
                                scrollDirection: Axis.vertical,
                                itemCount: __receipt_items_list.length,
                                itemBuilder: (context, index) {
                                  Divider(
                                    color: Colors.purple,
                                    height: 10,
                                  );
                                  return Container(
                                      margin: EdgeInsets.all(5),
                                      width: MediaQuery.of(context).size.width *
                                          0.3,
                                      child: Column(
                                        children: [
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: <Widget>[
                                              Expanded(flex:1,child:
                                              Column(
                                                mainAxisAlignment:
                                                MainAxisAlignment.spaceAround,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  /*Expanded(flex:1,child:*/
                                                  Padding(
                                                      padding: EdgeInsets.only(
                                                          left: 15),
                                                      child: Text(
                                                        __receipt_items_list[index].itemName,
                                                        style: TextStyle(
                                                            color:
                                                                login_passcode_text,
                                                            fontSize: 16,
                                                            fontFamily:
                                                                'Poppins',
                                                            fontWeight:
                                                                FontWeight
                                                                    .w700),
                                                        textAlign:
                                                            TextAlign.start,
                                                      ))/*)*/,
                                                ],
                                              )),
                                              /*Expanded(flex:1,child:*/
                                              Padding(
                                                  padding: EdgeInsets.only(
                                                      left: 15, right: 15),
                                                  child: Text(
                                                    new String.fromCharCodes(
                                                            new Runes(
                                                                '\u0024')) +
                                                        __receipt_items_list[index].unitPrice,
                                                    style: TextStyle(
                                                        color:
                                                            login_passcode_text,
                                                        fontSize: 16,
                                                        fontFamily: 'Poppins',
                                                        fontWeight:
                                                            FontWeight.w700),
                                                    textAlign: TextAlign.start,
                                                  ))/*)*/
                                            ],
                                          ),
                                          3 > 0
                                              ? ListView.builder(
                                                  shrinkWrap: true,
                                                  physics:
                                                      ClampingScrollPhysics(),
                                                  scrollDirection:
                                                      Axis.vertical,
                                                  itemCount: __receipt_items_list[index].modifiersList.length,
                                                  itemBuilder: (context, i) {
                                                    return Container(
                                                      margin: EdgeInsets.all(2),
                                                      width:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .width *
                                                              0.3,
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: <Widget>[
                                                          Padding(
                                                              padding: EdgeInsets
                                                                  .only(
                                                                      left: 15,
                                                                      right:
                                                                          40),
                                                              child: Text(
                                                                __receipt_items_list[index].modifiersList[i].modifierName,
                                                                style: TextStyle(
                                                                    color:
                                                                        coupontext,
                                                                    fontSize:
                                                                        14,
                                                                    fontFamily:
                                                                        'Poppins',
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400),
                                                                textAlign:
                                                                    TextAlign
                                                                        .start,
                                                              )),
                                                          Padding(
                                                            padding:
                                                                EdgeInsets.only(
                                                                    left: 10,
                                                                    right: 20),
                                                            child: Text(
                                                              new String.fromCharCodes(
                                                                      new Runes(
                                                                          '\u0024')) +
                                                                  __receipt_items_list[index].modifiersList[i].modifierTotalPrice,
                                                              style: TextStyle(
                                                                  color:
                                                                      coupontext,
                                                                  fontSize: 14,
                                                                  fontFamily:
                                                                      'Poppins',
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400),
                                                              textAlign:
                                                                  TextAlign
                                                                      .start,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    );
                                                  })
                                              : SizedBox(),
                                          2 > 0
                                              ? ListView.builder(
                                                  shrinkWrap: true,
                                                  physics:
                                                      ClampingScrollPhysics(),
                                                  scrollDirection:
                                                      Axis.vertical,
                                                  itemCount: __receipt_items_list[index].specialRequestList.length,
                                                  itemBuilder: (context, i) {
                                                    return Container(
                                                      margin: EdgeInsets.all(2),
                                                      width:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .width *
                                                              0.3,
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: <Widget>[
                                                          Padding(
                                                              padding: EdgeInsets
                                                                  .only(
                                                                      left: 10,
                                                                      right:
                                                                          40),
                                                              child: Text(
                                                                __receipt_items_list[index].specialRequestList[i].name,
                                                                style: TextStyle(
                                                                    color:
                                                                        coupontext,
                                                                    fontSize:
                                                                        14,
                                                                    fontFamily:
                                                                        'Poppins',
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400),
                                                                textAlign:
                                                                    TextAlign
                                                                        .start,
                                                              )),
                                                          Padding(
                                                            padding:
                                                                EdgeInsets.only(
                                                                    left: 10,
                                                                    right: 40),
                                                            child: Text(
                                                              new String.fromCharCodes(
                                                                      new Runes(
                                                                          '\u0024')) +
                                                                  __receipt_items_list[index].specialRequestList[i].requestPrice.toString(),
                                                              style: TextStyle(
                                                                  color:
                                                                      coupontext,
                                                                  fontSize: 14,
                                                                  fontFamily:
                                                                      'Poppins',
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400),
                                                              textAlign:
                                                                  TextAlign
                                                                      .start,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    );
                                                  })
                                              : SizedBox()
                                        ],
                                      ));
                                }),
                          ),
                          Container(
                            color: dashboard_bg,
                            child: Column(
                              children: [
                                Container(
                                  margin: EdgeInsets.fromLTRB(
                                      5.0, 10.0, 20.0, 0.0),
                                  alignment: Alignment.topRight,
                                  child: Padding(
                                    padding:
                                        EdgeInsets.only(left: 15, bottom: 0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        RichText(
                                            text: TextSpan(
                                          children: [
                                            TextSpan(
                                                text: "SUB TOTAL ",
                                                style: new TextStyle(
                                                    fontSize: 16,
                                                    color: login_passcode_text,
                                                    fontFamily: 'Poppins',
                                                    fontWeight:
                                                        FontWeight.w700))
                                          ],
                                        )),
                                        RichText(
                                            text: TextSpan(children: [
                                          TextSpan(
                                              text: new String.fromCharCodes(
                                                      new Runes('\u0024')) +
                                                  sub_total,
                                              style: new TextStyle(
                                                  fontSize: 16,
                                                  color: login_passcode_text,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w700))
                                        ]))
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.fromLTRB(5.0, 5.0, 20.0, 0.0),
                                  alignment: Alignment.topRight,
                                  child: Padding(
                                    padding:
                                        EdgeInsets.only(left: 15, bottom: 0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        RichText(
                                            text: TextSpan(
                                          children: [
                                            TextSpan(
                                                text: "DISCOUNT",
                                                style: new TextStyle(
                                                    fontSize: 16,
                                                    color: login_passcode_text,
                                                    fontFamily: 'Poppins',
                                                    fontWeight:
                                                        FontWeight.w700))
                                          ],
                                        )),
                                        RichText(
                                            text: TextSpan(children: [
                                          TextSpan(
                                              text: new String.fromCharCodes(
                                                      new Runes('\u0024')) +
                                                  discount_amount,
                                              style: new TextStyle(
                                                  fontSize: 16,
                                                  color: login_passcode_text,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w700))
                                        ]))
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  margin:
                                  EdgeInsets.fromLTRB(5.0, 5.0, 20.0, 0.0),
                                  alignment: Alignment.topRight,
                                  child: Padding(
                                    padding:
                                    EdgeInsets.only(left: 15, bottom: 0),
                                    child: Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                      children: [
                                        RichText(
                                            text: TextSpan(
                                              children: [
                                                TextSpan(
                                                    text: "TIP AMOUNT",
                                                    style: new TextStyle(
                                                        fontSize: 16,
                                                        color: login_passcode_text,
                                                        fontFamily: 'Poppins',
                                                        fontWeight:
                                                        FontWeight.w700))
                                              ],
                                            )),
                                        RichText(
                                            text: TextSpan(children: [
                                              TextSpan(
                                                  text: new String.fromCharCodes(
                                                      new Runes('\u0024')) +
                                                      tip_amount,
                                                  style: new TextStyle(
                                                      fontSize: 16,
                                                      color: login_passcode_text,
                                                      fontFamily: 'Poppins',
                                                      fontWeight: FontWeight.w700))
                                            ]))
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.fromLTRB(
                                      5.0, 5.0, 20.0, 10.0),
                                  alignment: Alignment.topRight,
                                  child: Padding(
                                    padding:
                                        EdgeInsets.only(left: 15, bottom: 0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        RichText(
                                            text: TextSpan(
                                          children: [
                                            TextSpan(
                                                text: "TAX",
                                                style: new TextStyle(
                                                    fontSize: 16,
                                                    color: login_passcode_text,
                                                    fontFamily: 'Poppins',
                                                    fontWeight:
                                                        FontWeight.w700))
                                          ],
                                        )),
                                        RichText(
                                            text: TextSpan(children: [
                                          TextSpan(
                                              text: new String.fromCharCodes(
                                                      new Runes('\u0024')) +
                                                  tax_amount,
                                              style: new TextStyle(
                                                  fontSize: 16,
                                                  color: login_passcode_text,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w700))
                                        ]))
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                              color: Colors.white,
                              child: Container(
                                margin:
                                    EdgeInsets.fromLTRB(15.0, 50.0, 15.0, 0.0),
                                child: Divider(
                                  color: cart_viewline,
                                ),
                              )),
                          Container(
                            margin: EdgeInsets.fromLTRB(5.0, 5.0, 10.0, 10.0),
                            alignment: Alignment.topRight,
                            child: Padding(
                              padding: EdgeInsets.only(left: 15, bottom: 0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  RichText(
                                      text: TextSpan(
                                    children: [
                                      TextSpan(
                                          text: "TOTAL:",
                                          style: new TextStyle(
                                              fontSize: 18,
                                              color: login_passcode_text,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w700))
                                    ],
                                  )),
                                  RichText(
                                      text: TextSpan(children: [
                                    TextSpan(
                                        text: new String.fromCharCodes(
                                                new Runes('\u0024')) +
                                            total_amount,
                                        style: new TextStyle(
                                            fontSize: 18,
                                            color: login_passcode_text,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.w700))
                                  ]))
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Container(
                          margin: EdgeInsets.fromLTRB(5, 50, 5, 0),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              color: login_passcode_bg1,
                              borderRadius: BorderRadius.circular(0)),
                          child: InkWell(
                              child: FlatButton(
                                  child: Padding(
                                      padding:
                                          EdgeInsets.only(left: 25, right: 25),
                                      child: Text("PRINT",
                                          style: TextStyle(
                                              fontSize: SizeConfig
                                                      .safeBlockHorizontal *
                                                  4,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w800,
                                              color: Colors.white))),
                                  onPressed: () {
                                    Toast.show("Printing....", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
                                   /* Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => Receipt()));*/
                                  }))),
                      Container(
                          margin: EdgeInsets.fromLTRB(5, 50, 5, 0),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              color: dashboard_quick_order,
                              borderRadius: BorderRadius.circular(0)),
                          child: InkWell(
                              child: FlatButton(
                                  child: Padding(
                                      padding:
                                          EdgeInsets.only(left: 5, right: 5),
                                      child: Text("SEND EMAIL",
                                          style: TextStyle(
                                              fontSize: SizeConfig
                                                      .safeBlockHorizontal *
                                                  4,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w800,
                                              color: Colors.white))),
                                  onPressed: () {
                                    showDialog(
                                        context:
                                        context,
                                        builder:
                                            (context) {
                                          return _MyDialog(
                                              restaurant_id:restaurant_id,
                                              employee_id:employee_id,
                                              order_id:order_id,
                                             );
                                        });
                                  })))
                    ],
                  )
                ],
              ),
            ))));
  }
}




//EmailDialog

class _MyDialog extends StatefulWidget {
  _MyDialog({
    this.restaurant_id,
    this.employee_id,
    this.order_id,

  });
  final String restaurant_id;
  final String employee_id;
  final String order_id;

  @override
  _MyDialogState createState() => _MyDialogState();
}

class _MyDialogState extends State<_MyDialog> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _email_controller = TextEditingController();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  bool _loading = true;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
        backgroundColor: Colors.white,
        insetPadding: EdgeInsets.all(20),
        child: SingleChildScrollView(
            child: Container(
              //height: MediaQuery.of(context).size.height,
                child:  Center(
                  child: Column(children: [
                    new Form( key: _formKey,  //padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
                      child: new Column(
                        children: <Widget>[
                          Container(  alignment: Alignment.centerRight,child: InkWell(child:FlatButton(onPressed:(){
                            Navigator.of(context).pop();
                          },child: Padding(
                              padding: EdgeInsets.only(top:10,left:25,right: 5),
                              child: Image.asset(
                                'images/cancel.png',
                                height: 25,
                                width: 25,
                              )),) ,),),

                          Padding(
                              padding: EdgeInsets
                                  .symmetric(
                                  horizontal: 25,
                                  vertical: 10),
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment
                                    .start,
                                children: <Widget>[
                                  SizedBox(
                                    height: 5.0,
                                  ),
                                  Container(
                                      margin: EdgeInsets.only(bottom: 8),
                                      child:Text(
                                        'Email',
                                        textAlign:TextAlign.left,
                                        style: TextStyle(color:login_username_label_color,fontSize: 16,fontWeight: FontWeight.bold),
                                      )),
                                  TextFormField(
                                    validator: (val) {
                                      if (val.isEmpty)
                                        return 'Enter Email';
                                      return null;
                                    },
                                    controller: _email_controller,
                                    obscureText:
                                    false,
                                    keyboardType:
                                    TextInputType.text,
                                    decoration:
                                    InputDecoration(
                                        filled: true,
                                        fillColor: Colors.white,
                                        hintText:
                                        "Enter Email",
                                        contentPadding:
                                        EdgeInsets
                                            .only(
                                          bottom:
                                          30 /
                                              2,
                                          left: 50 /
                                              2, // HERE THE IMPORTANT PART
                                          // HERE THE IMPORTANT PART
                                        ),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                            BorderRadius.circular(0.0))),
                                  ),
                                ],
                              )

                          ),
                          Container(
                              margin:
                              EdgeInsets.fromLTRB(
                                  25, 10, 25, 20),
                              height: 50,
                              width: double.infinity,
                              alignment:
                              Alignment.center,
                              decoration: BoxDecoration(
                                  color: login_passcode_bg1,
                                  borderRadius:
                                  BorderRadius.circular(
                                      0)),
                              child: InkWell(
                                  child: FlatButton(
                                      minWidth: double
                                          .infinity,
                                      height: double
                                          .infinity,
                                      child: Text(
                                          "SUBMIT",
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontFamily:
                                              'Poppins',
                                              fontWeight:
                                              FontWeight
                                                  .w800,
                                              color:
                                              Colors.white)),
                                      onPressed: () {if(_formKey.currentState.validate()) {
                                        _formKey.currentState.save();
                                        SendEmailRepository().sendemail(_email_controller.text.toString(),widget.order_id, widget.restaurant_id, widget.employee_id).then((value){
                                          print(value);
                                          if(value.responseStatus == 0){
                                            Navigator
                                                .of(
                                                _keyLoader
                                                    .currentContext,
                                                rootNavigator: true)
                                                .pop();
                                            Toast.show("Invalid Credentials", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
                                          }else {
                                             _loading = false;
                                            Toast.show("Email Sent Successfully!", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(builder: (context) => HomePage()));
                                          }
                                        });
                                      }
                                      })))
                        ],
                      ),)
                  ]),
                ))));
  }
}



class CancelDialogs {
  static Future<void> showLoadingDialog(
      BuildContext context, GlobalKey key) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(
                  key: key,
                  backgroundColor: Colors.black54,
                  children: <Widget>[
                    Center(
                      child: Column(children: [
                        CircularProgressIndicator(),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Cancelling....",
                          style: TextStyle(color: Colors.lightBlueAccent),
                        )
                      ]),
                    )
                  ]));
        });
  }
}
